\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2022/23}

\title{\bfseries Weihnachtsübung zur Analysis III}
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Lukas\ Stoll, M.\ Sc.}
\date{06.~Dezember 2022 -- 12.~Januar 2023
  \thanks{Es sind maximal 25 Bonuspunkte zu erreichen.
    }}

\begin{document}

\maketitle

% Vormals Aufgabe 259 auf Blatt 6.

\begin{enumerate}[start=294]

\item
Es muß wohl kurz vor Weihnachten und nach einem sehr
langen Arbeitstag gewesen sein, als die Oberbürgermeisterin der Stadt
Augsburg beschloß: "`Augsburg braucht ein neues Schwimmbad! Und zwar
soll es die schnellste Rutschbahn haben, die möglich ist!"'

Nun, als die Ingenieure der Stadt Augsburg über die Sache
nachdachten, wurden sie gewahr, daß die Bürgermeisterin sie mit einer
gar nicht so leichten Aufgabe konfrontiert hatte.  Ihnen war nicht
klar, wie eine Rutschbahn auszusehen hatte, damit man auf ihr
möglichst schnell rutscht.  Eine Gerade zwischen dem Start- und dem
Endpunkt der Rutsche ist jedenfalls nicht die optimale Lösung
(obwohl dies die kürzeste Verbindung zwischen den beiden Punkten
 ist).  Ist die Rutschbahn nämlich in der Nähe des Startpunktes
steiler, so nimmt zwar die Länge der Rutschbahn zu, aber sie wird in
einem größeren Teilstück schneller durchrutscht.  Wie nun mußte
die genaue Form der Rutschbahn sein? Die Ingenieure wußten keinen
Rat und wandten sich daher an die Hörer der Analysis III.  Jetzt
sind Sie dran!

\paragraph{Ein Abstecher in die Gefilde der Variationsrechnung: Das
  Bra\-chis\-to\-chro\-nen"=Pro\-blem.}  Im Jahre 1696 veröffentlichte
  \name{Johann Bernoulli} das Bra\-chis\-to\-chro\-nen"=Pro\-blem (griechisch
  für "`kürzeste Zeit"'): Es seien zwei Punkte $A$ und $B$ einer über dem
  Boden senkrechten Ebene vorgegeben, die nicht vertikal übereinander
  liegen.  Man finde, falls möglich, diejenige Kurve, die die beiden
  Punkte $A$ und $B$ so verbindet, daß ein Massenpunkt, der in $A$ mit
  Geschwindigkeit $0$ startet und längs dieser Kurve unter dem Einfluß
  der Schwerkraft unter Vernachlässigung von Reibungskräften von $A$
  nach $B$ gleitet, in der kürzest möglichen Zeit in $B$ eintrifft.

  \name{Johann Bernoulli} gab an, bereits eine Lösung zu kennen,
  forderte jedoch die großen Mathematiker seiner Zeit auf, ihrerseits
  das Problem zu lösen.

  Dieser neue Aufgabentypus führte zur Entwicklung einer neuen
  mathematischen Theorie: der Variationsrechnung.

  \paragraph{Zur Formulierung und Lösung des Problems.}
  Wir gehen davon aus, daß das Problem eine Lösung besitzt.  Wir
  betrachten die Ebene $\set R^2$ mit horizontaler $x$-Achse und
  vertikaler $y$-Achse, wobei die positive Richtung der $y$-Achse in
  Richtung des Erdmittelpunktes zeigt.

  Es sei $A = (0, 0)$ und $B = (a, b)$ mit $a > 0$ und $b \ge 0$
  (Warum nicht $b < 0$?).  Wir nehmen an, daß die Spur der gesuchten
  Kurve durch den Graphen einer stetigen Funktion $\phi_0\colon
  \interval 0 a \to \set R$ beschrieben wird, daß also $\gamma\colon
  \interval 0 a \to \set R^2, t \mapsto (t, \phi_0(t))$ eine
  Parametrisierung der gesuchten Kurven ist, daß
  $\phi_0|\interval[open] 0 a \in \Diff 2(\interval[open] 0 a, \set
  R_+)$ und $\phi_0|\interval 0 a \geq 0$; somit befindet sich die Kurve
  abgesehen von Anfangs- und Endpunkt unterhalb des Niveaus von
  $A$.

  Aus der Mechanik wissen wir, daß die "`Gleitzeit"' eines
  Massepunktes entlang einer Kurve $\interval 0 a \to \set R^2, t
  \mapsto (t, \phi(t))$ durch
  \[
    T(\phi) = \int_0^a \Bigl(\frac{1 + \phi'^2(x)}{2 g \cdot
    \phi(x)}\Bigr)^{\frac 1 2} \, dx
  \]
  gegeben ist, wobei $g = 9{,}81 m/s^2$ die Erdbeschleunigung ist.
  Hierbei handelt es sich um ein beiderseitig uneigentliches
  Integral.

  Wir definieren nun
  \[
    L\colon G \coloneqq \set R \times \set R_+ \times \set R \to \set
    R,
    (t, q, \dot q) \mapsto \Bigl(\frac{1 + \dot q^2} q\Bigr)^{1/2}
  \]
  und
  \[
    \Omega_2(G) \coloneqq \{\psi \in \Diff 2(\interval{t_0}{t_1}, \set
    R) \mid \psi > 0\}
  \]
  für alle $t_0$, $t_1 \in \set R$ mit $t_0 < t_1$ und
  \[
    S_2^{t_0, t_1}\colon \Omega_2(G) \to \set R, \phi \mapsto
    \frac 1 {\sqrt {2g}} \int_{t_0}^{t_1} L(t, \phi(t), \phi'(t)) \,
    dt.
  \]
  Schließlich definieren wir für jede auf einem Intervall
  $I \subseteq \set R$ definierte $\Diff 1$"~Funk\-tion
  $\phi\colon I \to \set R$ ihre \emph{Prolongation}
  \[
    \hat \phi \colon I \to I \times \set R \times \set R, t \mapsto
    (t, \phi(t), \phi'(t)).
  \]
  Zur Behandlung des Problems benötigen wir ein wesentliches Ergebnis
  aus der Variationsrechnung, das wir hier ohne Beweis angeben,
  welches sich aber ohne weiteres aus den Ergebnissen der Vorlesung
  herleiten läßt:

  \paragraph{Die Euler--Lagrangesche Differentialgleichung.}
  Die gesuchte Funktion $\phi_0$ erfüllt die folgende implizite
  Differentialgleichung zweiter Ordnung:
  \[
    \frac{d}{dt}\Bigl(\frac{\partial L}{\partial \dot q} \circ \hat
    \phi_0\Bigr) = \frac{\partial L}{\partial q} \circ \hat\phi_0.
  \]
  (Theoretischen Physikern ist diese Gleichung wohlvertraut.  Sie ist
  eine der wichtigsten Hilfsmittel zur Beschreibung des Verhaltens
  physikalischer Systeme.)

  Zeige nun:
  \begin{enumerate}
  \item
    \label{it:var1}
    Zu jeder Wahl von $s'$, $s'' \in \set R$ und $r \in \set R
    \setminus \{0\}$ existieren eindeutig bestimmte Parameter $c$, $d
    \in \set R$, so daß die Funktion $f = (x - r) \cdot (c x + d)
    \cdot x^3$ in $r$ die Ableitungen
    \[
      f'(r) = s'\quad\text{und}\quad f''(r) = s''
    \]
    hat.  Außerdem gilt für $f$, daß
    \[
      f(0) = f'(0) = f''(0) = f(r) = 0
    \]
    und für alle $t \in \interval 0 r$ gilt
    \[
      \abs{f'(t)} \leq M \cdot (\abs r \cdot \abs {s''} + \abs{s'})
    \]
    mit $M = 19$, also (?)
    \[
      \abs{f(t)} \leq M \cdot \abs r \cdot (\abs r \cdot \abs{s''} +
      \abs {s'}).
    \]
  \item
    Für alle $t_0$, $t_1 \in \interval[open]0 a$ mit $t_0 < t_1$ ist die 
    $\Diff 2$-Funktion $\phi_0|\interval{t_0}{t_1}$ ein Minimum von
    $S_2^{t_0, t_1}|\Omega_2(G; \phi_0(t_0), \phi_0(t_1))$ mit
    \[
      \Omega_2(G; h_0, h_1) \coloneqq \{\psi \in \Omega_2(G) \mid
      \psi(t_0) = h_0 \land \psi(t_1) = h_1\}.
    \]

    (Tip: Angenommen, $\phi_0|\interval{t_0}{t_1}$ sei kein Minimum
    der Funktion.  Also existiert ein
    $\psi \in \Omega_2(G; \phi_0(t_0), \phi_0(t_1))$ mit
    $\delta \coloneqq S_2^{t_0, t_1}(\phi_0|\interval{t_0}{t_1}) -
    S_2^{t_0, t_1}(\psi) > 0$.  Nun konstruiere (zu hinreichend
    kleinem $r \in \set R_+$) eine Funktion
    $\phi\colon \interval 0 a \to \set R$, die auf
    $\interval{t_0}{t_1}$ mit $\psi$ und auf
    $\interval 0 {t_0 - r} \cup \interval{t_1 + r} a$ mit $\phi_0$
    übereinstimmt, die auf $\interval{t_0 - r}{t_0}$ und
    $\interval {t_1}{t_1 + r}$ jeweils durch eine Summe von $\phi_0$
    mit einer Funktion $f$ wie in~\ref{it:var1} gegeben ist und deren
    Einschränkung $\phi|\interval[open] 0 a$ zweimal stetig
    differenzierbar ist.  Für genügend kleines $r$ ist (?) dann
    $T(\phi) < T(\phi_0)$.  Widerspruch!)
  \item
    Für die Funktion
    \[
      g\colon G \to \set R, (t, q, \dot q) \mapsto
      \frac{\partial L}{\partial \dot q}(t, q, \dot q) \cdot \dot q -
      L(t, q, \dot q)
    \]
    ist $g \circ \hat \phi_0$ konstant.

    (Tip: Verwende die Euler--Lagrangesche Differentialgleichung und
    über $L$ nur die Information, daß $L$ nicht von $t$ abhängt.)

    \paragraph{Bemerkung.}
    Dieser "`Erhaltungssatz"' ist ein Spezialfall des noetherschen
    Theorems:  Symmetrien von $L$ bedingen Erhaltungssätze.
  \item
    Die Funktion $\phi_0|\interval[open] 0 a$ ist eine Lösung der
    expliziten Differentialgleichung
    \begin{equation}
    \label{eq:dgl2}
      y'' = - \frac{1 + y'^2}{2 y}
    \end{equation}
    zweiter Ordnung; außerdem gibt es eine Konstante $c \in \set R_+$,
    so daß $\phi_0|\interval[open] 0 a$ eine Lösung der impliziten
    Differentialgleichung
    \begin{equation}
    \label{eq:dgl1}
      y \cdot (1 + y'^2) = c
    \end{equation}
    erster Ordnung ist.
    Im folgenden sei $c$ die so festgelegte Konstante.

    (Tip: Leite zunächst mit Hilfe des
    Erhaltungssatzes~\eqref{eq:dgl1} her und folgere daraus mit Hilfe
    der Euler--Lagrangeschen Differentialgleichung~\eqref{eq:dgl2}.)
  \item
    Für jede Lösung $\phi\colon J \to \set R$ von~\eqref{eq:dgl1}
    gilt:
    \begin{enumerate}
    \item
      $\phi(J) \subseteq \interval[open left] 0 c$ und $\phi'(t) = 0
      \iff \phi(t) = c$.
    \item
      Ist $\phi$ zweimal differenzierbar und $\phi \not\equiv c$, so
      löst $\phi$ auch die Differentialgleichung~\eqref{eq:dgl2} und
      nimmt $c$ höchstens an einer Stelle $t \in J$ an.
    \item
      Für alle $t_* \in \set R$ ist auch $t_* + J \to \set R, t
      \mapsto \phi(t - t_*)$ eine Lösung von~\eqref{eq:dgl1}.
    \end{enumerate}
  \item
    Für die Funktionen
    \[
      u_c \coloneqq \frac c 2 \cdot (x - \sin)\quad\text{und}\quad
      v_c \coloneqq \frac c 2 \cdot (1 - \cos)
    \]
    gilt:
    \begin{enumerate}
    \item
      $u_c$ besitzt eine Umkehrfunktion $\check u_c\colon \set R \to
      \set R$.
    \item
      Die Funktion $\psi_c \coloneqq v_c \circ \check u_c\colon \set R
      \to \set R$ ist stetig und
      \[
        \psi_c(0) = 0,\quad \psi_c(c \pi/2) = c,\quad
        \psi_c(c \pi) = 0\quad\text{und}\quad\psi_c|\interval[open] 0
        {c \pi} > 0.
      \]
    \item
      $\psi_c|\interval[open] 0 {c\pi}$ ist eine $\Diff
      \infty$-Funktion mit Grenzwerten
      $\lim\limits_{t \to 0+} \psi'_c(t) = \infty$ und
      $\lim\limits_{t \to c \pi-} \psi'_c(t) = -\infty$.
    \item
      $\psi_c|\interval[open] 0 {c\pi}$ ist eine nicht weiter
      fortsetzbare Lösung von~\eqref{eq:dgl1} und~\eqref{eq:dgl2}.
    \item
      Die Kurve $\alpha \coloneqq (u_c, v_c)$ ist die kanonische
      Parametrisierung einer Zykloide und daher parametrisiert $t
      \mapsto (t, \psi_c(t))$ dieselbe Zykloide.
    \end{enumerate}
  \item
    Eine $\Diff 2$-Funktion $\phi\colon J \to \set R$ mit $\phi
    \not\equiv c$ ist genau dann eine Lösung von~\eqref{eq:dgl1},
    wenn es ein $t_* \in \set R$ gibt, so daß $\phi$ eine
    Einschränkung von $\interval[open]{t_*}{t_* + c\pi} \to \set R,
    t \mapsto \psi_c(t - t_*)$ ist.

    (Tip: Zu einem beliebig vorgegebenem $s \in J$ existiert ein
    $\tilde s \in \interval [open] 0 {c \pi}$, so daß $\phi'(s) =
    \psi'_c(\tilde s)$ ist; wegen~\eqref{eq:dgl1} ist auch $\phi(s)
    = \psi_c(\tilde s)$.  Daher (?) gilt die Behauptung
    wegen~\eqref{eq:dgl2} mit $t_* \coloneqq s - \tilde s$.)
  \item
    Es ist $\phi_0 = \psi_c|[0, a]$.  Daher ist die gesuchte
    Brachistochrone ein Teilbogen einer Zykloide.
  \item
    Für alle $t_0$, $t_1 \in \interval[open] 0 {c \pi}$ mit $t_0 <
    t_1$ gilt
    \[
      \int_{t_0}^{t_1} L \circ \hat\psi_c \, dx = \int_{t_0}^{t_1}
      \frac{\sqrt c}{\psi_c} \, dx = \sqrt{c} \cdot \bigl(\check u_c (t_1)
      - \check u_c(t_0)\bigr).
    \]
    Daher beträgt die "`Gleitzeit"' längs des vollständigen
    Zykloidenbogens $T(\psi_c) = \sqrt{2 c/g} \cdot \pi$.
  \item
    Wie lange benötigt die Oberbürgermeisterin, wenn sie entlang eines
    vollständigen Zykloidenbogens rutscht, dessen Endpunkte $A$ und
    $B$ insgesamt $100 \, m$ voneinander entfernt liegen; wie tief
    gerät sie dabei unter das "`Nullniveau"'; und wie groß ist die
    erreichte Maximalgeschwindigkeit?  (Benutze den
    Energieerhaltungssatz.)
  \item
    Warum wurden die Pläne der Augsburger Oberbürgermeisterin trotz
    allem nicht realisiert?
    \begin{enumerate}
    \item
      Man kann sich am Anfang der Rutschbahn nicht hinsetzen.
    \item
      Rutschen macht Spaß; deshalb sollte es lange dauern.
    \item
      Die Stadt Augsburg spart, indem sie die Materialkosten
      minimiert.
    \item
      Um dafür Sorge zu tragen, daß die Reibungskräfte
      vernachlässigt werden können, wird zuviel Schmierseife benötigt.
    \end{enumerate}
  \end{enumerate}

\end{enumerate}
\end{document}
