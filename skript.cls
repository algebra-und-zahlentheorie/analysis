\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{skript}

\LoadClass[a4paper,twoside,11pt]{book}

\setlength{\headheight}{13.6pt}

\RequirePackage{analysis}
\RequirePackage{fancyhdr}
\tikzexternalize

\pagestyle{fancy}
\fancyhead[LE,RO]{\thepage}
\fancyhead[RE]{\itshape\nouppercase{\leftmark}}
\fancyhead[LO]{\itshape\nouppercase{\rightmark}}
\fancyfoot{}

\RequirePackage{tocloft}
\setlength{\cftsecnumwidth}{30pt}

\setlist[enumerate,1]{label={(\alph*)},
  align=right, itemindent=0pt, labelindent=\parindent, labelwidth=*, leftmargin=!}

\setlist[enumerate,2]{label={(\roman*)},
  align=right, leftmargin=*}

\setlist[description]{font=\bfseries,leftmargin=0pt}

\newlist{axiomlist}{enumerate}{1}
\setlist[axiomlist,1]{font=\bfseries,
  align=left, leftmargin=\parindent}

\theoremstyle{definition}

\newtheorem{theorem}{Theorem}[section]
\newtheorem*{theorem*}{Theorem}
\renewcommand{\thetheorem}{\arabic{theorem}}

\newtheorem{corollary}{Korollar}[section]
\newtheorem*{corollary*}{Korollar}
\renewcommand{\thecorollary}{\arabic{corollary}}

\newtheorem{lemma}{Lemma}[section]
\newtheorem*{lemma*}{Lemma}
\renewcommand{\thelemma}{\arabic{lemma}}

\newtheorem{proposition}{Proposition}[section]
\newtheorem*{proposition*}{Proposition}
\renewcommand{\theproposition}{\arabic{proposition}}

\newtheorem{definition}{Definition}[section]
\newtheorem*{definition*}{Definition}
\renewcommand{\thedefinition}{\arabic{definition}}

\newtheorem{axiom}{Axiom}[section]
\newtheorem*{axiom*}{Axiom}
\renewcommand{\theaxiom}{\arabic{axiom}}

\newtheorem{comment}{Kommentar}[section]
\newtheorem*{comment*}{Kommentar}
\renewcommand{\thecomment}{\arabic{comment}}

\newtheorem{comments}{Kommentare}[section]
\newtheorem*{comments*}{Kommentare}
\renewcommand{\thecomments}{\arabic{comments}}

\newtheorem{warning}{Warnung}[section]
\newtheorem*{warning*}{Warnung}
\renewcommand{\thewarning}{\arabic{warning}}

\newtheorem*{rule*}{Regel}

\newtheorem*{rules*}{Regeln}
\newtheorem{rules}{Regeln}[section]
\renewcommand{\therules}{\arabic{rules}}

\newtheorem{exercise}{Aufgabe}[section]
\newtheorem*{exercise*}{Aufgabe}
\renewcommand{\theexercise}{\arabic{exercise}}

\newtheorem{exercises}[exercise]{Aufgaben}
\newtheorem*{exercises*}{Aufgaben}
\renewcommand{\theexercises}{\arabic{exercises}}

\newtheorem{example}{Beispiel}[section]
\newtheorem*{example*}{Beispiel}
\renewcommand{\theexample}{\arabic{example}}

\newtheorem{examples}[example]{Beispiele}
\newtheorem*{examples*}{Beispiele}
\renewcommand{\theexamples}{\arabic{examples}}

\newcommand{\thistheoremname}{}
\newtheorem*{genericthm}{\thistheoremname}
\newenvironment{namedthm}[1]
  {\renewcommand{\thistheoremname}{#1}%
   \begin{genericthm}}
  {\end{genericthm}}

\numberwithin{equation}{section}
\renewcommand{\theequation}{\arabic{equation}}
