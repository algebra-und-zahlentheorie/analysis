\chapter{Konvergenz von Funktionen}

\label{chap:limits}

\section{Erweiterung der Definitionen auf $\widehat{\set R}$}
\label{sec:R-hat}

In Abschnitt~\ref{sec:func_limit} haben wir die Konvergenz von
Abbildungen eingeführt.  Dafür haben wir punktierte
$\epsilon$-Umgebungen und Häufungspunkte von Mengen definiert.  Wir
beziehen zunächst die Punkte $\infty$ und $-\infty$ in unsere
Definitionen ein.

\begin{definition*}
  Sei $\epsilon \in \set R_+$.  In $\widehat{\set R}$ definieren wir
  die punktierten $\epsilon$-Umgebungen von $\infty$ und $-\infty$
  durch
  \[
  \dot U_\epsilon(\infty) \coloneqq \left]1/\epsilon,
    \infty\right[\quad\text{und}\quad
  \dot U_\epsilon(-\infty) \coloneqq \left]-\infty, -1/\epsilon\right[.
  \]
  Durch wörtliche Übertragung der Definitionen aus den
  Abschnitten~\ref{sec:limit_points} und \ref{sec:func_limit} sei dann
  definiert, für welche Teilmengen $M \in \powerset{\set R}$ die
  Punkte $\infty$ und $-\infty$ Häufungspunkte sind, und was die
  Bedeutung der Limites
  \[
  \lim_{t \to \infty} f(t) = q,\quad
  \lim_{t \to -\infty} f(t) = q,\quad
  \lim_{p_0} f = \infty\quad\text{und}\quad
  \lim_{p_0} f = -\infty
  \]
  ist.
\end{definition*}

Natürlich wissen wir jetzt auch, was $\lim\limits_{t \to \infty} f(t) =
\infty$ für eine Funktion $f\colon M \to \set R$ mit $M \subseteq R$
bedeutet, usw.

\begin{exercise*}
  \leavevmode
  \begin{enumerate}
  \item
    Ist $M \in \powerset{\set R}$, so gilt: $\infty$
    (bzw.~$-\infty$) ist genau dann Häufungspunkt von $M$, wenn $M$
    nach oben (bzw.~nach unten) unbeschränkt ist.
  \item
    Ist $E$ ein metrischer Raum, $M \in \powerset E$ eine
    beliebige Teilmenge und ist $p_0 \in E$ ein Häufungspunkt von $M$,
    so gilt für jede Funktion $f\colon M \to \set R$,  daß
    \[
    \lim_{p_0} f = \infty \iff \forall c \in \set R_+\, \exists \delta
    \in \set R_+\, \forall p \in \dot U_\delta(p_0) \cap M\colon f(p)
    > c.
    \]

    Man formuliere eine entsprechende Aussage für $\lim_{p_0} f = - \infty$.
  \end{enumerate}
\end{exercise*}

\section{Weitere Bezeichnungen}

Wir vereinbaren einige Sprechweisen, um dann über das
Konvergenzverhalten spezieller Funktionen Aussagen machen zu können.

\begin{definition*}
  Es seien $E$ und $E'$ metrische Räume, $N$, $M \in \powerset E$
  mit $N \subseteq M$, $p_0 \in E$ ein Häufungspunkt von
  $N$, $q \in E'$ und $f\colon M \to E'$ eine Abbildung.  Dann
  definieren wir:
  \begin{enumerate}
  \item
    $\displaystyle \lim_{\substack{p \to p_0 \\ p \in N}} f(p) = q
    \coloniff \lim_{p_0} f|N = q$.
  \item Sowie
    \begin{alignat*}{2}
      f(a+) \coloneqq \lim_{t \to a+} f(t) = q
      & \coloniff \lim_{\substack{t \to a \\ t \in N}} f(t) = q\quad
      & \text{mit}\quad & N \coloneqq \{t \in M \mid t > a\} \\
      \intertext{und}
      f(a-) \coloneqq \lim_{t \to a-} f(t) = q
      & \coloniff \lim_{\substack{t \to a \\ t \in N}} f(t) = q\quad
      & \text{mit}\quad & N \coloneqq \{t \in M \mid t < a\}
    \end{alignat*}
    für $E = \set R$ und $p_0 = a$.
  \end{enumerate}
\end{definition*}

\section{Konvergenzkriterien und Folgerungen}
\label{sec:criteria_for_convergence}

\begin{namedthm}{Festsetzung}
  In diesem Abschnitt seien $(E, d)$ und $(E', d')$ metrische Räume,
  $p_0 \in E$ ein Häufungspunkt von $M$ für $M \in \powerset E$,
  und $f\colon M \to E'$ eine Abbildung.  Wir erinnern an den
  Abschnitt~\ref{sec:func_limit}, insbesondere die dort verabredeten
  Sprechweisen, die Aussage über die Eindeutigkeit von Grenzwerten und
  das Theorem, welches die Verbindung zwischen der Konvergenz und der
  Stetigkeit von Abbildungen beschreibt.
\end{namedthm}

\begin{theorem}[Vertauschbarkeit von Limesbildung und stetigen
  Operationen]
  Es sei $g\colon E' \to E''$ eine in $q \in E'$ stetige Abbildung in
  einen weiteren metrischen Raum $E''$.  Dann gilt
  \[
  \lim_{p_0} f = q \implies \lim_{p_0} g \circ f = g(q).
  \]
\end{theorem}

\begin{proposition}
\label{prop:lim_of_all_sequences_is_lim}
  Die Abbildung $f\colon M \to E'$ konvergiert genau dann für $p \to
  p_0$ gegen $q$, wenn $p_0$ ein Häufungspunkt von $M$ ist und wenn
  für jede gegen $p_0$ konvergierende Punktfolge $(p_n)_{n \ge 1}$ mit
  $p_n \in M \setminus \{p_0\}$ die Bildfolge $(f(p_n))_{n \ge 1}$
  gegen $q$ konvergiert.
\end{proposition}

Diese Proposition stellt offensichtlich eine Analogie zum
Heine-Kriterium dar.

\begin{namedthm}{Das Cauchy-Kriterium für die Konvergenz von
    Abbildungen}
  \leavevmode
  \begin{enumerate}
  \item
    Konvergiert die Abbildung $f$ für $p \to p_0$ gegen einen Punkt
    von $E'$, so gilt
    \begin{equation}
      \label{eq:cauchy_func}
      \tag{$\ast$}
      \forall \epsilon \in \set R_+\, \exists \delta \in \set R_+\,
      \forall p_1, p_2 \in \dot U_\delta(p_0) \cap M\colon
      d'(f(p_1), f(p_2)) < \epsilon.
    \end{equation}
  \item
    Ist der metrische Raum $E'$ vollständig, so gilt auch die
    Umkehrung. D.\,h.~gilt die Bedingung~\eqref{eq:cauchy_func}, so
    konvergiert die Abbildung $f$ für $p \to p_0$ gegen einen Punkt
    von $E'$.
  \end{enumerate}
\end{namedthm}

\begin{proposition}
  Ist $E' = \prod_k E_k$ das Produkt metrischer Räume $E_1$, \dots,
  $E_k$ und ist $f = (f_1, \dotsc, f_n)\colon M \to E'$ eine Abbildung, so
  gilt:
  \[
  \lim_{p_0} f = q \iff \forall k \in \{1, \dotsc, n\}\colon
  \lim_{p_0} f_k = \pr_k (q).
  \]
\end{proposition}

\begin{corollary*}
  Es seien $E$, $E'$ und $F$ normierte $\set K$-Vektorräume, $B\colon
  E \times E' \to F$ eine stetige bilineare Abbildung, $f$, $f_1$,
  $f_2\colon M \to E$ und $g\colon M \to E'$ Funktionen, $\alpha \in
  \set K$, und es gelte
  \[
  \lim_{p_0} f = v,\quad
  \lim_{p_0} f_1 = v_1,\quad
  \lim_{p_0} f_2 = v_2\quad\text{und}\quad
  \lim_{p_0} g = w
  \]
  mit $v$, $v_1$, $v_2 \in E$ und $w \in E'$.  Dann gilt auch
  \[
  \lim_{p_0} (f_1 + f_2) = v_1 + v_2,\quad
  \lim_{p_0} (\alpha f) = \alpha v\quad\text{und}\quad
  \lim_{p_0} B(f, g) = B(v, w).
  \]
  Ist $E = \set K$ und $v \neq 0$, so gilt auch $\lim\limits_{p_0} 1/f
  = 1/v$.
\end{corollary*}

\begin{exercise}
  Die Voraussetzungen seien wie im vorangegangenen Korollar.
  Insbesondere seien die allgemeinen Voraussetzungen dieses Abschnitts
  erfüllt.
  \begin{enumerate}
  \item
    Ist $f\colon M \to E$ eine Funktion mit $\lim\limits_{p_0} f = 0$ und $g
    \in B(M, E')$, so gilt auch $\lim\limits_{p_0} B(f, g) = 0$.
  \end{enumerate}
  Im weiteren seien Funktionen $f$, $g$, $h\colon M \to \set R$
  gegeben.
  \begin{enumerate}[start=2]
  \item
    Gilt $\lim\limits_{p_0} f = \infty$ und $g \ge a \in \set R_+$, so
    folgt
    $\lim\limits_{p_0} 1/f = 0$ und $\lim\limits_{p_0} (f g) = \infty$.
  \item
    Gilt $f \leq g$ und konvergieren $f$ und $g$ für $p \to p_0$ gegen
    $A$ bzw.~$B$, so gilt auch $A \leq B$.
  \item \textbf{Sandwich-Theorem}
    Gilt $f \leq g \leq h$ und konvergieren $f$ und $h$ für $p \to
    p_0$ gegen denselben Grenzwert $A \in \widehat{\set R}$, so
    konvergiert auch die "`eingeklemmte"' Funktion $g$ für $p \to p_0$
    gegen $A$.
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Es seien $P = \sum\limits_{k = 0}^n a_k x^k$ und $Q = \sum\limits_{k
    = 0}^m b_k x^k$ zwei reelle Polynomfunktionen mit $a_n > 0$ und
  $b_m > 0$.  Man bestimme den Limes der rationalen Funktion $P/Q$ für
  $t \to \infty$.
\end{exercise}

\begin{theorem}
  Seien $a, b \in \widehat{\set R}$ mit $a < b$.  Ist dann $f\colon
  \left]a, b\right[ \to \set R$ eine monoton wachsende (bzw.~fallende)
  Funktion, so gilt
  \begin{alignat*}{3}
    \lim_a f & = \inf f(\left]a, b\right[)\quad
    & \text{und} & \quad
    & \lim_b f & = \sup f(\left]a, b\right[) \\
    \intertext{(bzw.}
    \lim_a f & = \sup f(\left]a, b\right[)\quad
    & \text{und} & \quad
    & \lim_b f & = \inf f(\left]a, b\right[)).
  \end{alignat*}
\end{theorem}

\begin{examples*}
  Alle in diesen Beispielen auftauchenden Funktionen seien auf die
  reelle Achse beschränkt.
  \begin{enumerate}
  \item
    $\lim\limits_\infty \exp = \infty$ und $\lim\limits_{-\infty} \exp
    = 0$.
  \item
    $\lim\limits_0 \ln = - \infty$ und $\lim\limits_\infty \ln =
    \infty$.
  \item
    Sei $\alpha \in \set R_+$.  Dann gilt:
    \begin{alignat*}{3}
      \lim_\infty x^\alpha & = \infty\quad
      & \text{und} & \quad
      & \lim_{t \to 0+} t^\alpha & = 0, \\
      \lim_\infty x^{-\alpha} & = 0\quad
      & \text{und} & \quad
      & \lim_{t \to 0+} t^{-\alpha} & = \infty, \\
      \lim_\infty (x^{-\alpha} \exp) & = \infty\quad
      & \text{und} & \quad
      & \lim_\infty (x^\alpha \exp(-x)) & = 0.
    \end{alignat*}
  \item
    $\lim\limits_{t \to \pi/2-} \tan(t) = \infty$,
    $\lim\limits_{t \to \pi/2+} \tan(t) = -\infty$,
    $\lim\limits_\infty \arctan = \pi/2$ und $\lim\limits_{-\infty}
    \arctan = - \pi/2$.
  \item
    $\lim\limits_\infty \cosh = \infty$, $\lim\limits_\infty \sinh =
    \infty$
    und $\lim\limits_\infty (\cosh - \sinh) = 0$.
    $\lim\limits_\infty \tanh = \lim\limits_\infty \coth = 1$,
    $\lim\limits_{-\infty} \tanh = \lim\limits_{-\infty} \coth = -1$,
    $\lim\limits_{t \to 0+} \coth(t) = \infty$ und $\lim\limits_{t \to 0-} \coth(t)
    = - \infty$.
  \end{enumerate}
\end{examples*}

\section{Der erweiterte oder zweite Mittelwertsatz der
  Differentialrechnung}

\begin{theorem*}
  Seien $D \in \powerset{\set R}$ und $a$, $b \in \set R$ mit $a < b$
  sowie $[a, b] \subseteq D$.  Weiterhin seien $f$, $g\colon D \to \set
  R$ Funktionen, die in allen Punkten von $[a, b]$ stetig und auf
  $\left]a, b\right[$ differenzierbar sind.  Dann existiert ein $t_0
  \in \left]a, b\right[$ mit
  \[
  g'(t_0) \cdot (f(b) - f(a)) = f'(t_0) \cdot (g(b) - g(a)).
  \]
  Ist $g'(t) \neq 0$ für alle $t \in \left]a, b\right[$, so gilt
  $g(b) \neq g(a)$ und
  \[
  \frac{f(b) - f(a)}{g(b) - g(a)} = \frac{f'(t_0)}{g'(t_0)}.
  \]
\end{theorem*}

\begin{namedthm}{Eine vektorwertige Modifikation des erweiterten Mittelwertsatzes
    der Differentialrechnung}
  Seien $D \in \powerset{\set R}$ und $a$, $b \in \set R$ mit $a < b$
  und $[a, b] \subseteq D$.  Weiterhin seien $f\colon D \to E$,
  $g\colon D \to \set R$ Funktionen in einen Banachraum $E$, die in
  allen Punkten von $[a, b]$ stetig und auf $\left]a, b\right[$
  differenzierbar sind; es gelte $g' \ge 0$.  Dann existiert ein
  $t_0 \in \left]a, b\right[$ mit
  \[
    \anorm{f(b) - f(a)} \cdot g'(t_0) \leq \anorm{f'(t_0)} \cdot (g(b) -
    g(a)).
  \]
\end{namedthm}
Den Mittelwertabschätzungssatz aus
Abschnitt~\ref{sec:controlled_variation} erhält man offenbar aus
dieser Aussage, indem man $g = x$ setzt.

\begin{small}
  \begin{proof}
    Wegen $g' \ge 0$ ist $g$ monoton wachsend, also $g(b) - g(a) \ge
    0$.  Wir setzen $v_0 \coloneqq f(b) - f(a)$.  Ist $v_0 = 0$, so
    gilt die zu beweisende Ungleichung trivialerweise für jedes
    $t_0$.  Daher können wir $v_0 \neq 0$ voraussetzen.  Nach
    der Folgerung des Fortsetzungssatzes von Hahn--Banach
    aus~\ref{sec:controlled_variation} wählen wir eine Linearform
    $\lambda\colon E \to \set R$ mit
    \[
    \lambda(v) = \anorm{v_0}\quad\text{und}\quad
    \forall v \in E\colon \abs{\lambda(v)} \leq \anorm{v}
    \]
    und definieren damit die Funktion
    \[
    \psi\colon [a, b] \to \set R, t \mapsto \anorm{v_0} \cdot (g(t) -
    g(a)) - \lambda(f(t) - f(a)) \cdot (g(b) - g(a)).
    \]
    Da $\lambda$ nach der Aufgabe~\ref{ex:linear_cont} aus
    Abschnitt~\ref{sec:normed_vector_spaces_as_metric_spaces} stetig
    ist, ist auch $\psi$ stetig und wegen der Vertauschbarkeit von
    Differentiation und stetigen linearen Operatoren
    (vgl.~Abschnitt~\ref{sec:diff_elem})
    auf dem
    Intervall $\left]a, b\right[$ differenzierbar.  Da nach Wahl von
    $\lambda$ außerdem $\psi(a) = \psi(b) = 0$ ist, existiert nach dem
    Satz von Rolle ein $t_0 \in \left]a, b\right[$, so daß $\psi'(t_0)
    = 0$.  Da $(\lambda \circ (f - f(a)))' = \lambda \circ f'$, folgt
    \[
    0 = \psi'(t_0) = \anorm{v_0} \cdot g'(t_0) - \lambda (f'(t_0))
    \cdot (g(b) - g(a)),
    \]
    also nach Wahl von $\lambda$, daß
    \[
    \anorm{v_0} \cdot g'(t_0) = \lambda(f'(t_0)) \cdot (g(b) - g(a))
    \leq \anorm{f'(t_0)} \cdot (g(b) - g(a)).
    \qedhere
    \]
  \end{proof}
\end{small}

\section{Regel von de L'Hospital}

\begin{proposition*}
  Es seien $\alpha$, $\beta \in \widehat{\set R}$ sowie $a \in
  \left[\alpha, \beta\right]$ und $D \coloneqq \left]\alpha,
    \beta\right[ \setminus \{a\}$.  Weiterhin seien $f$, $g\colon D
  \to \set R$ differenzierbare Funktionen, für die gelten:
  \begin{enumerate}
  \item
    $\forall t \in D\colon g'(t) \neq 0$.
  \item
    $(\lim\limits_a f = 0 \land \lim\limits_a g = 0)
    \lor \lim\limits_a g = \infty$.
  \item
    Es existiert $\lim\limits_a (f'/g') \in \widehat{\set R}$.
  \end{enumerate}
  Dann besitzt der Quotient $f/g$ in $a$ einen Grenzwert, und zwar ist
  \[
  \lim_a (f/g) = \lim_a (f'/g').
  \]
\end{proposition*}

\begin{examples*}
  \leavevmode
  \begin{enumerate}
  \item
    Für jedes $\alpha \in \set R_+$ ist $\lim\limits_{t \to 0+} (t^\alpha
    \cdot \ln(t)) = - \lim\limits_\infty (x^{-\alpha} \cdot \ln) = 0$.
  \item
    Für jedes $t \in \set R$ ist $\exp(t) = \lim\limits_{n \to \infty}
    \Bigl(1 + \frac t n\Bigr)^n.$
  \end{enumerate}
\end{examples*}

\section{Uneigentliche Integrale}
\label{sec:improper_integral}

\begin{definition}
  \label{def:improper1}
  Seien $E$ ein $\set K$-Banachraum, $a$, $b \in \widehat{\set R}$ mit
  $- \infty < a < b$, $f \in R(\left[a, b\right[, E)$ und $v \in E$.
  Wir sagen, daß das \emph{uneigentliche Integral}
  $\int\limits _a^b f \, dx$ gegen $v$ konvergiert, und schreiben
  $\int\limits_a^b f \, dx = v$, wenn die Funktion $\int_a^x f \,
  dx\colon \left[a, b\right[ \to E$ für $t \to b$ gegen $v$ konvergiert.  Im
  Falle $E = \set R$ ist auch $v \in \{\infty, - \infty\}$ zugelassen.
\end{definition}

\begin{examples}
  \leavevmode
  \begin{enumerate}
  \item
    $\displaystyle \int_1^\infty x^{-\alpha} \, dx = \begin{cases} 1/(\alpha
      - 1) & \text{für $\alpha > 1$ und} \\
      \infty & \text{für $0 < \alpha \leq 1.$}
    \end{cases}$
  \item
    Für $t \in \set R_+$ heißt
    \[
    \Gamma(t) \coloneqq \int_0^\infty x^{t - 1} \exp(-x) \, dx
    \]
    die \emph{Eulersche Integraldarstellung der Gammafunktion}.  Diese
    Funktion ist eine Extrapolation der Fakultät, genauer:
    \[
    \forall n \in \set N_0\colon \Gamma(n + 1) = n!.
    \]
  \end{enumerate}
\end{examples}

%% Stirlingsche Formel?

\begin{comment*}[Stirlingsche Formel]
  Mit Hilfe der Gammafunktion läßt sich für alle $n \in \set N_0$ folgende Abschätzung
  herleiten:
  \[
  a_n \coloneqq \sqrt{2 \pi n} \Bigl(\frac n e\Bigr)^n < n! < a_n \cdot e^{\frac 1 {12n}}
  \]
\end{comment*}

\begin{namedthm}{Das Cauchy-Kriterium für die Konvergenz
    uneigentlicher Integrale}
  In der Situation der Definition~\ref{def:improper1} konvergiert das
  uneigentliche Integral genau dann gegen ein $v \in E$, wenn es zu
  jedem $\epsilon \in \set R_+$ ein $r \in \left[a, b\right[$ gibt, so
  daß
  \[
  \forall t, s \in \left[r, b\right[\colon \Biggl(t < s \implies
  \anorm{\int_t^s f\,dx} < \epsilon\Biggr).
  \]
\end{namedthm}

\begin{namedthm}{Majorantenkriterium}
  Existiert in der Situation aus der Definition~\ref{def:improper1} eine
  Majorante $g \in R(\interval[open right] a b, \set R)$ von $f$, d.\,h.:~$\anorm{f} \leq
  g$, für welche das uneigentliche Integral $\int\limits_a^b g \, dx$
  gegen eine Zahl in $\set R$ konvergiert, so konvergiert auch das
  uneigentliche Integral $\int\limits_a^b f \, dx$ gegen ein $v \in E$.
\end{namedthm}

\begin{exercise}[Das Integralkriterium für die Konvergenz von Reihen]
  \label{ex:int_series}
  Ist $m \in \set Z$ und $f\colon \left[m, \infty\right[ \to \set R_+$
  eine monoton fallende Funktion, so konvergiert
  $\int\limits_m^\infty f \, dx$ genau dann in $\set R$, wenn
  $\sum\limits_{n = m}^\infty f(n)$ in $\set R$ konvergiert.
\end{exercise}

Mit diesem Kriterium hat man ein einfaches Instrument an der Hand, um
erneut die Aufgabe~\ref{ex:dirichlet} aus
Abschnitt~\ref{sec:exp_any_base} zu behandeln.

\begin{exercise}[Wachstum der harmonischen Reihe]
  In Beispiel~\ref{ex:zeta} in Abschnitt~\ref{sec:series} haben wir
  $\sum\limits_{k = 1}^\infty \frac 1 k = \infty$ bewiesen, in dem wir
  das Wachstum der Folge der Partialsummen grob abgeschätzt haben.
  Hier soll nun gezeigt werden, daß das Wachstum demjenigen des
  natürlichen Logarithmus' entspricht.  Dazu definiere man
  \[
  \gamma_n \coloneqq \sum_{k = 1}^n \frac 1 k - \ln (n)
  \]
  und beweise mit ähnlichen Methoden wir in
  Aufgabe~\ref{ex:int_series}, daß
  $\gamma_n$ eine monoton fallende Folge positiver Zahlen ist mit
  Grenzwert $\gamma \in \left]0, 1\right[$, die
  sog.~Euler--Mascheronische Konstante. Sie hat den Wert
  \[
  \gamma = 0{,}57721566490\ldots.
  \]
\end{exercise}

\begin{definition}
  \label{def:improper2}
  Seien $E$ ein $\set K$-Banachraum, $a$, $b \in \widehat{\set R}$ mit
  $a < b < \infty$, $f \in R(\left]a, b\right], E)$ und $v \in E$.
  Wir sagen, daß das \emph{uneigentliche Integral}
  $\int\limits _a^b f \, dx$ gegen $v$ konvergiert, und schreiben
  $\int\limits_a^b f \, dx = v$, wenn die Funktion $\int_x^b f \,
  dx\colon \left]a, b\right] \to E$ für $t \to a$ gegen $v$ konvergiert.  Im
  Falle $E = \set R$ ist auch $v \in \{\infty, - \infty\}$ zugelassen.
\end{definition}

\begin{example}
  $\displaystyle \int_0^1 x^{-\alpha} \, dx = \begin{cases}
    1/(1 - \alpha) & \text{für $0 < \alpha < 1$ und} \\
    \infty & \text{für $1 \leq \alpha.$}
  \end{cases}$
\end{example}

Das Cauchy- und Majorantenkriterium gelten für die in
Definition~\ref{def:improper2} erklärten uneigentlichen Integrale
mutatis mutandis.

\begin{definition}
  \label{def:improper3}
  Es seien $a$, $b \in \widehat{\set R}$ mit $a < b$ und $f \in
  R(\left]a, b\right[, E)$. Wir sagen, daß das \emph{uneigentliche
    Integral}
  $\int\limits_a^b f \, dx$ in $E$ konvergiert, wenn es ein $c \in
  \left]a, b\right[$ gibt, so daß die uneigentlichen Integrale
  $\int\limits_a^c f \, dx$ und $\int\limits_c^b f \, dx$
  konvergieren.  Im Falle der Konvergenz setzen wir
  \[
  \int_a^b f \, dx = \int_a^c f \, dx + \int_c^b f \, dx.
  \]

  Dieser Wert ist von der Wahl von $c$ unabhängig.
\end{definition}

\begin{warning*}
  In Definition~\ref{def:improper3} bedeutet die Aufspaltung des
  Integrals $\int\limits_a^b f \, dx$ eine getrennte (voneinander
  unabhängige) Konvergenzuntersuchung für die untere und obere
  Integrationsgrenze.
\end{warning*}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "skript"
%%% End:
