\documentclass{uebung}

\newcommand{\semester}{Sommersemester 2022}

% 25 Punkte
\title{\bfseries 11.~Übung zur Analysis II}
\author{Prof.\ Dr.\ Marc\
  Nieper-Wißkirchen \and Lukas\ Stoll, M.\ Sc.}  
\date{8.~Juli
  2022\thanks{Die Übungsblätter sind bis zur Übung am
    15.~Juli 2022 zu bearbeiten.}}

\begin{document}

\maketitle

\begin{enumerate}[start=175]

\item \oralex
  \textbf{Riccatische Differentialgleichung.}
  Es seien $g$, $h$, $k\colon I \to \set R$ Funktionen.
  \begin{enumerate}
  \item
    Sei $u\colon I \to \set R$ eine Lösung der \emph{Riccatischen} Differentialgleichung
    \begin{equation}
      \label{eq:riccati}
      \tag{$\ast$}
      u' = g(x) \cdot u + h(x) \cdot u^2 + k(x).
    \end{equation}
    (Für $k = 0$ liegt also eine Bernoullische Differentialgleichung vor.)

    Ferner sei $J \subseteq I$ ein nicht entartetes Intervall von
    $\set R$.  Zeige: Es ist $y\colon J \to \set R$ genau dann eine
    Lösung der Riccatigleichung~\eqref{eq:riccati}, wenn
    $v \coloneqq y - u|J$ eine Lösung der Bernoulligleichung
    \[
      v' = (g(x) + 2 h(x) u(x)) \cdot v + h(x) \cdot v^2
    \]
    ist.
  \item
    Bestimme alle Funktionen $y\colon I \to \set R$, für welche gilt:
    \[
      xy' + 2 x^2 (y - 2x)^2 + y - 4 x = 0\quad\text{und}\quad
      y(-1) = -1.
    \]
    (Tip: Zunächst auf $\set R_-$ mit Ricattigleichung; versuche $u =
    ax + b$.)
  \end{enumerate}

\item \oralex
  \textbf{Legendresche Polynome.}

  Für jedes $n \in \set N_0$ heißt das durch die \emph{Formel von
    Rodriguez} definierte Polynom
  \[
    P_n \coloneqq \frac 1 {2^n \cdot n!} \cdot ((x^2 - 1)^n)^{(n)}
  \]
  das \emph{Legendresche Polynom vom Grade $n$}.

  \begin{enumerate}
  \item
    Berechne und skizziere $P_0$, \dots, $P_4$.
  \item
    \label{it:legendre_ode}
    Zeige, daß $P_n$ eine Lösung der \emph{Legendreschen}
    Differentialgleichung
    \[
      (1 - x^2) y'' - 2 x y' + n (n + 1) y = 0
    \]
    ist.
  \item
    \label{it:legendre_zero}
    Zeige, daß $P_n$ in $\interval[open]{-1}{1}$ genau $n$ paarweise
    verschiedene Nullstellen hat und daß für $n \ge 1$ zwischen je zwei
    aufeinanderfolgenden Nullstellen von $P_n$ genau eine Nullstelle
    von $P_{n - 1}$ liegt.
  \end{enumerate}

  (Tip: Definiere $Q_n \coloneqq (x^2 - 1)^n$, zeige $(x^2 - 1) 
  Q_n' - 2 n x Q_n = 0$ und berechne die $k$-te Ableitung.  Aus $k = n
  + 1$ folgt~\ref{it:legendre_ode}.  Leite aus $k = n$ die
  Rekursionsformel $P_{n + 1} = x P_n + \frac{x^2 - 1}{n + 1} P_n'$
  her und beweise nun~\ref{it:legendre_zero} mit vollständiger
  Induktion.)
  
\item \writtenex % 5 Punkte
  Bestimme eine Lösung $y\colon \set R \to \set R^2$ der Anfangswertaufgabe
  \[
    y' = \begin{pmatrix}
      -1 & 2 \\
      -2 & 3
    \end{pmatrix} \cdot y\quad\text{mit}\quad
    y(0) = \begin{pmatrix}
      1 \\ 1
    \end{pmatrix}.
  \]

  (Tip: Bestimme eine Matrix $C$, so daß $CAC^{-1}$ Dreiecksgestalt
  hat.  Die transformierte Differentialgleichung $z' = (CAC^{-1}) z$
  ist leicht zu lösen, oder?  Wie hängen ihre Lösungen mit der Lösung
  des gegebenen Anfangswertproblems zusammen?)

\item \oralex
  \textbf{Spezielle Normen auf $\Lin(\set K^n, \set K^m)$}
  Seien $n$, $m \in \set N_0$, $A \in \Hom_{\set K}(\set K^n, \set
  K^m)$ und $(a_{ik})$ diejenige $(m \times n)$-Matrix, welche $A$
  bezüglich der kanonischen Basen von $\set K^n$ und $\set K^m$
  beschreibt.  Auf dem $\set K^n$ bzw.~$\set K^m$ betrachten wir die
  Normen
  \[
    \norm 1 v \coloneqq \sum \abs{v_k}\quad\text{und}\quad
    \norm \infty v \coloneqq \max \{ \abs{v_k} \}.
  \]
  Mit $\norm 1 A$ bzw.~$\norm \infty A$ bezeichnen wir die
  in (a) im Theorem aus Abschnitt 11.7 auf $\Lin(\set K^n, \set K^m)$
  eingeführten Normen, und zwar jeweils bezüglich der
  korrespondierenden Normen des $\set K^n$ bzw.~$\set K^m$.  Zeige: Hierfür
  gilt dann:
  \begin{align*}
    \norm 1 A & = \max\Biggl\{\sum_{i = 1}^m \abs{a_{ik}} \Biggm| k = 1,
    \dotsc, n\Biggr\} \\
    \intertext{und}
    \norm \infty A & = \max\Biggl\{\sum_{k = 1}^n \abs{a_{ik}} \Biggm| i =
                     1, \dotsc, m\Biggr\}.
  \end{align*}
  (Deswegen heißt $\norm 1 A$ auch die \emph{Spaltensummen-} und
  $\norm \infty A$ die \emph{Zeilensummennorm} von $A$.)

\item \writtenex
  Es seien $f(z) = \sum\limits_{n = 0}^\infty a_n \cdot z^n$ eine Potenzreihe mit
  Koeffizienten $a_n \in \set K$ und Konvergenzradius $\rho > 0$ und
  $A \in \Lin(E, E)$ ein Operator mit $\anorm A < \rho$.
  \begin{enumerate}
  \item % 1,5 + 1,5 Punkte
    Ist $\Phi\colon \Lin(E, E) \to F$ eine stetige lineare Abbildung
    in einen weiteren Banachraum $F$, so konvergiert die Reihe
    \[
      \sum_{n = 0}^\infty a_n \cdot \Phi(A^n)
    \]
    in $F$ gegen $\Phi(f(A))$.  Insbesondere konvergieren daher (?)
    für jeden Vektor $v \in E$ und für jeden Operator $C \in \Lin(E,
    E)$ die Reihen
    \[
      \sum_{n = 0}^\infty a_n \cdot (A^n(v)),
      \sum_{n = 0}^\infty a_n \cdot (C \circ A^n)\quad\text{und}\quad
      \sum_{n = 0}^\infty a_n \cdot (A^n \circ C)
    \]
    gegen $f(A)(v)$ bzw.~$C \circ f(A)$ bzw.~$f(A) \circ C$.
  \item % 0,5 Punkte
    Ist $C \in \Lin(E, E)$ ein Operator, der mit $A$ kommutiert,
    d.\,h.~$A \circ C = C \circ A$, so gilt auch
    $C \circ f(A) = f(A) \circ C$.
  \item % 1,5 Punkte
    Ist $C \in \GL(E)$ (vgl.\ Abschnitt 11.9), und gilt auch
    $\anorm{C \circ A \circ C^{-1}} < \rho$, so ist
    \[
      f(C \circ A \circ C^{-1}) = C \circ f(A) \circ C^{-1}.
    \]
  \end{enumerate}  

\item \writtenex % 5 Punkte
  Sei $A \in \Lin(E, E)$ mit $\anorm A < \infty$.  Zeige
  \[
    \exp(A) = \lim_{n \to \infty} \Bigl(1_E + \frac A n\Bigr)^n.
  \]
  (Tip: Zeige dazu $\anorm{\sum\limits_{k = 0}^n \frac 1 {k!} A^k -
    \bigl(1_E + \frac A n\bigr)^n} \leq \sum\limits_{k = 0}^n \frac 1
  {k!}
  \anorm{A}^k - \bigl(1 + \frac{\anorm A} n\bigr)^n$; dabei schreibe
  $\bigl(1_E + \frac A n\bigr)^n$ als explizites Polynom von $A$.)
  
\item \writtenex % 5 Punkte
  Sei $E$ ein Banachraum.  Zeige: Ist $\dim E < \infty$, so
  ist $\GL(E)$ eine \emph{dichte} Teilmenge von
  $\Lin(E, E) = \Hom_{\set K}(E, E)$ (vgl.~Definition 2 aus Abschnitt
  4.8).

  (Tip: Verwende das charakteristische Polynom von $A \in \Lin(E, E)$.)
  
\item
  \textbf{Über $\exp\colon \Lin(E, E) \to \GL(E)$}
  Zeige:
  \begin{enumerate}
  \item \writtenex % 4 Punkte
    $\displaystyle \forall A, B \in \Lin(E, E)\colon
    (A \circ B = B \circ A \implies \exp(A + B) = \exp(A) \circ
    \exp(B)).$

    (Tip: $\frac d{dt} (\gamma_A(t) \circ \gamma_B(t)) = ?$.)
  \item \writtenex % 1 Punkt
    Für jedes $A \in \Lin(E, E)$ ist $\gamma_A$ (vgl.~Theorem 2 aus
    Abschnitt 11.11) ein Gruppenhomomorphismus, d.\,h.
    \[
      \forall t, s \in \set R\colon \gamma_A(t + s) = \gamma_A(t)
      \circ \gamma_A(s).
    \]

    Wir nennen $\gamma_A$ daher auch eine Einparameter-Untergruppe
    von $\GL(E)$ und $A$ ihren Erzeuger.  (Diese Aussage ist in der
    Theorie der Liegruppen von fundamentaler Bedeutung.)

  \item \oralex
    Es sei $\beta\colon E \times E \to F$ eine stetige bilineare
    Abbildung in einen anderen $\set K$-Banachraum $F$.  Dann ist
    \[
      G(E, \beta) \coloneqq \{A \in \GL(E) \mid \forall v, w \in
      E\colon
      \beta(Av, Aw) = \beta(v, w)\}
    \]
    eine Untergruppe von $\GL(E)$, d.\,h.~$\forall A, B \in G(E,
    \beta)\colon
    A \circ B^{-1} \in G(E, \beta)$ und $G(E, \beta) \neq \emptyset$.
    (Auf diese Weise entstehen z.\,B.\ die orthogonale Gruppe oder die
    Lorentzgruppe, indem für $\beta$ geeignete Bilinearformen gewählt
    werden.)

    Zeige weiter: Für jedes $A \in \Lin(E, E)$ gilt folgende
    Äquivalenz:
    \[
      \bigl(\forall v, w \in E\colon \beta(Av, w) + \beta(v, Aw) =
      0\bigr)
      \iff
      \gamma_A(\set R) \subset G(E, \beta).
    \]
    (Tip: $\frac{d}{dt} \beta(\gamma_A(t) v, \gamma_A(t) w) = ?$.)
  \end{enumerate}

\item \oralex
  Zu jeder stetigen Funktion $A\colon J \to \Lin(E, E)$ werde $Y(s, t)
  \in \GL(E)$ wie in Teil (a) des Theorems aus Abschnitt 11.12 definiert.
  Dann gilt
  \[
    \forall r, s, t \in J\colon Y(s, t) \circ Y(r, s) = Y(r, t)\land
    Y(s, t)^{-1} = Y(t, s).
  \]

\item \oralex
  Berechne $\displaystyle \exp
  \begin{pmatrix} \phantom{-}0 & 1 \\ - 1 & 0\end{pmatrix}$.
  
% Später!
\item \oralex \textbf{Stetigkeit multilinearer Abbildungen.}
  Seien $E_1$, \dots, $E_n$ endlich-di\-men\-sio\-nale $\set K$-Vektorräume und
  $F$ irgendein normierter $\set K$-Vektorraum.  Zeige:
  Jede $n$-lineare Abbildung
  \[
  \phi\colon E_1 \times \dotsb \times E_n \to F
  \]
  ist bezüglich jeder Wahl von Normen auf den $E_k$ stetig.

  (Tip: Für jedes $k = 1$, \dots, $n$ wähle man eine Basis $(b_{k1},
  \dotsc, b_{km_k})$ von $E_k$ und bezeichne mit $(\lambda_{k1}, \dotsc,
  \lambda_{km_k})$ die dazu duale Basis von $E^*_k \coloneqq \Hom_{\set
    K}(E_k, \set K)$, d.\,h.~$\lambda_{ki}\colon E_k \to \set K$ ist die
  Linearform, die durch
  \[
  \forall j \in \{1, \dotsc, m_k\}\colon \lambda_{ki}(b_{kj}) =
  \delta_{ij}
  \]
  charakterisiert ist.  Dann gilt (?)
  \begin{multline*}
  \forall v = (v_1, \dotsc, v_n) \in E_1 \times \dotsb \times
  E_n\colon \\
  \phi(v) = \sum \lambda_{1i_1}(v_1) \dotsm \lambda_{ni_n}(v_n) \cdot
  \phi(b_{1i_1}, \dotsc, b_{ni_n}),
  \end{multline*}
  wobei in der Summe der Index $i_k$
  jeweils die Zahlen $1$, \dots, $m_k$ durchläuft.)

\end{enumerate}  

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
