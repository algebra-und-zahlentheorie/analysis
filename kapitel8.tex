\chapter{Ein elementares Integral}
\label{chap:elementary_integral}

In Kapitel~\ref{chap:integration} werden wir das Lebesguesche Integral
kennenlernen.  Für den Moment begnügen wir uns mit einem elementaren
Integral, welches wir in diesem Kapitel mit funktionalanalytischen
Methoden einführen.  Von der Einfachheit ist es auch mit dem
Riemannschen Integral zu vergleichen.

\section{Stetige Fortsetzung gleichmäßig stetiger Abbildungen}

\label{sec:extension}

\begin{theorem*}
  Sei $E$ ein metrischer Raum und $E'$ ein vollständiger metrischer
  Raum, sowie $M$ eine Teilmenge von $E$, $\overline M$ ihre abgeschlossene
  Hülle in $E$ (vgl.\ Abschnitt~\ref{sec:closed_subsets}) und $f\colon
  M \to E'$ eine \emph{gleichmäßig} stetige Abbildung
  (vgl.\ Abschnitt~\ref{sec:uniformity}).  Dann existiert genau eine
  stetige Abbildung $F\colon \overline M \to E'$, welche $f$
  \emph{fortsetzt}, d.\,h.~$F|M = f$.  Tatsächlich ist $F$ wieder
  gleichmäßig stetig.
\end{theorem*}

\begin{small}
  \begin{proof}
    Für jedes $p \in E$ sei $\mathfrak F_p$ die Menge der Folgen
    $(p_n)_{n \in \set N_0}$ in $M$ mit $\lim\limits_{n \to \infty} p_n = p$.

    Nach~\ref{sec:closed_subsets} ist $\overline M = \{p \in E \mid
    \mathfrak F_p \neq \emptyset\}$.

    \begin{description}
    \item[Zur Eindeutigkeit von $F$.]  Sei $F\colon \overline M \to
      E'$ eine stetige Fortsetzung von $f$, und sei $p \in \overline
      M$.  Dann wählen wir eine Folge $(p_n) \in \mathfrak F_p$.  Nach dem Heine-Kriterium
      gilt dann
      \begin{equation}
        \label{eq:extension}
        F(p) = \lim\limits_{n \to \infty} F(p_n) = \lim_{n \to \infty} f(p_n).
      \end{equation}
      Somit ist also $F(p)$ durch $f$ eindeutig festgelegt.

    \item[Zur Existenz von $F$.]  Die Formel~\eqref{eq:extension} ist
      die Idee zur Definition von $F$.  Dazu müssen wir zunächst
      zeigen, daß für jedes $(p_n) \in \mathfrak F_p$ der Grenzwert
      $\lim\limits_{n \to \infty} f(p_n)$ der rechten Seite
      von~\eqref{eq:extension} existiert: Nach
      Aussage~\ref{prop:conv_cauchy} aus \ref{sec:cauchy} ist $(p_n)$
      eine Cauchyfolge; nach Aussage~\ref{prop:image_cauchy} aus
      \ref{sec:cauchy} daher auch $(f(p_n))$ (an dieser Stelle geht die
      gleichmäßige Stetigkeit von $f$ wesentlich ein).  Schließlich
      konvergiert wegen der Vollständigkeit von $E'$ die letztgenannte Folge.

      Als nächstes zeigen wir, daß die rechte Seite
      von~\eqref{eq:extension} unabhängig von der gewählten Folge
      $(p_n)$ aus $\mathfrak F_p$ ist: Seien dazu $(p_n')$ und
      $(p_n'') \in \mathfrak F_p$.  Wir fügen beide Folgen zu einer
      neuen Folge $(p_n)_{n \in \set N_0}$ zusammen, indem wir $p_{2n}
      \coloneqq p'_n$ und $p_{2n + 1} \coloneqq p_n''$ setzen.  Dann
      ist auch $(p_n) \in \mathfrak F_p$.  Folglich sind $(f(p'_n))$
      und $(f(p_n''))$ zwei Teilfolgen der nach obigem konvergenten
      Folge $(f(p_n))$.  Somit haben $(f(p_n'))$ und $(f(p_n''))$ den
      gleichen Grenzwert, nämlich $\lim\limits_{n \to \infty} f(p_n)$.  Damit haben wir gezeigt,
      daß genau eine Abbildung $F\colon \overline M \to E'$ mit
      \[
      \forall p \in \overline M\,
      \forall (p_n) \in \mathfrak F_p\colon
      \lim_{n \to \infty} f(p_n) = F(p)
      \]
      existiert.

      Mit dieser Definition setzt $F$ insbesondere $f$ fort,
      d.\,h.~$F|M = f$: Ist etwa $p \in M$, so können wir die
      konstante Folge $(p_n) \in \mathfrak F_p$ mit $p_n \coloneqq p$
      für alle $n \in \set N_0$ betrachten.  Dann folgt
      \[
      F(p) = \lim_{n \to \infty} f(p_n) = f(p).
      \]

      Es bleibt zu zeigen, daß das so konstruierte $F$ gleichmäßig
      stetig ist: Geben wir uns also ein $\epsilon \in \set R_+$ vor.
      Aufgrund der gleichmäßigen Stetigkeit von $f$ existiert ein
      $\delta \in \set R_+$, so daß
      \begin{equation}
        \label{eq:extension_cont}
        \forall p, q \in M\colon \bigl(d(p, q) < \delta \implies d'(f(p), f(q)) < \epsilon/2\bigr).
      \end{equation}
      Seien weiter $p$, $q \in \overline M$ mit $d(p, q) < \delta$
      vorgegeben.  Wir wählen zwei Folgen $(p_n) \in \mathfrak F_p$
      und $(q_n) \in \mathfrak F_q$.  Aufgrund der Stetigkeit der
      Abstandsfunktion $d$ gilt $\lim\limits_{n \to \infty} d(p_n,
      q_n) = d(p, q) < \delta$.  Ohne Einschränkung der Allgemeinheit
      dürfen wir also annehmen, daß schon $d(p_n, q_n) < \delta$ für
      alle $n \in \set N_0$.  Wegen~(\ref{eq:extension_cont}) folgt
      deswegen auch $d'(f(p_n), f(q_n)) < \epsilon/2$.  Schließlich
      nutzen wir die Stetigkeit der Abstandsfunktion $d'$ und
      erhalten
      \begin{equation*}
        d'(F(p), F(q)) = d'\bigl(\lim_{n \to \infty} f(p_n), \lim_{n \to \infty} f(q_n)\bigr)
        = \lim_{n \to \infty} d'(f(p_n), f(q_n)) \leq \epsilon/2 < \epsilon.
      \end{equation*}
      Damit ist die gleichmäßige Stetigkeit von $F$ bewiesen.
    \end{description}
  \end{proof}
\end{small}

\begin{corollary*}
  Sind $E$ und $F$ Banachräume über $\set K$, ist $U$ ein
  Untervektorraum von $F$ und $\overline U$ seine abgeschlossene Hülle
  in $F$, so ist $\overline U$ ebenfalls ein Untervektorraum von $F$
  (vgl.\ Aufgabe~\ref{ex:closure_of_subspace} aus
  Abschnitt~\ref{sec:normed_vector_spaces_as_metric_spaces}) und zu
  jeder stetigen, linearen Abbildung $f\colon U \to E$ existiert genau
  eine stetige, lineare Abbildung $\overline f\colon \overline U \to E$ mit $\overline f|U
  = f$.

  Ist überdies die Konstante $M \in \left[0, \infty\right[$, so daß
  \[
  \forall v \in U\colon \anorm{f(v)} \leq M \cdot \anorm v,
  \]
  so gilt auch
  \[
  \forall v \in \overline U\colon \anorm{\overline f(v)} \leq M \cdot \anorm v
  \]
  (vgl.\ Aufgabe~\ref{ex:linear_cont} aus
  Abschnitt~\ref{sec:normed_vector_spaces_as_metric_spaces}).
\end{corollary*}

\section{Das Integral von Treppenfunktionen}

\label{sec:step_functions}

\begin{namedthm}{Festsetzung}
  Für den Rest des Kapitels seien $a$, $b \in \set R$ reelle Zahlen
  mit $a < b$ und $(E, \anorm \cdot)$ ein $\set K$-Banachraum.
\end{namedthm}

\newpage
\begin{definition*}
  \leavevmode
  \begin{enumerate}
  \item
    Eine \emph{Zerlegung} des Intervalls $[a, b]$ ist eine endliche
    Folge $(t_k)_{k = 0, \dotsc, n}$ von Punkten $t_k \in [a, b]$ mit
    \[
    a = t_0 < t_1 < \dotsb < t_{n - 1} < t_n = b.
    \]
  \item
    Ist $(t_k)_{k = 0, \dotsc, n}$ eine Zerlegung des Intervalls $[a, b]$, so nennen wir eine weitere
    Zerlegung $(t'_{k'})_{k' = 0, \dotsc, n'}$ von $[a, b]$ eine \emph{Verfeinerung} von
    $(t_k)_{k = 0, \dotsc, n}$, wenn es eine streng monoton wachsende Abbildung
    $\sigma\colon \{0, \dotsc, n\} \to \{0, \dotsc, n'\}$ mit $t_k = t'_{\sigma(k)}$ für alle $k \in
    \{0, \dotsc, n\}$ gilt.
  \item
    Eine Funktion $\phi\colon [a, b] \to E$ heißt eine \emph{Treppenfunktion} (mit Werten in $E$),
    wenn es eine Zerlegung $(t_k)_{k = 0, \dotsc, n}$ von $[a, b]$ gibt, so daß
    \begin{equation}
      \label{eq:step_function}
      \forall k \in \{1, \dotsc, n\}\colon \phi|\left]t_{k - 1}, t_k\right[ \equiv \text{const.}
    \end{equation}
    Eine Zerlegung, für welche~\eqref{eq:step_function} gilt, heißt der Treppenfunktion $\phi$
    \emph{angepaßt}.
  \item
    Die Menge aller Treppenfunktionen $\phi\colon [a, b] \to E$ wird
    mit $T([a, b], E)$ bezeichnet.
  \end{enumerate}
\end{definition*}

\begin{lemma*}
  \leavevmode
  \begin{enumerate}
  \item
    Ist eine Zerlegung $(t_k)$ des Intervalls $[a, b]$ einer
    Treppenfunktion $\phi \in T([a, b], E)$ angepaßt, so ist auch jede Verfeinerung von
    $(t_k)$ der Treppenfunktion $\phi$ angepaßt.
  \item
    Zu je zwei Zerlegungen $(t'_i)$ und $(t''_j)$ des Intervalls $[a,
      b]$ gibt es stets eine gemeinsame Verfeinerung $(t_k)$.
  \end{enumerate}
\end{lemma*}

\begin{proposition}
  Sind $\phi$, $\psi\colon [a, b] \to E$ Treppenfunktionen und ist $c \in \set K$, so sind auch
  $\phi + \psi$, $c \cdot \phi$ und $\anorm \phi$ Treppenfunktionen.  Daher ist
  $T([a, b], E)$ ein Untervektorraum des Banachraumes $B([a, b], E)$
  (vgl.\ Beispiel~\ref{ex:sup_norm} aus
  Abschnitt~\ref{sec:normed_vector_spaces} und Abschnitt~\ref{sec:banach_bounded}).
\end{proposition}

\begin{proposition}
  Zu jeder Treppenfunktion $\phi\colon [a, b] \to E$ existiert genau ein Vektor $I(\phi) \in E$,
  so daß für jede der Funktion $\phi$ angepaßte Zerlegung $(t_k)_{k = 0, \dotsc, n} $ von $[a, b]$
  gilt, daß
  \[
  I(\phi) = \sum_{k = 1}^n \phi \Bigl(\frac{t_{k - 1} + t_k} 2\Bigr) \cdot (t_k - t_{k - 1}).
  \]
\end{proposition}

In Abschnitt \ref{sec:integral} werden wir $I(\phi)$ als das Integral von $\phi$ erkennen.

\begin{example*}
  Für jedes $v \in E$ ist die konstante Funktion $\phi\colon [a, b]
  \to E, t \mapsto v$ eine Treppenfunktion; für sie gilt $I(\phi) = v
  \cdot (b - a)$.
\end{example*}

\begin{proposition}
  Die Abbildung $I\colon T([a, b], E) \to E, \phi \mapsto I(\phi)$ ist
  linear; d.\,h.~für alle $\phi$,~$\psi \in T([a, b], E)$ und alle $c \in \set K$ gilt
  \[
  I(\phi + \psi) = I(\phi) + I(\psi) \quad\text{und}\quad I(c \cdot \phi) = c \cdot I(\phi).
  \]
  Weiterhin haben wir die Abschätzung
  \[
  \anorm{I(\phi)} \leq I(\anorm \phi) \leq (b - a) \cdot \norm{\infty} \phi.
  \]
  Insbesondere ist die Abbildung $I$ gleichmäßig stetig.  Im Falle $E
  = \set R$ gilt schließlich folgende Monotonie-Aussage:
  \[
  \phi \leq \psi \implies I(\phi) \leq I(\psi).
  \]
\end{proposition}

\section{Der Raum der Regelfunktionen und deren Integral}

\label{sec:integral}

\begin{definition*}
  Mit $R([a, b], E)$ bezeichnen wir die abgeschlossene Hülle von
  $T([a, b], E)$ in dem $\set K$-Banachraum $B([a, b], E)$.  Die
  Funktionen $f \in R([a, b], E)$ heißen \emph{Regelfunktionen} auf
  $[a, b]$ mit Werten in $E$.

  Nach der Folgerung aus Abschnitt~\ref{sec:extension} existiert genau
  eine stetige, lineare Funktion
  \[
  J\colon R([a, b], E) \to E,
  \]
  welche die Funktion $I\colon T([a, b], E) \to E$ fortsetzt.  Für
  jedes
  $f \in R([a, b], E)$ heißt der Wert
  \[
  \int_a^b f \, dx \coloneqq \int_a^b f(t) \, dt
  \coloneqq J(f)
  \]
  das \emph{Integral} von $f$.
\end{definition*}

\begin{theorem}
  Für alle $\phi \in T([a, b], E)$, $f$, $g \in R([a, b], E)$ und $c
  \in \set K$ gilt:
  \begin{axiomlist}[label=(I\arabic*), start=0]
  \item
    $\displaystyle \int_a^b \phi \, dx = I(\phi),$
  \item
    $\displaystyle \int_a^b (f + g) \, dx = \int_a^b f \, dx +
    \int_a^b g \, dx,$
  \item
    $\displaystyle \int_a^b (c \cdot f) \, dx = c \cdot \int_a^b f \, dx,$
  \item
    \label{it:int_norm}
    $\displaystyle \anorm{\int_a^b f \, dx} \leq \int_a^b \anorm f \,
    dx \leq (b - a) \cdot \norm\infty f$ und
  \item
    $\displaystyle f \leq g \implies \int_a^b f \, dx \leq \int_a^b g
    \, dx$ im Falle $E = \set R$.
  \end{axiomlist}
  Im Falle \ref{it:int_norm} sei beachtet, daß
  $\anorm f \in R([a, b], \set R)$ für alle $f \in R([a, b], E)$.
\end{theorem}

\begin{theorem}
  Als abgeschlossener Untervektorraum des Banachraumes $B([a, b], E)$
  ist $R([a, b], E)$ auch ein Banachraum.  Es gilt der
  \emph{elementare Vererbungssatz} der Integralrechnung:

  Ist $(f_n)_{n \ge m}$ eine Folge von Regelfunktionen $f_n\colon [a,
    b] \to E$, welche gleichmäßig gegen eine Funktion $f\colon [a, b]
  \to E$ konvergiert, so ist auch $f$ eine Regelfunktion, und es gilt
  \[
  \int_a^b f \, dx = \lim\limits_{n \to \infty} \int_a^b f_n \, dx.
  \]
\end{theorem}

\begin{lemma*}
  Zu jeder Regelfunktion $f \in R([a, b], E)$ und jedem $\epsilon \in
  \set R_+$ existiert eine Zerlegung $(t_k)_{k = 0, \dotsc, n}$ des Intervalls
  $[a, b]$, so daß für jede Folge $(\xi_k)_{k = 1, \dotsc, n}$ von
  Parametern $\xi_k \in \left]t_{k - 1}, t_k\right[$ für die
    Treppenfunktion $\phi \in T([a, b], E)$, welche durch
    \[
    \phi(t) \coloneqq \begin{cases}
      f(\xi_k) & \text{für $t \in \left]t_{k - 1}, t_k\right[$ und} \\
      f(t_k) & \text{für $t = t_k$}
    \end{cases}
    \]
    definiert ist,
    \[
    \norm\infty{f - \phi} < \epsilon
    \]
    gilt.  Es sei beachtet, daß $\phi([a, b]) \subseteq f([a, b])$
    nach Konstruktion von $\phi$.
\end{lemma*}

\begin{proposition*}
  \leavevmode
  \begin{enumerate}
  \item
    Ist $f \in R([a, b], E)$ eine Regelfunktion und $U$ ein
    \emph{abgeschlossener} Untervektorraum von $E$ mit $f([a, b]) \subseteq
    U$, so gilt auch $\int_a^b f \, dx \in U$.
  \item
    \label{it:int_and_linear_function}
    Ist $F$ ein weiterer $\set K$-Banachraum und $A\colon E \to F$
    eine \emph{stetige} lineare Abbildung, so gilt
    \[
    A \circ f \in R([a, b], F)\quad\text{und}\quad
    \int_a^b (A \circ f) \, dx = A \Bigl(\int_a^b f \, dx\Bigr)
    \]
    für alle $f \in R([a, b], E)$.
  \end{enumerate}
\end{proposition*}

\begin{exercise}
  Seien $E_1$, \dots, $E_n$ Banachräume über $\set K$, und sei $E
  \coloneqq \prod_k E_k$ der Produktbanachraum (vgl.~Beispiel~\ref{ex:product_vector_space} aus
  Abschnitt~\ref{sec:normed_vector_spaces}).
  Dann ist eine Funktion
  \[
  f = (f_1, \dotsc, f_n)\colon [a, b] \to E
  \]
  genau dann eine
  Regelfunktion, wenn $f_k \in R([a, b], E_k)$ für alle $k = 1,
  \dotsc, n$.  In diesem Falle gilt
  \[
  \int_a^b f \, dx = \Bigl(\int_a^b f_1 \, dx, \dotsc, \int_a^b f_n \,
  dx\Bigr).
  \]

  Man leite hieraus her, wie sich das Integral einer Funktion $f = U +
  i V \in R([a, b], \set C)$ durch Integration der reellwertigen
  Funktionen
  $U$, $V \in R([a, b], \set R)$ berechnet.
\end{exercise}

\begin{exercise}[Produkte von Regelfunktionen]
  Es seien $E$, $E_1$ und $E_2$ Banachräume über $\set K$ und $B\colon
  E_1 \times E_2 \to E$ eine stetige bilineare Abbildung, und zwar
  gelte
  \[
  \forall u \in E_1, v \in E_2 \colon \anorm{B(u, v)} \leq M \cdot
  \anorm u \cdot \anorm v
  \]
  (vgl.~\ref{sec:bilinear_maps}).  Weiterhin sei $A$ eine nicht leere Menge.
  Dann gilt:
  \begin{enumerate}
  \item
    Sind $f \in B(A, E_1)$ und $g \in B(A, E_2)$, so ist
    \[
    \tilde B(f, g)\colon A \to E, p \mapsto B(f(p), g(p))
    \]
    eine beschränkte Funktion mit
    \[
    \norm\infty{\tilde B(f, g)} \leq M \cdot \norm\infty f \cdot
    \norm\infty g.
    \]
    Daher (?) ist
    \[
    \tilde B\colon B(A, E_1) \times B(A, E_2) \to B(A, E), (f, g)
    \mapsto \tilde B(f, g)
    \]
    eine stetige bilineare Abbildung.
  \item
    Sind $f \in R([a, b], E_1)$ und $g \in R([a, b], E_2)$, so auch
    $\tilde B(f, g) \in R([a, b], E)$.

    (Tip: Zuerst überlege man, daß $\tilde B(T([a, b], E_1) \times
    T([a, b], E_2)) \subseteq T([a, b], E)$.)
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Jede monotone Funktion $f\colon [a, b] \to \set R$ ist eine
  Regelfunktion.

  (Tip: Ist $f$ monoton wachsend, so zerlege man das Intervall $[f(a),
    f(b)]$ in Teilintervalle, die höchstens die Länge $\epsilon$
  haben, und definiere mit Hilfe einer geeigneten Zerlegung von $[a,
    b]$ eine derartige Treppenfunktion $\phi$, so daß $\norm \infty {f
    - \phi} \leq \epsilon$ gilt.)
\end{exercise}

\begin{exercise}
  Seien $E$ und $F$ zwei $\set K$-Banachräume, $f \in R([a, b], E)$
  eine Regelfunktion, $D \in \powerset E$ eine Teilmenge von $E$ mit
  $f([a, b]) \subseteq D$ und $g\colon D \to F$ eine Funktion, die
  auf $f([a, b])$ gleichmäßig stetig ist.  Dann ist auch $g \circ f$
  eine Regelfunktion.
\end{exercise}

\section{Integration stetiger Funktionen}

\begin{theorem*}
  Der $\set K$-Vektorraum $C([a, b], E)$ aller stetigen Funktionen $f\colon [a, b] \to E$ ist
  ein Untervektorraum von $R([a, b], E)$.

  Ist $f \in C([a, b], E)$, so existiert zu jedem
  $\epsilon \in \set R_+$ ein $\delta \in \set R_+$, so daß für jede
  beliebige Zerlegung $(t_k)_{k = 0, \ldots, n}$ von $[a, b]$ mit
  $t_k - t_{k - 1} \leq \delta$ für $k = 1$, \dots, $n$ und für jede
  Folge $(\xi_k)_{k = 1, \ldots, n}$ von Parametern
  $\xi_k \in \left]t_{k - 1}, t_k\right[$ gilt, daß
  \[
  \anorm{\int_a^b f\, dx - \sum_{k = 1}^n f(\xi_k) \cdot (t_k - t_{k - 1})} \leq \epsilon.
  \]

  Die in der letzten Abschätzung stehende Summe heißt eine \emph{Riemannsche Summe}.
\end{theorem*}

\begin{exercise}
  Ist $f \in C([a, b], E)$ mit
  $\int_a^b \anorm f \, dx = \int_a^b \anorm{f(t)} \, dt = 0$,
  so ist $f \equiv 0$.
\end{exercise}

\begin{comment*}
  Regelfunktionen sind genau jene Funktionen, die überall einen links- und rechtsseitigen
  Grenzwert besitzen; vgl.~M.~Barner, F.~Flohr: \emph{Analysis I}, 4.~Aufl.~1991, de Gruyter.
  Für den Begriff des einseitigen Grenzwertes vgl.~Kapitel~\ref{chap:limits}.
\end{comment*}

\begin{namedthm}{Mittelwertsatz der Integralrechnung}
  Sei $f\colon [a, b] \to \set R$ eine stetige Funktion, und sei $g\colon [a, b] \to \set R$ eine
  \emph{nirgends negative} Regelfunktion.  Dann existiert ein $t_0 \in [a, b]$ mit
  \[
  \int_a^b f g \, dx = f(t_0) \cdot \int_a^b g \, dx.
  \]
\end{namedthm}

Interpretieren wir $g$ als die Dichte einer Massenverteilung auf
$[a, b]$, so ist $\int_a^b g \, dx$ die Gesamtmasse der Verteilung.

\begin{exercise}
  Die Funktion
  \[
  R([a, b], \set R) \times R([a, b], \set R) \to \set R, (f, g) \mapsto
  \langle f, g\rangle \coloneqq \int_a^b f g \, dx
  \]
  ist symmetrisch, bilinear und \emph{positiv semidefinit}, d.\,h.
  \[
  \forall f \in R([a, b], \set R)\colon \langle f, f\rangle \ge 0.
  \]

  Der letzten Ungleichung wegen können wir
  \[
  \norm 2 f \coloneqq \sqrt{\langle f, f\rangle}
  \]
  definieren.  Es gilt die \emph{Cauchy--Schwarzsche-Ungleichung}:
  \[
  \forall f, g \in R([a, b], \set R)\colon \abs{\langle f, g\rangle} \leq
  \norm 2 f \cdot \norm 2 g.
  \]
  (Tip: Man werte $0 \leq ({\norm 2 {f - \lambda g}})^2$ für $\lambda = \langle f, g\rangle \cdot t$
  für $t \in \set R$ aus.)

  % FIXME: Referenzen
  Daher (?) gelten die Norm-Axiome (N0), (N2) und (N3). Schränken wir $\langle \cdot, \cdot\rangle$
  beziehungsweise $\norm 2 \cdot$ auf den $\set R$-Vektorraum $C([a, b], \set R)$ der stetigen
  Funktionen $f\colon [a, b] \to \set R$ ein, so erhalten wir ein Skalarprodukt bzw.~eine Norm.

  Im Gegensatz zu $(C([-1, 1], \set R), \norm \infty \cdot)$ ist
  $(C([-1, 1], \set R), \norm 2 \cdot)$ kein Banachraum.
  (Tip: Man betrachte die Funktionenfolge $f_n\colon [-1, 1] \to \set R$ mit
  $f_n|[-1, -1/n] \equiv -1$, $f_n|[1/n, 1] \equiv 1$ und $f_n(t) = nt$ für $t \in \left]-1/n,
    1/n\right[$.)
\end{exercise}

\section{Der Raum $R(I, E)$}

\begin{namedthm}{Festsetzung}
  Ist $D \in \powerset{\set R}$ eine nicht leere Teilmenge von $\set R$ mit $[a, b] \subseteq D$
  und $f\colon D \to E$ eine Funktion mit $f|[a, b] \in R([a, b], E)$, so schreiben wir
  \[
  \int_a^b f \, dx \coloneqq \int_a^b f|[a, b] \, dx.
  \]
\end{namedthm}

\begin{proposition}
  Ist $[c, d]$ ein Teilintervall von $[a, b]$, so gilt
  \[
  \forall f \in R([a, b], E)\colon f|[c, d] \in R([c, d], E).
  \]
\end{proposition}

\begin{definition*}
  Der letzten Proposition wegen können wir für jedes Intervall $I \subseteq \set R$ definieren:
  \[
  R(I, E) \coloneqq \{f\colon I \to E \mid \forall a, b \in I\colon (a < b \implies
  f|[a, b] \in R([a, b], E))\}.
  \]

  Ist $f \in R(I, E)$ und sind $a$, $b \in I$ mit $a < b$, so ist also $\int_a^b f \, dx$
  definiert.  Desweiteren definieren wir
  \begin{align*}
    \int_a^a f \, dx & \coloneqq 0 \\
    \intertext{und}
    \int_b^a f \, dx \coloneqq - \int_a^b f\, dx.
  \end{align*}
\end{definition*}

Damit ist also im Falle $f \in R(I, E)$ für alle $c$, $d \in I$ das Integral $\int_c^d f\, dx$
eingeführt.

\begin{proposition}
  Für alle $a$, $b$, $c \in I$ und alle $f \in R(I, E)$ gilt
  \[
  \int_a^c f \, dx = \int_a^b f \, dx + \int_b^c f \, dx.
  \]
\end{proposition}

\begin{theorem*}
  Für jedes Intervall $I$ gilt $C(I, E) \subseteq R(I, E)$.
\end{theorem*}

\begin{exercise*}
  Für jede Funktion $f \in R(I, E)$ und jeden Parameter $a \in I$ ist die Funktion
  \[
  \int_a^x f \, dx\colon I \to E, t \mapsto \int_a^t f \, dx
  \]
  stetig.
\end{exercise*}

\section{Der Hauptsatz der Differential- und Integralrechnung}

\begin{definition*}
  Eine Funktion $F\colon D \to E$ auf einer Teilmenge
  $D \in \powerset{\set K}$ heißt eine \emph{Stammfunktion} einer
  Funktion $f\colon D \to E$, wenn $F$ in allen Punkten von $D$
  differenzierbar ist und wenn $F' \equiv f$ gilt.
\end{definition*}

\begin{proposition*}
  Ist $I \in \powerset{\set R}$ ein nicht entartetes Intervall und $F$
  eine Stammfunktion einer Funktion $f\colon I \to E$, so ist $\{F + c
  \mid c \in E\}$ die Gesamtheit aller Stammfunktionen von $f$.
\end{proposition*}

\begin{namedthm}{Hauptsatz der Differential- und Integralrechnung}
  Seien $I \in \powerset{\set R}$ ein Intervall von $\set R$, $E$ ein
  $\set K$-Banachraum und $f\colon I \to E$ eine \emph{stetige}
  Funktion.  Dann gilt:
  \begin{enumerate}
  \item Für jedes $a \in I$ ist die Funktion
    \[
    \int_a^x f \, dx\colon I \to E
    \]
    eine Stammfunktion von $f$.
  \item
    Ist $F\colon I \to E$ eine Stammfunktion von $f$, so gilt für alle
    $a$, $b \in I$, daß
    \[
    \int_a^b f \, dx = F\big|_a^b \coloneqq F(b) - F(a).
    \]
  \end{enumerate}
\end{namedthm}

\begin{exercise*}
  Seien $I$, $J \in \powerset{\set R}$ Intervalle, $\phi$,
  $\psi\colon I \to \set R$ differenzierbare Funktionen mit $\phi(I)$, $\psi(I) \subseteq J$
  und $f\colon J \to E$ eine stetige Funktion.  Dann ist
  \[
  g\colon I \to E, t \mapsto \int_{\phi(t)}^{\psi(t)} f \, dx
  \]
  eine differenzierbare Funktion.  Wie lautet ihre Ableitung?
\end{exercise*}

\section{Partielle Integration}
\label{sec:part_integration}

\begin{theorem*}
  Seien $E_1$, $E_2$ und $F$ Banachräume über $\set K$,
  $B\colon E_1 \times E_2 \to F$ eine stetige, bilineare Abbildung
  (vgl.\ Abschnitt~\ref{sec:bilinear_maps}), $I$ ein nicht entartetes
  Intervall und $f\colon I \to E_1$ und $g\colon I \to E_2$ stetig
  differenzierbare Funktionen.  Dann gilt für alle $a$, $b \in I$, daß
  \[
  \int_a^b B(f, g') \, dx = B(f, g)\big|_a^b - \int_a^b B(f', g) \, dx.
  \]
\end{theorem*}

\section{Substitutionsmethode}

\begin{theorem*}
  Seien $I$ und $J$ nicht entartete Intervalle, $f\colon I \to E$ eine
  stetige und $\phi\colon J \to \set R$ eine stetig differenzierbare
  Funktion mit $\phi(J) \subseteq I$.  Dann gilt für alle $a$, $b \in
  J$, daß
  \[
  \int_a^b (f \circ \phi) \cdot \phi' \, dx = \int_{\phi(a)}^{\phi(b)}
  f \, dx.
  \]
\end{theorem*}

\begin{exercise*}
  Es sei $f\colon [a, b] \to \set R$ eine injektive,
  stetig differenzierbare Funktion.  Dann ist $J \coloneqq f([a, b])$ ein
  abgeschlossenes Intervall von $\set R$, auf $J$ ist die Umkehrfunktion $\check f$
  definiert, und es gilt
  \[
  \int^{f(b)}_{f(a)} \check f \, dx = x \cdot f \big|_a^b - \int_a^b f \, dx.
  \]
\end{exercise*}

\section{Integration rationaler Funktionen}

Seien $P$ und $Q \not\equiv 0$ reelle Polynomfunktionen.  Aufgrund des Fundamentalsatzes der
Algebra (vgl.~Abschnitt~\ref{sec:bilinear_maps}) können wir $Q$ als Produkt seiner \emph{irreduziblen} Faktoren
wie folgt darstellen:
\[
Q = \gamma \cdot \prod_i (x - a_i)^{n_i} \cdot \prod_j (x^2 + \alpha_j x + \beta_j)^{m_j},
\]
wobei $\gamma$, $a_i$, $\alpha_j$, $\beta_j \in \set R$,
$\alpha_j^2 - 4 \beta_j < 0$ und $n_i$, $m_j \in \set N_1$.
Infolgedessen läßt sich die rationale Funktion $P/Q$ als Summe einer
Polynomfunktion (die im Falle $\deg P < \deg Q$ identisch
verschwindet) und rationaler Funktionen folgender Typen
\[
\frac{A}{(x - a_i)^n}\quad\text{und}\quad
\frac{B x + C}{(x^2 + \alpha_j x + \beta_j)^m},
\]
wobei $1 \leq n \leq n_i$ und $1 \leq m \leq m_j$ und
$A, B, C \in \set R$, darstellen.  Diese Darstellung von $P/Q$ als
Summe solcher "`einfachen Brüche"' heißt
\emph{Partialbruchzerlegung}.  Die Koeffizienten $A$, $B$ und $C$
erhalten wir durch formalen Ansatz und Koeffizientenvergleich.

Für die Funktionen des Typs $A/(x - a)^n$ sind Stammfunktionen wohlbekannt.

Für die Funktionen des Typs $(B x + C) / (x^2 + \alpha x + \beta)^m$ erhalten wir
Stammfunktionen mittels der folgenden Aussage:

\begin{proposition}
  Definieren wir rekursiv
  \begin{align*}
    F_1 & \coloneqq \arctan \\
    \intertext{und}
    F_{n + 1} & \coloneqq \frac 1 {2n} \Bigl((2n - 1) \cdot F_n + \frac x {(1 + x^2)^n}\Bigr)
  \end{align*}
  für alle $n \in \set N_1$,
  so gilt:
  \begin{enumerate}
  \item
    Für jedes $n \in \set N_1$ ist $F_n$ eine Stammfunktion von $1/(1 + x^2)^n$.
  \item
    \label{it:partial_b}
    Ist $x^2 + \alpha x + \beta$ ein über $\set R$ irreduzibles Polynom (d.\,h.~hat dieses
    Polynom keine reelle Nullstelle), so ist
    \[
    \gamma\coloneqq \frac 1 2 \sqrt{4 \beta - \alpha^2} \in \set R_+,
    \]
    und für jedes $n \in \set N_1$ ist
    \[
    \gamma^{1 - 2 n} \cdot F_n\Bigl(\frac{x + \frac \alpha 2} \gamma\Bigr)
    \]
    eine Stammfunktion von $1/(x^2 + \alpha x + \beta)^n$.
  \item
    \label{it:partial_c}
    Im Falle $m = 1$ ist $\ln{\abs{x^2 + \alpha x + \beta}}$, im Falle $m > 1$ ist
    $-1/((m - 1) \cdot (x^2 + \alpha x + \beta)^{m - 1})$ eine Stammfunktion von
    $(2 x + \alpha)/(x^2 + \alpha x + \beta)^m$.
  \end{enumerate}
\end{proposition}

Eine Stammfunktion von $(B x + C)/(x^2 + \alpha x + \beta)^m$ läßt sich durch geeignete
Linearkombinationen der Funktionen aus~\ref{it:partial_b}
und~\ref{it:partial_c} erhalten.

Nachdem wir prinzipiell in der Lage sind, für jede (reelle)
rationale Funktion eine Stammfunktion zu ermitteln, können wir
jetzt auch weitere Stammfunktionen berechnen:
\begin{proposition}
  \leavevmode
  \begin{enumerate}
  \item
    Ist $R$ eine rationale Funktion (einer Variablen),
    $\alpha \in \set C^*$ und $F$ eine Stammfunktion der
    rationalen Funktion
    $\displaystyle u \mapsto \alpha^{-1} \cdot R(u)/u$, so ist
    $\displaystyle F(e^{\alpha x})$ eine Stammfunktion von $R(e^{\alpha x})$.
  \item Ist
    $R$ eine rationale Funktion zweier Variablen und
    $F$ eine Stammfunktion der rationalen Funktion
    \[
    \frac 2 {1 + x^2} \cdot
    R\Bigl(\frac{1 - x^2}{1 + x^2}, \frac{2 x}{1 + x^2}\Bigr),
    \]
    so ist $F(\tan(x/2))$ eine Stammfunktion
    von $R(\cos, \sin)$ auf $\left]-\pi, \pi\right[$.

    \begin{small}
      \begin{proof}
        Mit der \emph{Universalsubstitution} $u \coloneqq \tan(x/2)$ gilt:
        $\cos|\left]-\pi, \pi\right[ = (1 - u^2)/(1 + u^2)$, $\sin|\left]-\pi, \pi\right[ =
        2 u / (1 + u^2)$ und $u' = (1 + u^2)/2$, woraus sich die
        Behauptung ergibt.
      \end{proof}
    \end{small}
  \end{enumerate}
\end{proposition}

%% TODO: Hinweis auf Bronstein

\section{Die Simpsonsche Regel}

\label{sec:simpson}

%% TODO: Hinweis auf Risch-Algorithmus

Wir haben mit den Integrationsmethoden der vorangegangenen Abschnitte
wertvolle Hilfsmittel zur Berechnung von Integralen an die Hand
bekommen.  Diese werden durch die Tabellen von Stammfunktionen in
einschlägigen Handbüchern ergänzt.  Trotzdem lassen sich die wenigsten
Funktionen explizit integrieren.  Zwar sagt der Hauptsatz der
Differential- und Integralrechnung, daß jede stetige Funktion über
einem Intervall eine Stammfunktion besitzt.  Diese ist jedoch in der
Regel nicht mehr durch elementare Funktionen ausdrückbar.  Beispiele
hierfür sind die Funktionen $\exp(-x^2)$, welche in der
Wahrscheinlichkeitsrechnung wichtig ist (Normalverteilung, Gaußsches
Fehlerintegral), und $\sqrt{a + b \sin^2}$, welche bei der Ermittlung
der Bogenlänge eines Ellipsenbogens auftaucht (sogenannte elliptische
Integrale).  In diesen Fällen können wir aber immer zu numerischen
Integrationsmethoden greifen.  Die \emph{Simpsonsche Regel} ist eine
einfache, aber recht gute derartige Methode.

\begin{proposition*}
  Seien $a$, $b$, $y_1$, $y_2$, $y_3 \in \set R$, und es gelte $a <
  b$.
  \begin{enumerate}
  \item
    Ist $P$ das \emph{Interpolationspolynom} vom Grad höchstens $2$ zu
    den Stützpunkten $(a, y_1)$, $((a + b)/2, y_2)$ und $(b, y_3)$,
    d.\,h.~ist
    \[
    P(a) = y_1,\quad P((a + b)/2) = y_2\quad\text{und}\quad P(b) =
    y_3,
    \]
    so gilt
    \[
    \int_a^b P \, dx = (y_1 + 4 y_2 + y_3) \frac h 3
    \]
    mit $h \coloneqq (b - a)/2$.
  \item
    Ist $f \in C([a, b], \set R)$, so konvergiert die Folge der Zahlen
    \[
    s_n \coloneqq \sum_{k = 1}^n (f(\xi_{2k - 2}) + 4 f(\xi_{2k -
      1}) + f(\xi_{2k})) \cdot \frac h 3
    \]
    mit $h \coloneqq (b - a)/(2n)$  und $\xi_k \coloneqq a + k \cdot
    h$ für $n \to \infty$ gegen $\int\limits_a^b f \, dx$.
  \end{enumerate}
\end{proposition*}

\begin{small}
  Die folgende Scheme-Prozedur berechnet die Folgenglieder $s_n$ zu
  gegebener Funktion $f$ und Intervallgrenzen $a$ und $b$:
  \begin{lstlisting}[gobble=4,basicstyle=\ttfamily,keywordstyle=\bfseries,
    alsodigit=',
    alsoletter=*+?-<>,
    otherkeywords={'},
    morekeywords={define,car,cdr,cons,force,delay,let,if,quotient,zero?,-,<,+,*,/,=}]
    (define (simpson f a b n)
      (let ((h (* 1/2 (- b a) (/ n))))
        (let loop ((k n)
                   (x a)
                   (y (f a))
                   (s 0))
          (if (zero? k)
              (* 1/3 s h)
              (let ((z (f (+ x (* 2 h)))))
                (loop (- k 1)
                      (+ x (* 2 h))
                      z
                      (+ s y (* 4 (f (+ x h))) z)))))))
  \end{lstlisting}

  Der folgende Ausdruck etwa liefert (mit $a = 0$, $b = 2 \pi$, $f =
  \sqrt{4 + 5 \sin^2}$, $n = 10$) approximativ den Umfang einer
  Ellipse mit den Halbachsen $2$ und $3$:
  \begin{lstlisting}[gobble=4,basicstyle=\ttfamily,keywordstyle=\bfseries,
    alsodigit=',
    alsoletter=*+?-<>,
    otherkeywords={'},
    morekeywords={define,lambda,atan,sin,force,delay,let,if,quotient,zero?,-,<,+,*,/,=}]
    (simpson (lambda (t) (sqrt (+ 4 (* 5 (sin t) (sin t)))))
             0
             (* 8 (atan 1))
             10)
  \end{lstlisting}
\end{small}

\section{Stetige und differenzierbare Abhängigkeit des Integrals von
  einem Parameter}

\label{sec:partial_dim1}

\begin{namedthm}{Festsetzung}
  Sind $M$, $M'$ und $M''$ Mengen und ist $f\colon M' \times M'' \to
  M$ eine Abbildung, so setzen wir für jedes Paar $(p, q) \in M'
  \times M''$:
  \[
  f_p(q) \coloneqq f^q(p) \coloneqq f(p, q).
  \]
  Durch Variation des Paares $(p, q)$ werden dadurch für jedes $p \in
  M'$ bzw.~$q \in M''$ Abbildungen
  \[
  f_p\colon M'' \to M\quad\text{bzw.}\quad f^q\colon M' \to M
  \]
  definiert.
\end{namedthm}

\begin{lemma*}
  Seien $K$, $E$ und $E'$ metrische Räume, $q_0 \in E$ und $f\colon K
  \times E \to E'$ eine Abbildung, die in allen Punkten $(p, q_0)$ mit
  $p \in K$ stetig ist.  Ist der Raum $K$ folgenkompakt, so gilt:
  \[
  \forall \epsilon \in \set R_+ \,
  \exists \delta \in \set R_+\,
  \forall (p, q) \in K \times U_\delta(q_0)\colon
  d'(f(p, q), f(p, q_0)) < \epsilon.
  \]
\end{lemma*}

\begin{theorem*}
  Seien $a$, $b \in \set R$ mit $a < b$, $E$ ein metrischer Raum, $E'$
  ein Banachraum, $f\colon [a, b] \times E \to E'$ eine Funktion und
  $p_0 \in E$.  Wir setzen $f^p \in R([a, b], E')$ für alle $p \in E$
  voraus.  Dann gelten
  für die Funktion
  \[
  F\colon E \to E', p \mapsto \int_a^b f(t, p) \, dt =
  \int_a^b f^p \, dx
  \]
  folgende Aussagen:
  \begin{enumerate}
  \item
    Ist $f$ in allen Punkten $(t, p_0)$ mit $t \in [a, b]$ stetig, so
    ist auch $F$ in $p_0$ stetig.  Ist insbesondere $f$ überall
    stetig, so ist auch $F$ überall stetig.
  \item
    Sei jetzt $E$ ein Intervall $I \in \powerset{\set R}$, und für
    jedes $t \in [a, b]$ sei die Funktion $f_t\colon I \to E'$
    differenzierbar.  Dann können wir für alle $(t, s) \in [a, b]
    \times I$ die sogenannte \emph{partielle} Ableitung
    \[
    \frac{\partial f}{\partial s}(t, s) \coloneqq f_t'(s)
    \]
    von $f$ bezüglich (der ersten Variablen) $s$ definieren.  Sei $s_0
    \in I$ eine derartige Stelle, so daß die partielle Ableitung
    $\frac{\partial f}{\partial s}$ in allen Punkten $(t, s_0)$ mit $t
    \in [a, b]$ stetig ist.  Insbesondere ist dann $\frac{\partial
      f}{\partial s}(x, s_0)$ stetig und somit eine Regelfunktion.
    Außerdem --- und das ist die entscheidende Aussage --- ist die
    Funktion $F$ in $s_0$ differenzierbar, und ihre Ableitung ist
    \[
    F'(s_0) = \int_a^b \frac{\partial f}{\partial s}(t, s_0) \, dt.
    \]
  \end{enumerate}
\end{theorem*}

In den Beweis der letzten Aussage geht wesentlich das Theorem von der
kontrollierten Schwankung ein.  Das diesbezügliche Argument wird in
der folgenden Aufgabe auch noch einmal aufgegriffen:

\begin{exercise*}
  Seien $I \in \powerset{\set R}$ ein Intervall, $a \in I$, $E$ ein
  $\set K$-Banachraum und $f\colon I \to E$ eine stetige Funktion, die
  auf $I \setminus \{a\}$ differenzierbar ist.  Dann gilt:
  \begin{enumerate}
  \item
    Existieren ein $v \in E$ und ein $L \in \left[0, \infty\right[$, so daß
    \[
    \forall t \in I \setminus \{a\}\colon \anorm{f'(t) - v} \leq L
    \]
    gilt, so ist
    \[
    \forall t \in I\colon \anorm{f(t) - f(a) - (t - a) \cdot v} \leq L
    \cdot \abs{t - a}.
    \]
  \item
    Konvergiert für $t \to a$ die Ableitung $f'(t)$ gegen einen Vektor
    $v \in E$, so ist $f$ auch in $a$ differenzierbar, und es gilt
    $f'(a) = v$.
  \item
    Sei $b \in I$ mit $a < b$, sei $f$ auf ganz $I$ differenzierbar
    und sei die Ableitung $f'$ in allen $s \in [a, b]$ stetig.  Dann
    gilt
    \begin{multline*}
    \forall \epsilon \in \set R_+ \, \exists \delta \in \set R_+\,
    \forall (t, s) \in I \times [a, b]\colon \\
    \Bigl(0 < \abs{t - s} \leq \delta \implies
    \anorm{\frac{f(t) - f(s)}{t - s} - f'(s)} \leq \epsilon\Bigr).
    \end{multline*}

    Diese Eigenschaft wird als \emph{gleichmäßige Differenzierbarkeit}
    von $f$ in $[a, b]$ bezeichnet.

    (Tip: $\anorm{f'(t) - f'(s)} \leq \epsilon$?)
  \end{enumerate}
\end{exercise*}

\section{Flächeninhalte und Volumina}
\label{sec:volumes}

Ist $\phi\colon [a, b] \to \set R$ eine nicht negative
Treppenfunktion, so ist offenbar $I(\phi)$ als Summe der
Flächeninhalte von endlich vielen Rechtecken der Inhalt der Fläche
zwischen dem Graphen von $\phi$ und der $x$-Achse.

Ist $f\colon [a, b] \to \set R$ eine nicht negative stetige Funktion
und $(\phi_n)$ eine Folge nicht negativer Treppenfunktionen $\phi_n
\in T([a, b], \set R)$, die gleichmäßig gegen $f$ konvergiert, so
konvergieren die Flächeninhalte $I(\phi_n)$ gegen $\int\limits_a^b f
\, dx$, weswegen es statthaft ist, dieses Integral als den Inhalt der
Fläche zwischen dem Graphen von $f$ und der $x$-Achse zu
\emph{bezeichnen}.

Warum "`bezeichnen"'? \emph{Ist} es nicht der Flächeninhalt? Nun,
bisher haben wir keine Definition für Flächeninhalte, deswegen müssen
wir ein bißchen vorsichtig formulieren.

Diese Idee soll anhand der folgenden Aufgaben illustriert werden:

\begin{exercise}
  Seien $\alpha_0$, $\alpha_1 \in \set R$ mit $\alpha_0 < \alpha_1
  \leq \alpha_0 + 2 \pi$, $r \colon [\alpha_0, \alpha_1] \to \set R_+$
  eine
  stetige Funktion und $\gamma$ die Kurve
  \[
  \gamma\colon [\alpha_0, \alpha_1] \to \set C \cong \set R^2, \alpha
  \mapsto r(\alpha) \cdot e^{i \alpha}.
  \]
  Es soll der Flächeninhalt $F$ der durch $\gamma$ und die
  Verbindungsstrecken vom Ursprung zum Anfangs- bzw.~Endpunkt von
  $\gamma$ begrenzten Fläche berechnet werden.  Hierzu approximiere
  man $F$ durch Kreissektoren. % TODO: Skizze.
  Man begründe, daß man auf diese Weise
  \[
  F = \frac 1 2 \int_{\alpha_0}^{\alpha_1} r^2(\alpha) \, d\alpha
  \]
  erhält.

  (Tip: Ein Segment eines Kreises vom Radius $R$, welches einen Winkel
  $\phi$ (im Bogenmaß gemessen) einschließt, hat den Flächeninhalt
  $\phi/2 \cdot R^2$.)
\end{exercise}

\begin{exercise}
  Seien $a$, $b \in \set R$ mit $a < b$ und $\rho\colon [a, b] \to
  \set R_+$ eine stetige Funktion.  Durch Rotation der Kurve
  $(\rho(x), 0, x)$ um die $z$-Achse erhalten wir eine Fläche, die den
  Rotationskörper
  \[
  K \coloneqq \{(r \cdot \cos \phi, r \cdot \sin \phi, t) \mid t \in [a, b], 0
  \leq r \leq \rho(t), \phi \in \left[0, 2 \pi\right[\}
  \]
  begrenzt.  Durch Ausschöpfung des Rotationskörpers durch
  zylindrische Scheiben senkrecht zur $z$-Achse % TODO: Skizze
  leite man eine Formel zur Berechnung des Volumens von $K$ her.

  Mit Hilfe dieser Formel berechne man das Volumen $V(R)$ einer Kugel
  mit Radius $R$ zu $\frac {4 \pi} 3 \cdot R^3$.  Man begründe auch,
  daß die Oberfläche dieser Kugel durch $V'(R) = 4 \pi \cdot R^2$
  gegeben ist.
\end{exercise}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "skript"
%%% End:
