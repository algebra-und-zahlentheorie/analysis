(define (div-poly p1 p2)
  (if (variable=? (variable p1) (variable p2))
      (call-with-values
	  (lambda ()
	    (div-terms (term-list p1)
		       (term-list p2)))
	(lambda (quotient-terms remainder-terms)
	  (values (make-poly (variable p1) (quotient-terms))
		  (make-poly (variable p2) (remainder-terms)))))
      (error "div-poly: polynomials are not in the same variable"
	     p1 p2)))

(define (div-terms l1 l2)
  (if (empty-termlist? l1)
      (values (the-empty-termlist) (the-empty-termlist))
      (let ((t1 (first-term l1))
	    (t2 (first-term l2)))
	(if (> (order t2) (order t1))
	    (values (the-empty-termlist) l1)
	    (let ((new-c (div (coeff t1) (coeff t2)))
		  (new-o (- (order t1) (order t2))))
	      (let ((first-term (make-term new-c new-o)))
		(call-with-values
		    (lambda ()
		      (div-terms (sub-terms l1
					    (mul-term-by-all-terms first-term l2))
				 l2))
		(lambda (quotient remainder)
		  (values (adjoin-term first-term quotient)
			  remainder)))))))))
