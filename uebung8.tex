\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2021/22}

\title{\bfseries 8.~Übung zur Analysis I}
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Dr.\ Ingo Blechschmidt}
\date{7.~Dezember 2021\thanks{Die Übungsblätter sind bis
    zur Übung am 14.~Dezember 2021 zu bearbeiten.}}

\begin{document}

\maketitle

\begin{enumerate}[start=44]

\item \writtenex \textbf{Archimedes bei den Pyramiden.}  Anläßlich
  eines Forschungsaufenthaltes in Alexandria besuchte Archimedes auch
  die Pyramiden von Gizeh.  Da er damals eingehende Untersuchungen an
  Dreiecken anstellte, trug er stets ein solches mit sich herum.  Ganz
  zufällig bemerkte er, daß der Schatten seines Dreieckes auf einer
  der Pyramidenflächen gleichseitig war, obwohl sein Dreieck doch eine
  ganz andere Form hatte.  Wie üblich setzte sich Archimedes in den
  Sand, um den Hintergrund dieses Phänomens zu ergründen. "`Heureka!
  Ich kann jedes Dreieck so halten, daß sein Schatten auf der Pyramide
  gleichseitig ist."' (Wir sollten hinzufügen, daß die Sonne zu diesem
  Zeitpunkt senkrecht zur Pyramidenfläche stand.)  Später wurde im
  Sand eine Steinplatte mit dem folgenden Bild eines gleichseitigen
  Prismas ($s \leq a \leq b \leq c$) gefunden:
  \begin{center}
    \begin{tikzpicture}[scale=2]
      \coordinate (A) at (0, 1.5);
      \coordinate (B) at (1.5, 1.5);
      \coordinate (C) at (0.75, 1);

      \coordinate (D) at (0, 0);
      \coordinate (E) at (1.5, 0);
      \coordinate (F) at (0.75, -0.5);

      \coordinate (U) at (0, 0.2);
      \coordinate (V) at (0.75, 0.4);
      \coordinate (W) at (1.5, 1.1);
      \coordinate (X) at (0, 0.4);

      \draw[->] (U) node[left] {$0$} -- node[below] {$a$} (V);
      \draw[->] (V) -- node[below] {$b$} (W);
      \draw[->, dashed] (W) -- node[above, pos=0.67] {$c$} (X) node[left] {$f(s)$};

      \draw (A) -- node[above] {$s$} (B) -- node[below] {$s$} (C) -- node[below] {$s$} (A);
      \draw [dashed] (D) --(E);
      \draw (E) -- (F) -- (D);
      \draw (A) -- (D);
      \draw (B) -- (E);
      \draw (C) -- (F);

    \end{tikzpicture}
  \end{center}

  Beweise Archimedes' Aussage!

  (Tip: Der Zwischenwertsatz ist nützlich.  Wie lautet die Funktion $f$?)

\item \oralex Zeige, daß es keine surjektive stetige Funktion
  $f\colon \set R \to \set R$ gibt, welche jeden Funktionswert genau
  zweimal annimmt.

\item \oralex
  Sei $M$ ein Teilraum eines metrischen Raumes $E$, $(p_n)_{n \ge m}$
  eine Punktfolge in $M$ und $q \in M$. Beweise:
  Die Folge $(p_n)_{n \ge m}$ konvergiert genau dann in dem Teilraum
  $M$ gegen $q$, wenn sie in $E$ gegen $q$ konvergiert.

\item \textbf{Weitere Rechenregeln für Zahlenfolgen.}
  Seien $(a_n)_{n \ge m}$, $(b_n)_{n \ge m}$, sowie $(c_n)_{n \ge m}$
  reelle Zahlenfolgen, $a$, $b \in \widehat{\set R}$, und es gelte
  $\displaystyle \lim_{n \to \infty} a_n = a$ und
  $\displaystyle \lim_{n \to \infty} b_n = b$. Zeige:
  \begin{enumerate}
  \item \oralex
    Ist $a = \infty$, so ist $\displaystyle\lim_{n \to \infty} 1/a_n = 0$.
  \item \oralex
    Ist $a = 0$ und $(c_n)_{n \ge m}$ eine beschränkte Zahlenfolge, so gilt auch
    $\displaystyle\lim_{n \to \infty} (a_n \cdot c_n) = 0$.
  \item \writtenex
    Ist $a = \infty$ und existiert ein $\epsilon \in \set R_+$, so daß $\forall n \ge m\colon
    c_n \ge \epsilon$ gilt, so gilt auch $\displaystyle\lim_{n \to \infty} (a_n \cdot c_n) = \infty$.
  \item \writtenex
    Gilt $a_n \leq b_n$ für alle $n \geq m$, so gilt auch $a \leq b$.
  \item \writtenex
    Und schließlich:
    \paragraph{Das Sandwich-Theorem.}
      \leavevmode
      \begin{enumerate}
      \item Gilt $a_n \leq c_n \leq b_n$ für alle $n \ge m$ und ist
        $a = b$, so gilt auch $\displaystyle \lim_{n \to \infty} c_n = a$.
      \item Gilt $a_n \leq c_n$ für alle $n \ge m$ und ist $a = \infty$, so
        gilt $\displaystyle \lim_{n \to \infty} c_n = \infty$.
      \item
        Gilt $c_n \leq b_n$ für alle $n \ge m$ und ist $b = -\infty$, so gilt
        $\displaystyle \lim_{n \to \infty} c_n = -\infty$.
      \end{enumerate}
  \end{enumerate}

\item \oralex Sei $(E, d)$ ein metrischer Raum, und seien
  $(p_n)_{n \ge m}$, $(q_n)_{n \ge m}$ zwei Punktfolgen in $E$.
  Zeige: Konvergiert $(p_n)_{n \ge m}$ gegen $p \in E$ und ist
  $(d(p_n, q_n))_{n \ge m}$ eine Nullfolge, so konvergiert auch
  $(q_n)_{n \ge m}$ gegen $p$.

\item \oralex Seien $q \in \set R$, $n \in \set N_0$ und
  $s_n \coloneqq \sum_{k = 0}^n q^k$.  Zeige: Ist $\abs q < 1$, so
  konvergiert die Folge $(s_n)_{n \ge 0}$.  Wie lautet der Grenzwert?
  (Tip: Übungsaufgabe 41 (a).)

\item \writtenex
  Die Funktion $f\colon \left]0, \infty\right[ \to \set R$ sei definiert durch
  \[
  t \mapsto \begin{cases}
    0 & \text{für $t \in \set R \setminus \set Q$} \\
    \frac 1 q & \text{für $t = \frac p q$ mit teilerfremden $p$, $q \in \set N_1$}.
  \end{cases}
  \]
  Zeige, daß $f$ an den irrationalen Stellen stetig und an den
  rationalen Stellen unstetig ist.  (Tip zu irrationalem $t$: Zu
  $\epsilon \in \set R_+$ wähle $n \in \set N_1$ mit
  $\frac 1 n < \epsilon$.  Wieviele rationale Zahlen $p/q$ mit $q \leq n$ liegen in $\left]t - 1,
    t + 1\right[$?)

  \paragraph{Fazit.} Es gibt Funktionen, deren Stetigkeits- und
  Unstetigkeitsstellen jeweils dicht liegen.
\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
