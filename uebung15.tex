\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2021/22}

\title{\bfseries 15.~Übung zur Analysis I} \author{Prof.\ Dr.\ Marc\
  Nieper-Wißkirchen \and Dr.\ Ingo Blechschmidt}  \date{8.~Februar
  2022\thanks{Die Übungsblätter sind bis zum 1.~März 2022 zu bearbeiten und in den
    Analysisbriefkastem im Gebäude L1 zu werfen.  Die
    mündlichen Aufgaben sind für die erste Analysis-II-Übungsstunde
    vorzubereiten.}}

\begin{document}

\maketitle

\begin{enumerate}[start=91]

\item \oralex
  \label{ex:fix}
  Seien $D \in \powerset{\set K}$ eine nicht leere offene Teilmenge von $\set K$
  und $f\colon D \to \set K$ eine in $a \in D$ differenzierbare
  Funktion.  Wir setzen voraus, daß $a$ ein Fixpunkt von $f$ ist, und
  definieren $q \coloneqq (1 + \abs{f'(a)})/2$.  Zeige:
  \begin{enumerate}
  \item Ist $f'(a) \neq 1$, so ist $a$ ein \emph{isolierter Fixpunkt},
    d.\,h.\ $a$ ist ein isolierter Punkt der Menge $\Fix(f)$ aller
    Fixpunkte von $f$.  (Vergleiche die Definition (c) aus 6.2.)
  \item Ist $\abs{f'(a)} < 1$, also $0 < q < 1$, so existiert ein
    $\delta \in \set R_+$, so daß $U_\delta(a) \subseteq D$ und
    \[
    \forall t \in \dot U_\delta(a)\colon \abs{f(t) - a} < q \cdot \abs{t - a}
    \]
    (d.\,h.\ insbesondere $f(U_\delta(a)) \subseteq U_\delta(a)$).  Für jedes
    $t_0 \in U_\delta(a)$ konvergiert daher (?) die Banachfolge
    bezüglich $f$ mit Startpunkt $t_0$ gegen $a$.

    (Man nennt $a$ deshalb auch einen \emph{Attraktor} (oder
    \emph{anziehenden} Fixpunkt) von $f$.
  \item
    Ist $\abs{f'(a)} > 1$, also $1 < q$, so existiert ein $\delta \in \set R_+$, so daß
    $U_\delta(a) \subseteq D$ und
    \[
    \forall t \in \dot U_\delta(a)\colon \abs{f(t) - a} > q \cdot \abs{t - a}.
    \]
    Existiert zu $t_0 \in \dot U_\delta(a)$ die Banachfolge
    $(t_n)_{n \ge 0}$ bezüglich $f$ mit Startpunkt $t_0$, so verläßt
    diese daher (?) irgendwann $U_\delta(a)$.

    (Man nennt $a$ deshalb auch einen \emph{Repeller} (oder \emph{abstoßenden}
    Fixpunkt) von $f$.)
  \end{enumerate}

\item \oralex Seien $D \in \powerset{\set R}$ eine offene Teilmenge
  von $\set R$, $a \in D$ und $f\colon D \to \set R^n$ eine Funktion.
  Ist $f$ stetig auf $D$ und differenzierbar auf $D \setminus \{a\}$
  und gilt $\lim_a f' = v \in \set R^n$, so ist $f$ differenzierbar in
  $a$ mit $f'(a) = v$.

  (Tip: Behandle zunächst den Fall $n = 1$.)

\item \writtenex Seien $D \in \powerset{\set R}$ eine nicht leere
  Teilmenge von $\set R$, $a \in D$, $E$ ein $\set K$"~Banach\-raum und
  $f\colon D \to E$ eine Funktion.  Zeige: Ist $f$ in $a$
  differenzierbar und sind $(a_n)_{n \ge 0}$ und $(b_n)_{n \ge 0}$
  zwei Folgen in $D$ mit
  $\displaystyle \lim_{n \to \infty} a_n = a = \lim_{n \to \infty} b_n$ und
  \[
  \forall n \in \set N_0\colon (a_n < b_n \land a_n \leq a \leq b_n),
  \]
  so gilt
  \[
  \lim_{n \to \infty} \frac{f(b_n) - f(a_n)}{b_n - a_n} = f'(a).
  \]
  (Tip: Affine Approximation.)

\item \oralex Es sei ein beliebiges Dreieck mit den Eckpunkten $A$,
  $B$, $C \in \set R^2$ und den Seitenlängen $a$, $b$, $c$ gegeben.
  Für je zwei Punkte $P$, $Q \in \set R^2$ sei mit $\overline{PQ}$ die
  Strecke $\{P + t (Q - P) \mid t \in [0, 1]\}$ bezeichnet.

  Für jeden Punkt $E$ auf $\overline{AB}$ sei $D$ der Schnittpunkt
  zwischen $\overline{AC}$ und einer Parallelen zu $\overline{BC}$ durch
  $E$.  Mit $\Delta(E)$ bezeichnen wir das Dreieck mit den Eckpunkten $C$, $D$ und $E$.
  \begin{center}
    \begin{tikzpicture}
      \coordinate[label=left:$A$] (A) at (0,0);
      \coordinate[label=right:$B$] (B) at (7,0);
      \coordinate[label=above right:$C$] (C) at (5.5,4);
      \coordinate[label=below:$E$] (E) at ($(A) !0.55! (B)$);
      \coordinate[label=above left:$D$] (D) at (intersection of C--A
      and E--{$(E) + (-1.5,4)$});

      \draw (A) -- (B) -- (C) -- (A);
      \draw (D) -- (E) -- (C);

      \node at ($(E)+(0,2)$) {$\Delta(E)$};
    \end{tikzpicture}
  \end{center}
  Bestimme nun den Punkt $E$ so, daß die Fläche von $\Delta(E)$ maximal wird.

  (Tipp: $E(t) = A + t \cdot (B - A)$.  Benutze die Strahlensätze.)

\item \writtenex Eine Tagesetappe bei der Rallye Paris--Dakar führt
  durch die Wüste von Oase $A$ zu Oase $B$.  Aufgrund der
  Geländebeschaffenheit können die Teilnehmer oberhalb einer Geraden
  $g$ mit der Geschwindigkeit $v_1$ und unterhalb von $g$ mit der
  Geschwindigkeit $v_2$ fahren: Zeige: Der Zeitaufwand der Strecke im
  folgenden Bild ist minimal, wenn
  $v_1 \cdot \sin (\phi_2) = v_2 \cdot \sin(\phi_1)$.
  \begin{center}
    \begin{tikzpicture}
      \coordinate [label=above:$A$] (A) at (-3,2);
      \coordinate [label=below:$B$] (B) at (1,-2);

      \draw (A) -- (0,0) coordinate (O) -- (B);
      \draw (-4,0) -- (4, 0) node [label=below:$g$] {};
      \draw (0,3) coordinate (T) --(0,-3) coordinate (S);

      \draw pic ["$\phi_1$",draw,angle radius=1.5cm] {angle=T--O--A};
      \draw pic ["$\phi_2$",draw,angle radius=1.5cm,angle eccentricity=0.75] {angle=S--O--B};
    \end{tikzpicture}
  \end{center}
  Nach \textsc{Pierre de Fermat} (1601--1665) ist folgendes Prinzip
  bekannt: "`Ein Lichtstrahl, der unter vorgegebenen Nebenbedingungen
  von einem Punkt $A$ zu einem Punkt $B$ gelangen soll, schlägt immer
  den Weg ein, der die kürzeste Zeit erfordert."'

  Frage an die Physikstudenten:  Was hat Fermat mit Paris--Dakar zu tun?

\item \oralex
  \begin{enumerate}
  \item Ein Unternehmer will ins Eintopfgeschäft einsteigen.  Seine
    "`Erbsensuppe Spezial"' soll in zylindrischen Blechdosen (von
    vorgegebener Blechstärke) mit dem Volumen $V$ verkauft werden.
    Welche Maße müssen die Dosen haben, damit der Materialaufwand
    minimal wird?
  \item Hans Raser will sein neues Motorrad seiner Freundin Helga
    vorführen.  Ihr Haus liegt im Abstand $h$ von der geradlinig
    verlaufenden Landstraße mitten auf einer großen Wiese.  Auf der
    Landstraße fährt er $100\,\mathrm{km}/\mathrm{h}$ und auf der
    Wiese $40\,\mathrm{km}/\mathrm{h}$.  Wo muß Hans von der Straße
    auf die Wiese abbiegen, um möglichst schnell zu Helga zu kommen?
  \end{enumerate}

\item \writtenex \textbf{Das Newtonsche Nullstellenverfahren.}
  Seien $I$ ein nicht leeres, offenes Intervall in $\set R$,
  $a \in I$ und $f\colon I \to \set R$ eine differenzierbare
  Funktion, deren Ableitung keine Nullstelle besitzt.  Also ist $f$
  streng monoton (vgl.~6.10) und besitzt somit höchstens eine
  Nullstelle.  Es sei $g \coloneqq x - f/f'$.  Zeige:
  \begin{enumerate}
  \item
    \label{it:tangent}
    Für jedes $a \in I$ ist $g(a)$ die Nullstelle der affinen
    Approximation
    \[
    t \mapsto f(a) + f'(a) \cdot (t - a)
    \]
    von $f$ im Punkt $a$, deren Graph bekanntlich die Tangente an den Graphen
    von $f$ im Punkte $(a, f(a))$ ist.
  \item
    Es ist $a$ genau dann Nullstelle von $f$, wenn $a$ ein Fixpunkt von $g$ ist.
  \item
    Ist $a$ eine Nullstelle von $f$ und ist $f'$ in $a$ stetig, so ist
    $g$ in $a$ differenzierbar mit Ableitung $g'(a) = 0$, und somit (?) ist
    $a$ ein anziehender Fixpunkt von $g$ (vgl.~Aufgabe~\ref{ex:fix});
    insbesondere existiert also ein $r \in \set R_+$, so daß für jedes
    $t_0 \in U_r(a)$ die "`Newton-Folge"' $(t_n)_{n \ge 0}$ mit
    \[
    t_{n + 1} = t_n - \frac{f(t_n)}{f'(t_n)}
    \]
    gegen $a$ konvergiert.
  \item Das Heronsche Verfahren zur Ermittlung von Quadratwurzeln kann
    als Newtonsches Nullstellenverfahren für eine geeignete Funktion
    betrachtet werden.  Für welche?
  \item Ein Nachteil des Newtonschen Nullstellenverfahrens besteht
    darin, daß die Ableitung der zu untersuchenden Funktion bekannt
    sein muß.  Dieser Mangel läßt sich dadurch beheben, daß die
    Ableitung $f'(t_n)$ durch den Differenzenquotienten
    $(f(t_n) - f(t_{n - 1}))/(t_n - t_{n - 1})$ ersetzt wird.  In
    diesem Falle müssen natürlich zwei Startwerte $t_0$ und $t_1$
    vorgegeben werden.  Dadurch (?) gelangen wir zur rekursiven
    Definition
    \[
    t_{n + 1} = \frac{t_{n - 1} \cdot f(t_n) - t_n \cdot f(t_{n -
      1})}{f(t_n) - f(t_{n - 1})}
    \]
    für $n \ge 1$.  Das hierdurch beschriebene Nullstellenverfahren
    heißt das \emph{Sekantenverfahren}, oder auch die
    Nullstellenbestimmung nach der \emph{regula falsi}.  Zeige, daß in
    Analogie zu \ref{it:tangent} gilt: Das Folgeglied $t_{n + 1}$ wird
    durch den Schnittpunkt der $x$-Achse mit der Sekante durch die
    Punkte $(t_{n - 1}, f(t_{n - 1}))$ und $(t_n, f(t_n))$
    festgelegt.

    Mache Dir die Wirkungsweise des Newtonschen Nullstellenverfahrens
    und des Sekantenverfahrens an Skizzen klar.

  \item Prüfe das Sekantenverfahren an dem Polynom
    \[
    f = x^3 + \frac{11}{2} \, x^2 - 8\,x - 44.
    \]

  \item Schreibe ein (Scheme-)Programm zur Berechnung einer Nullstelle
    einer Funktion mit Hilfe des Newtonschen Nullstellenverfahrens.
  \end{enumerate}
\end{enumerate}

\begin{verse}
  Die Klausur findet am 19.~März 2022 in der Zeit von 8:30--11:00 Uhr
  im Sigma-Park.
  \\
  Wir wünschen allen Studentinnen und Studenten
  viel Erfolg bei der Klausur!
\end{verse}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
