\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2021/22}

\title{\bfseries 6.~Übung zur Analysis I}
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Dr.\ Ingo
  Blechschmidt}
\date{23.~November 2021\thanks{Die Übungsblätter sind
    bis zur Übung am 30.~November 2021 zu bearbeiten}}

\begin{document}

\maketitle

\begin{enumerate}[start=30]

\item
  Für $v = (v_i) \in \set R^n$ definieren wir
  \[
  \norm \infty v \coloneqq \max\{\abs{v_1}, \abs{v_2}, \ldots, \abs{v_n}\}\quad\text{und}\quad
  \norm 2 v \coloneqq \sqrt{\sum_{i = 1}^n v_i^2}.
  \]
  Offenbar ist dann
  \[
  d(u, v) \coloneqq \norm \infty {v - u}
  \]
  die in Abschnitt 3.3 eingeführte Metrik des $\set R^n$.  Durch
  \[
  \tilde d(u, v) \coloneqq \norm 2 {v - u}
  \]
  wird eine weitere Metrik des $\set R^n$, die sogenannte
  \emph{euklidische} Metrik definiert.  Die $\epsilon$-Umgebungen
  bezüglich $d$ und $\tilde d$ werden im folgenden mit $U_\epsilon(p)$
  bzw.\ $\tilde U_\epsilon(p)$ bezeichnet.  Zeige:
  \begin{enumerate}
  \item \oralex
    $\forall v \in \set R^n\colon c \norm 2 v \leq \norm \infty v \leq C \norm 2 v$
    mit $c \coloneqq 1/\sqrt n$ und $C \coloneqq 1$.

    Sind die beiden Abschätzungen scharf, d.\,h.~wird die Aussage
    falsch, wenn Du $c$ durch eine größere bzw.~$C$ durch eine
    kleinere Konstante ersetzt?
  \item \writtenex
    $\forall v \in \set R^n \, \forall \epsilon \in \set R_+\colon
    \tilde U_\epsilon(v) \subseteq U_\epsilon(v) \subseteq \tilde U_{\epsilon \sqrt n} (v)$.
  \item \writtenex
    Sei $E$ ein weiterer metrischer Raum.  Dann gilt:
    \begin{enumerate}
    \item Eine Abbildung $f\colon E \to \set R^n$ ist genau dann in
      $p_0 \in E$ bezüglich $d$ stetig, wenn $f$ in $p_0$ bezüglich
      $\tilde d$ stetig ist.
    \item Eine Abbildung $g\colon \set R^n \to E$ ist genau dann in
      $v_0 \in \set R^n$ bezüglich $d$ stetig, wenn $g$ in $v_0$
      bezüglich $\tilde d$ stetig ist.
    \end{enumerate}
  \end{enumerate}

\item \oralex
  Seien $M$ eine nicht leere Menge und
  \[
  B(M) \coloneqq \{f\colon M \to \set R\mid \text{$f$ ist beschränkt}\}.
  \]
  Dabei heißt $f$ \emph{beschränkt}, wenn eine Konstante $C \ge 0$ mit
  $\abs{f(p)} \leq C$ für alle $p \in M$ existiert.

  Für $f$, $g \in B(M)$ und $\alpha \in \set R$ definieren wir
  $f + g\colon M \to \set R$ und $\alpha f\colon M \to \set R$ durch
  \[
  (f + g)(p) = f(p) + g(p)\quad\text{und}\quad(\alpha f)(p) = \alpha \cdot f(p)
  \]
  für alle $p \in M$.
  Dadurch wird $B(M)$ offenbar zu einem reellen Vektorraum.

  Weiterhin definieren wir für jedes $f \in B(M)$:
  \[
  \norm \infty f \coloneqq \sup\{\abs{f(p)} \mid p \in M\}.
  \]
  Zeige, daß $\norm \infty \cdot$ eine \emph{Norm} auf $B(M)$ ist, das
  heißt, es gelten die folgenden "`Axiome"':
  \begin{axiomlist}[label=(N\arabic*),start=0]
  \item
    $\forall f \in B(M)\colon \norm \infty f \ge 0$,
  \item
    $\forall f \in B(M)\colon \left(f = 0 \iff \norm \infty f = 0\right)$,
  \item
    $\forall \alpha \in \set R \, \forall f \in B(M)\colon
    \norm \infty {\alpha f} = \abs \alpha \cdot \norm \infty f$,
  \item
    $\forall f, g \in B(M)\colon \norm \infty{f + g} \leq \norm \infty f + \norm \infty g$.
  \end{axiomlist}
  Daher (?) ist $d\colon B(M) \times B(M) \to \set R, (f, g) \mapsto \norm \infty {f - g}$ eine
  Metrik auf $B(M)$.

\item \writtenex 
  \begin{enumerate}
  \item
    Sei $E$ eine Menge.  Eine Funktion
    $d\colon E \times E \to \widehat{\set R}$ heißt eine
    \emph{Fastmetrik} auf $E$, wenn für $d$ die vier Axiome (M0)--(M3)
    aus Abschnitt 3.1 gelten.  Eine Fastmetrik $d$ auf $E$ ist offenbar genau dann eine Metrik auf
    $E$, wenn $d(p, q) < \infty$ für alle $p$, $q \in E$ gilt.
    Sei weiter $\Phi$ die Funktion
    \[
    \Phi\colon [0, \infty] \to [0, 1], t \mapsto \begin{cases}
      \frac t {1 + t} & \text{für $t \in \left[0, \infty\right[,$} \\
      1               & \text{für $t = \infty.$}
    \end{cases}
    \]
    Zeige: Ist $d\colon E \times E \to \widehat{\set R}$ eine Fastmetrik auf $E$, so ist
    $\Phi \circ d$ eine Metrik auf $E$.
  \item
    Sei $M$ eine nicht leere Menge und
    \[
    A(M) \coloneqq \{f\colon M \to \set R\}.
    \]
    Beweise: Die Abbildung
    \[
    d\colon A(M) \times A(M) \to \widehat{\set R}, (f, g) \mapsto
    \sup\{\abs{f(p) - g(p)} \mid p \in M\}
    \]
    ist eine Fastmetrik auf $A(M)$.
  \end{enumerate}
  
\item
  \begin{enumerate}
  \item
    \oralex Zeige: Die Funktion
    \[
    f\colon \set R \to \set R, t \mapsto \begin{cases}
      \sin(1/t) & \text{für $t \neq 0$} \\
      0 & \text{für $t = 0$}
    \end{cases}
    \]
    ist in $0$ nicht stetig.
    Ist $x \cdot f\colon \set R \to \set R$ in $0$ stetig?
  \item
    \writtenex In welchen Punkten ist die Funktion
    \[
    g\colon \set R \to \set R, t \mapsto \begin{cases}
      t & \text{für $t \in \set Q$} \\
      t^2 & \text{für $t \in \set R \setminus \set Q$}
    \end{cases}
    \]
    stetig?
  \end{enumerate}

\item \oralex \textbf{Stetigkeit ist eine lokale Eigenschaft.}
  Beweise: Eine Abbildung zwischen metrischen Räumen $E$ und $E'$,
  $f\colon E \to E'$, ist in
  $p_0 \in E$ stetig, wenn es ein $\epsilon \in \set R_+$ gibt, so daß
  $f|U_\epsilon(p_0)$ in $p_0$ stetig ist.

\item \oralex \textbf{Aneinanderheften stetiger Funktionen.}
  Es seien $a$, $b$, $c \in \set R$ Zahlen mit $a < b < c$, $E$ ein
  metrischer Raum, sowie $f\colon [a, b] \to E$ und
  $g\colon [b, c] \to E$ stetige Abbildungen mit $f(b) = g(b)$.
  Zeige, daß dann genau eine stetige Abbildung $h\colon [a, c] \to E$
  mit
  \[
  h|[a, b] = f\quad\text{und}\quad h|[b, c] = g
  \]
  existiert.

\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
