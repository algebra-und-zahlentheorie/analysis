\documentclass{uebung}

\newcommand{\semester}{Sommersemester 2022}

% 35 Punkte
\title{\bfseries 3.~Übung zur Analysis II}
\author{Prof.\ Dr.\ Marc\
  Nieper-Wißkirchen \and Lukas Stoll, M.\ Sc.}
\date{13.~Mai
  2022\thanks{Die Übungsblätter sind bis zur Übung am 20.~Mai 2022 zu bearbeiten.}}

\begin{document}

\maketitle

\begin{enumerate}[start=113]

\item
  \begin{enumerate}
  \item \oralex
    \textbf{Die Dirichletsche Reihe.}
    In Abschnitt~5.8 haben wir bereits die Dirichletsche Reihe
    $\sum\limits_{n = 1}^\infty \frac 1 {n^s}$ für $s \in \set N_1$
    betrachtet.  Jetzt können wir sie für alle $s \in \set C$
    definieren.  Für reelle $s$ untersuche die Reihe auf Konvergenz,
    und zwar zeige, daß sie für $s > 1$ in $\set R$ konvergiert und
    für $s \leq 1$ gegen $\infty$ konvergiert.

    (Tip: Cauchysches Verdichtungslemma aus Abschnitt~5.8.)
  \item \writtenex % 5 Punkte
    Für jedes $s \in \set R$ seien
    \begin{alignat*}{2}
      H_s & \coloneqq \{z \in \set C \mid \Re(z) \ge s\}
      \quad\text{und}\quad&
      H_s^o & \coloneqq \{z \in \set C \mid \Re(z) > s\}.
    \end{alignat*}
    Zeige, daß für jedes $z \in H \coloneqq H_1^o$ die Dirichletsche
    Reihe $\sum\limits_{n = 1}^\infty n^{-z}$ gegen eine Zahl
    $\zeta(z) \in \set C$ konvergiert, daß diese Konvergenz auf jeder
    Teilmenge $H_s$ mit $s > 1$ gleichmäßig ist und daß daher die
    Funktion $\zeta\colon H \to \set C$ stetig ist.  Diese Funktion
    (oder genauer ihre maximale analytische Funktion) heißt
    \emph{Riemannsche Zeta-Funktion}.

    (Tip: Man beachte den ersten Aufgabenteil und das Theorem aus Abschnitt 7.1.)
  \end{enumerate}

\item \writtenex % 5 Punkte
  Beweise, daß $0$ ein anziehender Fixpunkt der
  Funktion $f\coloneqq x \cdot (1 - x)|[0, 1]$ ist; genauer, daß für
  jedes $t \in [0, 1]$ die $f$-Banachfolge mit Startpunkt $t$ gegen
  $0$ konvergiert.

  (Tip: Zeige, daß jede $f$-Banachfolge mit einem Startwert $t_0 > 0$
  monoton fallend ist; sie besitzt daher einen Grenzwert, welcher nach
  Aufgabe (a) aus 4.15 dann der Fixpunkt $0$ ist.)

\item \oralex
  Seien $n \in \set Z$ und $z$, $w \in \set C^*$. Zeige
  \begin{enumerate}
  \item
    $n \cdot \arg (z) \in \Arg(z^n)$ und
  \item
    $\arg(z) - \arg(w) \in \Arg(z/w)$.  Gilt $\arg(z) - \arg(w)
    = \arg(z/w)$ uneingeschränkt?
  \item
    Bestimme die Argumente von $(1 - i \sqrt{3})^{27}$, $(7 + i)/(4 - 3 i)$, $1/(3 - i)^2$.
  \end{enumerate}

\item \oralex
  \textbf{Über die Exponentialfunktion.}
  \begin{enumerate}
  \item
    Sei $s \in \set R$.  Skizziere $s + i \set R$ und $\set R + i s$ in einem Bild, sowie
    $\exp(s + i \set R)$ und $\exp(\set R + i s)$ in einem zweiten.
  \item
    Zeige, daß die Funktion $f\colon \set C^* \to \set C, z \mapsto \exp(1/z)$ für jedes $\epsilon
    \in \set R_+$ auf $\dot U_\epsilon(0)$ alle Werte auf $\set C^*$ annimmt.
  \end{enumerate}

\item \oralex
  \textbf{Komplexe Wurzeln.}
  Für jedes $n \in \set N_1$ und jedes $z \in \set C$ bezeichne $W^n(z)$ die Menge aller komplexen
  $n$-ten Wurzeln von $z$, d.\,h.:
  \[
  W^n(z) \coloneqq \{w \in \set C \mid w^n = z\}.
  \]
  Zeige:
  \begin{enumerate}
  \item
    $W^n(0) = \{0\}$ und $W^n(z) = \{\sqrt[n]{\abs z} \cdot \exp(i \frac{\arg(z) + 2 \pi k} n)
    \mid k = 0, \ldots, n - 1\}$ für alle $z \in \set C^*$.
  \item
    Auf der geschlitzten Ebene $E_- \cup \{0\}$ (vgl.~Theorem aus Abschnitt~7.19) existiert genau eine stetige
    Funktion $f_n\colon E_- \cup \{0\} \to \set C$, so daß gilt:
    \[
    f_n(1) = 1
    \quad\text{und}\quad
    \forall z \in E_- \cup \{0\}\colon f_n(z) \in W^n(z).
    \]
    Diese Funktion heißt der \emph{Hauptzweig} der komplexen $n$-ten
    Wurzelfunktion.
  \item
    $\forall z \in \set C\colon (w \in W^2(z) \implies W^2(z) = \{w, -w\})$.
  \end{enumerate}

\item
  \begin{enumerate}
  \item \writtenex % 5 Punkte
    Sei $P \coloneqq \sum\limits_{k = 0}^n a_k z^k$ eine
    komplexe Polynomfunktion mit reellen Koeffizienten $a_0$, \ldots,
    $a_n \in \set R$.  Zeige: Ist $z_0$ eine Nullstelle von $P$, so
    ist auch $\overline{z_0}$ eine Nullstelle von $P$.
  \item \oralex Bestimme alle Lösungen der Gleichungen $z^5 - 1 = 0$,
    $z^7 - i = 0$, $(1 + i) \cdot z^2 + 1 - i = 0$.  Skizziere die Lösungsmengen.
  \end{enumerate}

\item \writtenex
  Beweise:
  \begin{enumerate}
  \item % 3 Punkte
    Für den Hauptzweig $f_n$ der $n$-ten Wurzel gilt
    \[
    \forall z \in E_- \colon f_n(z) = z^{1/n}.
    \]
  \item % 2 + 1 Punkte
    Sei $\nu \in \set C$. Dann existiert eine Konstante $C \in \set C$, so daß für jedes
    $z_0 \in \set R_-$ gilt
    \[
    \lim_{\substack{z \to z_0 \\ \Im(z) \ge 0}} z^\nu =
    C \cdot \lim_{\substack{z \to z_0 \\ \Im(z) < 0}} z^\nu.
    \]
    In Worten ausgedrückt: Die Funktion $z^\nu$ macht einen
    (multiplikativen) Sprung um $C$ an der negativen reellen Achse.
    Für welche $\nu$ ist $C = 1$?
  \item % 1 Punkt
    An welchen Stellen ist $z^\nu$ stetig?
  \item % 1 + 2 Punkte
    Sind $\nu$ und $\mu \in \set C$, so gilt generell
    $z^{\nu + \mu} = z^\nu \cdot z^\mu$, im allgemeinen aber nicht $(z^\nu)^\mu = z^{\nu \mu}$.
  \end{enumerate}

\item \writtenex
  Beweise:
  \begin{enumerate}
  \item % 2 Punkte
    Mit den beiden kanonischen Koordinatenfunktionen $x$,
    $y\colon \set C \to \set R$ (vgl.~Abschnitt 5.1) gilt:
    \begin{align*}
      \cos & = \cos(x) \cdot \cosh(y) - i \sin(x) \cdot \sinh(y), \\
      \sin & = \sin(x) \cdot \cosh(y) + i \cos(x) \cdot \sinh(y).
    \end{align*}
    Daher (?) sind die Verschwindungsmengen der komplexen Kosinus- und Sinusfunktion durch
    \begin{align*}
      V(\cos) & \coloneqq \{z \in \set C \mid \cos(z) = 0\}
                = \{\pi/2 + k \cdot \pi \mid k \in \set Z\} \\
      \intertext{und}
      V(\sin) & \coloneqq \{z \in \set C \mid \sin(z) = 0\}
                = \{k \cdot \pi \mid k \in \set Z\}
    \end{align*}
    gegeben.

    Die analogen Verschwindungsmengen des komplexen Kosinus hyperbolicus
    und Sinus hyperbolicus sind damit durch
    \begin{align*}
      V(\cosh) & \coloneqq \{z \in \set C \mid \cosh(z) = 0\}
                 = i V(\cos)
                 \intertext{und}
                 V(\sinh) & \coloneqq \{z \in \set C \mid \sinh(z) = 0\}
                            = i V(\sin)
    \end{align*}
    verbunden.
  \item % 2 Punkte
    Die komplexe \emph{hyperbolische Tangensfunktion}
    \[
    \tanh \coloneqq \frac{\sinh}{\cosh}
    \]
    ist auf $\set C \setminus i V(\cos)$ definiert und holomorph; die komplexe
    \emph{hyperbolische Kotangensfunktion}
    \[
    \coth \coloneqq \frac{\cosh}{\sinh}
    \]
    ist auf $\set C \setminus i V(\sin)$ definiert und holomorph.  Die Ableitungen dieser
    Funktionen sind
    \begin{align*}
      \tanh' & = \frac 1 {\cosh^2} = 1 - \tanh^2, \\
      \coth' & = - \frac 1 {\sinh^2} = 1 - \coth^2.
    \end{align*}
    Die Funktionen $\tanh$ und $\coth$ sind ungerade, d.\,h.~$\tanh(-z) = - \tanh(z)$
    und $\coth(-z) = - \coth(z)$.

  \item % 2 Punkte
    Nun zu den Einschränkungen $\cosh(x)$,
    $\sinh(x)$, $\tanh(x)$ und $\coth(x)$ der komplexen
    Hyperbelfunktionen auf die reelle Achse $\set R$:
    $\sinh(x)\colon \set R \to \set R$ ist streng monoton wachsend mit
    $\sinh(\set R) = \set R$;
    $\cosh|\left[0, \infty\right[\colon \left[0, \infty\right[ \to \set
    R$
    ist streng monoton wachsend mit
    $\cosh(\left[0, \infty\right[) = \left[1, \infty\right[$; $\tanh(x)$
    ist streng monoton wachsend mit $\tanh(\set R) = \left]-1, 1\right[$; und
    $\coth|\set R_+\colon \set R_+ \to \set R$ ist streng monoton fallend mit $\coth(\set R_+) =
    \left]1, \infty\right[$.
    Daher (?) besitzen diese Funktionen Umkehrfunktionen, die sogenannten Areafunktionen:
    \begin{align*}
      \tag{Areasinus}
      \arsinh & \colon \set R \to \set R, \\
      \tag{Areakosinus}
      \arcosh & \colon \left[1, \infty\right[ \to \set R, \\
      \tag{Areatangens}
      \artanh & \colon \left]-1, 1\right[ \to \set R \\
      \intertext{und}
      \tag{Areakotangens}
      \arcoth & \colon \left]1, \infty\right[ \to \set R.
    \end{align*}
  \item % 2 Punkte
    Die Funktionen $\arsinh$, $\artanh$ und $\arcoth$ sind
    differenzierbar; $\arcosh$ ist stetig und auf
    $\left]1, \infty\right[$ differenzierbar.  Die Ableitungen sind
    \begin{align*}
      \arsinh' & = \frac 1 {\sqrt{x^2 + 1}}, \\
      \arcosh' & = \frac 1 {\sqrt{x^2 - 1}} \Big|\left]1, \infty\right[, \\
      \artanh' & = \frac 1 {1 - x^2} \Big|\left]-1, 1\right[ \\
      \intertext{und}
      \arcoth' & = \frac 1 {1 - x^2} \Big|\left]1, \infty\right[.
    \end{align*}
  \item % 2 Punkte
    Die Areafunktionen hängen folgendermaßen mit der Logarithmusfunktion zusammen:
    \begin{align*}
      \arsinh & = \ln\bigl(x + \sqrt{x^2 + 1}\bigr), \\
      \arcosh & = \ln\bigl(x + \sqrt{x^2 - 1}\bigr), \\
      \artanh & = \frac 1 2 \cdot \ln\Bigl(\frac{1 + x}{1 - x}\Bigr) \\
      \intertext{und}
      \arcoth & = \frac 1 2 \cdot \ln\Bigl(\frac{x + 1}{x - 1}\Bigr) \Big|\left]1, \infty\right[.
    \end{align*}
  \end{enumerate}
\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
