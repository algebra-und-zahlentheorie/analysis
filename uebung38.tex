\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2022/23}

\title{\bfseries 8.~Übung zur Analysis III}
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Lukas\ Stoll, M.\ Sc.}
\date{08.~Dezember -- 15.~Dezember 2022
  \thanks{Es sind maximal 20 Punkte zu erreichen.
  }}

\begin{document}

\maketitle

\begin{enumerate}[start=268]

  \item \writtenex \textbf{(10 Punkte)} \textbf{Differenzierbare Bilder Lebesguescher Nullmengen.}
  Ziel der Aufgabe ist der Beweis folgender Aussage:
  \begin{verse}
    Ist $G$ eine offene Teilmenge des $\set R^n$, $f\colon G \to \set
    R^n$ eine $\Diff 1$-Abbildung und $N \subseteq G$ eine
    $\lambda^n$-Nullmenge, so ist auch das Bild $f(N)$ eine
    $\lambda^n$-Nullmenge.
  \end{verse}
  Da geschickterweise bei der Bearbeitung der Aufgabe die
  Charakterisierung Lebesguescher Nullmengen aus
  Theorem~2 aus Abschnitt~15.3 verwendet wird, ist es sinnvoll, im
  folgenden den $\set R^n$ mit der Supremumsnorm zu versehen.

  Der Beweis erfolgt unter Benutzung der in Abschnitt~15.3 eingeführten
  Bezeichnung $K_m(G)$ in folgenden Schritten:
  \begin{enumerate}
  \item
    \label{it:diffnull1}
    Es sei zunächst zusätzlich vorausgesetzt, daß es eine Konstante $c
    \in \set R_+$ gibt, so daß $\forall p \in G\colon \anorm{D_p f}
    \leq c$ ist.  Zeige:
    \begin{enumerate}
    \item
      Zu jedem abgeschlossenen Würfel $Q_0 \subseteq G$ mit
      Mittelpunkt $a \in \set R^n$ existiert ein Würfel $Q_0'$ mit
      Mittelpunkt $f(a)$, so daß
      \[
        f(Q_0) \subseteq Q_0'\quad\text{und}\quad
        \lambda^n(Q_0') \leq c^n \cdot \lambda^n(Q_0).
      \]
    \item
      Ist $Q \subseteq G$ irgendein abgeschlossener Quader, so ist
      $f(N \cap Q)$ eine $\lambda^n$"=Nullmenge.
    \item
      Für jedes $m \in \set N_0$ ist $f(N \cap K_m(G))$ eine
      $\lambda^n$-Nullmenge, also (?) ist auch $f(N) = f(N \cap G)$
      eine $\lambda^n$-Nullmenge.
    \end{enumerate}
  \item
    Nun beweise vermittels~\ref{it:diffnull1} in der allgemeinen
    Situation, daß für jedes $m \in \set N_0$ das Bild $f(N \cap
    \interior{K_m(G)})$ eine $\lambda^n$-Nullmenge ist, und folgere
    daraus, daß auch $f(N)$ eine $\lambda^n$-Nullmenge ist.
  \end{enumerate}
  Gilt die Aussage auch noch, wenn $f$ nur als stetig vorausgesetzt
  wird?

\item \oralex \textbf{Bilder niederdimensionaler Teilmengen sind dünn.}
  \leavevmode
  \begin{enumerate}
  \item
    Seien $G$ eine offene Teilmenge des $\set R^k$ und $f\colon G \to
    \set R^n$ eine $\Diff 1$-Abbildung.  Ist dann $k < n$, so ist das
    Bild $f(G)$ eine Lebesguesche Nullmenge.

    Insbesondere (?) sind daher alle echten Untervektorräume des $\set
    R^n$ Lebesguesche Nullmengen.

    (Tip: $g\colon \set R^{n - k} \times G \to \set R^n, (p, q)
    \mapsto f(q)$.)
  \item
    Jede $k$-dimensionale $\Diff 1$-Untermannigfaltigkeit $M$ von
    $\set R^n$ ist eine Lebesguesche Nullmenge, wenn $k < n$.
  \end{enumerate}

\item \writtenex \textbf{(5 Punkte)}
  Ist $g \in \hMeasp(X, \mathfrak A)$, so wird durch
  \[
    \mu_g\colon \mathfrak A \to \widehat{\set R}, A \mapsto \int_A g
    \, d\mu
  \]
  ein Maß auf $\mathfrak A$ definiert; wir sagen, daß $\mu_g$ das Maß der
  \emph{Dichte} $g$ bezüglich $\mu$ ist.  Dieses Maß ist
  \emph{absolut stetig} bezüglich $\mu$, d.\,h.~$\Null(X, \mathfrak
  A, \mu) \subseteq \Null(X, \mathfrak A, \mu_g)$; weiterhin gilt:
  \[
    \forall f \in \hMeasp(X, \mathfrak A)\colon \int f \, d\mu_g =
    \int f \cdot g \, d\mu.
  \]

  (Tip: Zum Beweis der $\sigma$-Additivität von $\mu_g$ beachte das
  Korollar~2 in Abschnitt~15.4.  Zum Beweis der
  Integralgleichheit: 1.~Schritt: $f = 1_A$ mit $A \in \mathfrak A$;
  2.~Schritt: $f \in \Simplep(X, \mathfrak A)$; 3.~Schritt: $f \in
  \hMeasp(X, \mathfrak A)$.)

\item \oralex
  Sind $\phi$, $\psi \in \Simplep(X, \mathfrak A)$, so sind auch $\phi
  \cdot \psi$, $\sup(\phi, \psi)$, $\inf(\phi, \psi) \in \Simplep(X,
  \mathfrak A)$.

\item \oralex
  Ist $\nu$ das Zählmaß auf $\set N_0$, so ist jede nirgends negative
  Funktion $f\colon \set N_0 \to \set R$ ein Element von $\hMeasp(\set
  N_0, \powerset{\set N_0})$ und
  \[
    \int f \, d\nu = \sum_{n = 0}^\infty f(n).
  \]

\item \oralex
  Sei $f \in \hMeasp(X, \mathfrak A)$ eine Funktion, für welche $\int
  f\,d\mu < \infty$ ist.  Dann gilt:
  \begin{enumerate}
  \item
    Für jedes $a \in \set R_+$ hat die Menge $M_a \coloneqq \{p \in X
    \mid f(p) \ge a\}$ ein endliches $\mu$-Maß.
  \item
    Die Menge $M_0 \coloneqq \{p \in X \mid f(p) > 0\}$ ist
    $\sigma$-endlich, d.\,h.~es existiert eine Folge $(A_n)$ von
    Mengen $A_n \in \mathfrak A$ mit $\mu(A_n) < \infty$ und $M_0
    \subseteq \bigcup A_n$.
  \item
    Zu jedem $\epsilon \in \set R_+$ existiert eine Menge $A \in
    \mathfrak A$ derart, daß $\mu(A) < \infty$ und
    \[
      \int f \,d\mu \leq \int_A f \, d\mu + \epsilon.
    \]
    (Tip: Arbeite mit einer Abbildung $\phi \in \Simplep(X, \mathfrak A)$,
    für welche $\phi \leq f$ und $\int f \, d\mu \leq \int \phi \,
    d\mu + \epsilon$ gilt.)
  \end{enumerate}

\item \oralex
  \label{ex:lebesgue_descreasing}
  Es sei $\lambda$ das Lebesguesche Maß auf $\set R$.
  \begin{enumerate}
  \item
    \label{it:lebesgue_decreasing1}
    Ist $f_n \coloneqq \frac 1 n \cdot 1_{\interval[open right] n \infty}$,
    so ist $(f_n)_{n \ge 1}$ eine monoton fallende Folge, die
    gleichmäßig gegen $f \equiv 0$ konvergiert.  Gilt
    \[
      \lim_{n \to \infty} \int f_n \, d\lambda = 0?
    \]
    Welche Schlüsse können wir aus diesem Beispiel für monoton
    fallende Folgen auf $\hMeasp(X, \mathfrak A)$ ziehen?
  \item
    Modifiziere~\ref{it:lebesgue_decreasing1}, indem Du die Folge
    $f_n \coloneqq \frac 1 n \cdot 1_{\interval 0 n}$ betrachtest.  Das
    Lemma von \name{Fatou} muß ja anwendbar sein; wie sehen da die
    Verhältnisse aus?
  \item
    Modifiziere~\ref{it:lebesgue_decreasing1}, indem Du die
    Folge der Funktionen $f_n \coloneqq n \cdot
    1_{\interval{\frac 1 n}{\frac 2 n}}$ betrachtest.  Ist die Konvergenz von
    $(f_n)$ gegen $f \equiv 0$ gleichmäßig? Wie sehen die Verhältnisse
    beim Lemma von \name{Fatou} aus?
  \end{enumerate}

\item \writtenex \textbf{(5 Punkte)}
  Es seien $\mathfrak A$, $\mathfrak B \subseteq \powerset X$ zwei
  $\sigma$-Algebren über einer Menge $X$ und $\mu\colon \mathfrak A
  \to \widehat{\set R}$ und $\nu\colon \mathfrak B \to \widehat{\set
    R}$ zwei Maße, und zwar sei $\nu$ eine Fortsetzung von $\mu$,
  d.\,h.
  \[
    \mathfrak A \subseteq \mathfrak B\quad\text{und}\quad
    \mu = \nu|\mathfrak A.
  \]
  Dann gilt:
  \begin{enumerate}
  \item
    $\Meas(X, \mathfrak A) \subseteq \Meas(X, \mathfrak B)$,
    $\Simplep(X, \mathfrak A) \subseteq \Simplep(X, \mathfrak B)$,
    $\MeasC(X, \mathfrak A) \subseteq \MeasC(X, \mathfrak B)$,
    $\hMeas(X, \mathfrak A) \subseteq \hMeas(X, \mathfrak B)$ und
    $\hMeasp(X, \mathfrak A) \subseteq \hMeasp(X, \mathfrak B)$.
  \item
    $\Null(X, \mathfrak A, \mu) \subseteq \Null(X, \mathfrak B, \nu)$.
  \item
    Für alle $\phi \in \Simplep(X, \mathfrak A)$ gilt $\int \phi \,
    d\mu = \int \phi \, d\nu$.
  \item
    Für alle $f \in \hMeasp(X, \mathfrak A)$ gilt $\int f \, d\mu =
    \int f \, d\nu$.
    (Tip: Beachte Lemma~1 aus Abschnitt~15.4.)
  \end{enumerate}

\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
