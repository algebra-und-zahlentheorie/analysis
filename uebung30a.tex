\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2022/23}


\title{\bfseries Vorbereitung auf die Analysis III}
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Lukas Stoll, M.\ Sc.}

\begin{document}

\maketitle

In den folgenden Aufgaben sind in \textsf{serifenloser Schrift} Verweise auf Abschnitte des Skriptes zur Analysis angegeben, wo die jeweiligen benötigten Begriffe eingeführt werden.
Dies dient zum Einen der Wiederholung für diejenigen, welche die Analysis II bei Prof.~Dr.~Nieper-Wißkirchen gehört haben, zum Anderen dem leichteren Einstieg in die Analysis III für Neueinsteiger.
Das Skript sowie weitere Informationen zur Vorlesung finden sich unter \url{https://ana.mathe.sexy/}.

\begin{enumerate}

\item Sei $M$ eine Teilmenge eines metrischen Raumes $E$ (\textsf{3.1}), $\overline M$ ihre abgeschlossene Hülle (\textsf{4.8, 12.7}) in $E$, $E'$ ein vollständiger (\textsf{4.14}) metrischer Raum und $f:M \to E'$ eine gleichmäßig stetige (\textsf{4.12}) Abbildung.
Zeige:
\begin{enumerate}
  \item \label{item:ucont} Es existiert genau eine gleichmäßig stetige Abbildung $F:\overline{M}\to E'$, welche $f$ fortsetzt, also $F|M=f$ erfüllt.\\
  (Tipp: Zeige zunächst Eindeutigkeit um dich für den Existenzbeweis inspirieren zu lassen.
  Für die gleichmäßige Stetigkeit, nutze die Stetigkeit der Distanzfunktion aus.)
  \item Die Aussage aus \ref{item:ucont} ist im Allgemeinen falsch, wenn \dots
  \begin{enumerate}
    \item \dots $E'$ nicht vollständig ist.
    \item \dots $f$ nicht \emph{gleichmäßig} stetig ist.
  \end{enumerate}
\end{enumerate}

\item
Für $v=(v_i)\in\set R^n$ sei
  $\norm \infty v \coloneqq \max\{\abs{v_1},\dots,\abs{v_n}\}$.
Zeige:
\begin{enumerate}
  \item Die so definierte Abbildung $\norm \infty \cdot : \set R^n \to \set R$ ist eine Norm (\textsf{5.2}), welche den $\set R^n$ zu einem Banachraum (\textsf{5.3}) macht.
  \item Für jedes $k\in\{1,\dots,n\}$ ist die $k$-te Koordinatenfunktion $x_k : \set R^n \to \set R, (v_i)\mapsto v_k$ stetig (\textsf{3.2, 12.4}).
  \item Sei $E$ ein metrischer Raum.
  Eine Abbildung $f:E \to \set R^n$ ist genau dann stetig bezüglich $\norm \infty \cdot$, wenn für jedes $k\in\{1,\dots,n\}$ die Abbildung $f_k\coloneqq x_k\circ f:\set E\to\set R$ stetig ist.
  \item Jede reellwertige Matrix $(a_{ij})\in\set R^{m\times n}$ mit $m$ Zeilen und $n$ Spalten definiert eine stetige lineare Abbildung $A:\set R^n \to \set R^m$ durch die durch Matrixmultiplikation gegebene Zuordnung
  $$
  (v_j) \longmapsto (a_{ij})\cdot (v_j) = \big(\sum_j a_{ij}v_j\big)_i.
  $$
  (Tipp: Verwende Aufgabe \ref{ex:linearMaps})
  \item Sei $E$ ein endlich-dimensionaler normierter Vektorraum der Dimension $n$.
  Jede Basis $(b_1,\dots,b_n)$ von $E$ definiert einen Homöomorphismus (\textsf{12.4}) $\set R^n \to E$ durch die Zuordnung
  $$
  (v_i) \longmapsto \sum_{i} v_i \cdot b_i
  $$
  (Tipp: Verwende Aufgabe \ref{ex:linearMaps} und \textsf{Proposition 12.9.3}.)
  \item Jede lineare Abbildung zwischen zwei endlich-dimensionalen Vektorräumen ist stetig.
\end{enumerate}

\item \label{ex:linearMaps}
Sei $A: E \to F$ eine lineare Abbildung zwischen normierten Vektorräumen.
\begin{enumerate}
  \item Es ist $A$ genau dann stetig, wenn eine Zahl $M\in [0,\infty[$ existiert, sodass
  \begin{equation}
    \forall v \in E : \anorm{Av} \leq M \cdot \anorm v.
  \end{equation}
  In diesem Fall ist $\anorm A\coloneqq\sup \{\anorm {Av} \mid \anorm v \leq 1\}$ die kleinste Zahl $M$, welche dies erfüllt.
  \item Die Abbildung $A \mapsto \anorm A$ definiert eine Norm auf dem Vektorraum $L(E,F)$ (\textsf{11.7}) der stetigen linearen Abbildungen $E \to F$, die sogenannte Operatornorm.
  \item Es ist $L(E,F)$ ein Banachraum, falls $F$ ein Banachraum ist.\\
  (Tipp: Es ist $\anorm\cdot$ bereits eine Norm auf $L(E,F)$, es genügt daher Vollständigkeit zu zeigen, dass also jede Cauchyfolge in $L(E,F)$ konvergiert.)
  \item Sei $E=\set R^n$, $F=\set R^m$, sodass $A$ durch eine Matrix $(a_{ij}) \in \set R^{m\times n}$ dargestellt wird.
  Sind $E$ und $F$ mit der Supremumsnorm $\norm \infty \cdot$ versehen, so ist die Operatornorm von $A$ durch die Zeilensummennorm gegeben:
  $$
  \anorm A = \max \left\{ \sum_{j=1}^{n}\abs{a_{ij}} \biggm| i=1,\dots,m \right\}
  $$
\end{enumerate}

\item Sei $G$ eine offene Teilmenge (\textsf{6.1}, \textsf{12.2}, \textsf{12.3}) des $\set R^n$, $a\in G$ und $f:G \to \set R^m$ eine Funktion. Zeige:
\begin{enumerate}
  \item Ist $n=1$, so ist $f$ genau dann in $a$ differenzierbar (\textsf{6.4}), wenn ein $m \in  \set R^m$ existiert, sodass
  $$
  \lim_{p \to a} \frac{f(p)-f(a)-m\cdot(p-a)}{p-a} = 0.
  $$
  \item Sei $n\in\set N$ beliebig und $f$ auf ganz $G$ für jedes $k\in\{1,\dots,n\}$ partiell nach $x_k$ differenzierbar (\textsf{13.1}), sodass die partiellen Ableitungen $\tfrac{\partial f}{\partial x_k}$ in $a$ stetig sind.
  Dann gilt für die Jacobische Matrix $J_a(f)$ (\textsf{13.1}) von $f$ im Punkt $a$:
  $$
  \lim_{p \to a} \frac{f(p)-f(a)-J_a(f)\cdot (p-a)}{\anorm{p-a}} = 0
  $$
  (Tipp: Ohne Einschränkung kann man $m=1$ annehmen.
  Wende dann den Mittelwertsatz (\textsf{6.9}) auf Hilfsfunktionen der Form $f(a+\sum_{i=1}^{j-1}v_ie_i+x\cdot v_je_j):[0,1]\to\set R$ an, wobei $v\coloneqq p-a$.)
\end{enumerate}
  
\item Sei $G$ eine offene Teilmenge des $\set R^n$, $a \in G$ und $f:G \to \set R^m$ eine Abbildung für die in $a$ alle partiellen Ableitungen nach den Koordinaten $x_1,\dots,x_n$ von $\set R^n$ existieren. 
Sei weiter $G'$ eine offene Teilmenge des $\set R^m$, sodass $f(G)\subseteq G'$, und $g:G' \to \set R^l$ eine Abbildung für die in $f(a)$ alle partiellen Ableitungen nach den Koordinaten $x_1,\dots,x_m$ von $\set R^m$ existieren.
$$
G \overset{f}{\longrightarrow} G' \overset{g}{\longrightarrow} \set R^l
$$
\begin{enumerate}
  \item Zeige die Kettenregel für die Jacobische Matrix der Komposition von $g$ und $f$:
  $$
  J_a(g\circ f) = J_{f(a)}(g) \cdot J_a(f)
  $$
  (Tipp: Verwende \textsf{Beispiel 13.2.(d)}.)
  \item Berechne die Jacobi-Matrix der Polarkoordinatentransformation (\textsf{7.19})
  $$
  \Phi:\set R_+ \times ]0,2\pi[ \to \set R^2, (r,\theta) \mapsto (r\cos(\theta),r\sin(\theta)).
  $$
  und ihre Inverse.
  \item Berechne die Jacobi-Matrix der Abbildung
  $$
  F:\{p\in\set R^2 \mid x(p)\neq 0\} \to \set R^2, p \mapsto \left(\ln\left(\sqrt{x(p)^2+y(p)^2}\right), \arctan\left(\frac{y(p)}{x(p)}\right)\right),
  $$
  wobei $x,y:\set R^2 \to \set R$ die kanonischen Koordinatenfunktionen des $\set R^2$ bezeichnen.
  (Tipp: \textsf{7.16}.)
\end{enumerate}




%\item \textbf{Normen auf dem $\set R^n$.}
%  Für $v=(v_i)\in\set R^n$ definiere
%  $$
%  \qquad \norm 2 v \coloneqq \sqrt{\sum_{i = 1}^n v_i^2},
%  \qquad \norm \infty v \coloneqq \max\{\abs{v_i}\mid i = 1,\dots n\}.
%  $$
%  Zeige:
%  \begin{enumerate}
%    \item Die so definierten Abbildungen $\set R^n \to \set R$ sind Normen auf $\set R^n$.\\
%    (Tip: $\anorm{v}_2^2 = \scp v v$.)
%    \item Es ist $\norm \infty v \leq \norm 2 v \leq \sqrt{n} \norm \infty v$ für alle $v\in\set R^n$, das heißt die Normen $\norm 2 \cdot$ und $\norm \infty \cdot$ sind äquivalent.
%    \item Wir erinnern an den Begriff der $\epsilon$-Umgebung eines Punktes $p\in\set R^n$ bezüglich einer Norm $\norm a \cdot$ auf $\set R^n$:
%    $$
%    U_\epsilon^{a}(p) = \{q \in \set R^n \mid \norm a {p-q} < \epsilon\}
%    $$
%    Es gilt:
%    \begin{enumerate}
%      \item Eine Teilmenge $G$ von $\set R^n$ ist genau dann offen bezüglich $\norm \infty \cdot$, wenn sie bezüglich $\norm 2 \cdot$ offen ist.
%      \item Eine Funktion $f:\set R^n \to \set R^m$ ist genau dann stetig bezüglich $\norm \infty \cdot$, wenn sie bezüglich $\norm 2 \cdot$ stetig ist.
%      \item Eine Folge in $\set R^n$ konvergiert genau dann bezüglich $\norm \infty \cdot$, wenn sie bezüglich $\norm 2 \cdot$ konvergiert.
%      \item Eine Folge in $\set R^n$ ist genau dann eine Cauchyfolge bezüglich $\norm \infty \cdot$, wenn sie eine Cauchyfolge bezüglich $\norm 2 \cdot$ ist.
%    \end{enumerate}
%    \item 
%  \end{enumerate}

  
\end{enumerate}

\end{document}
