\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{uebung}

\LoadClass[a4paper,12pt]{article}

\RequirePackage{analysis}
\RequirePackage{fancyhdr}

\setlength{\headheight}{14.5pt}
\fancypagestyle{plain}{%
  \lhead{Universität Augsburg}
  \rhead{\semester}
  \renewcommand{\headrulewidth}{0pt}
  \renewcommand{\footrulewidth}{0pt}
}

\setlist[enumerate,1]{label={\bfseries \arabic*.}, ref={\arabic*.}, 
  align=right, leftmargin=*}
\setlist[enumerate,2]{label={\bfseries (\alph*)}, ref={(\alph*)}, 
  align=right, leftmargin=*, beginpenalty=100, midpenalty=100}
\setlist[enumerate,3]{label={\bfseries (\roman*)}, ref={\theenumii(\roman*)}, 
  align=right, leftmargin=*, beginpenalty=100, midpenalty=100}
\setlist[enumerate,4]{label={\bfseries \Alph*.}, ref={\Alph*.}, 
  align=right, leftmargin=*, beginpenalty=100, midpenalty=100}
\newlist{axiomlist}{enumerate}{1}
\setlist[axiomlist,1]{font=\bfseries,
  align=right, leftmargin=*}

\newcommand{\writtenex}{\textbf{s.}\ }
\newcommand{\oralex}{\textbf{m.}\ }
