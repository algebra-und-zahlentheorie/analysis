\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2022/23}

\title{\bfseries 7.~Übung zur Analysis III}
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Lukas\ Stoll, M.\ Sc.}
\date{01.~Dezember -- 08.~Dezember 2022
  \thanks{Von den 30 zu erreichenden Punkten werden maximal 25 Punkte angerechnet.
  }}

\begin{document}

\maketitle

% Vormals Start bei 260
\begin{enumerate}[start=259]

\item \oralex
  Beweise, daß für alle $a$, $b \in \set R$ mit $a < b$ gilt:
  \[
    \interval a b = \bigcap_{n = 1}^\infty \interval[open]{a - \frac 1
      n}{b + \frac 1 n}\quad\text{und}\quad
    \interval[open] a b = \bigcup_{n = 1}^\infty \interval{a + \frac 1
      n}{b - \frac 1 n}.
  \]
  Folglich enthält jede $\sigma$-Algebra $\mathfrak A \subseteq
  \powerset{\set R}$, welche alle offenen (bzw.~alle abgeschlossenen)
  Intervalle enthält, auch alle abgeschlossenen (bzw.~alle offenen) Intervalle.

\item \oralex
  Gib ein Beispiel einer nicht meßbaren Funktion
  $f\colon \set R \to \set R$ an, für die $\abs f$ meßbar ist; es darf
  benutzt werden, daß $\borel {\set R} \subsetneq \powerset{\set R}$;
  vgl.\ Aufgabe~2 aus Abschnitt~15.7.
  (Vergleiche dies mit Proposition~3 aus Abschnitt~15.2.)

\item \oralex
  Zeige: Für jedes $R \in \set R_+$ ist die Abschneide-Funktion
  \[
    \chi_R\colon \widehat{\set R} \to \set R, t \mapsto
    \begin{cases}
      R & \text{für $t \in \interval[open left] R \infty,$} \\
      t & \text{für $t \in \interval {-R} R,$} \\
      -R & \text{für $t \in \interval[open right] {-\infty}{-R}$}
    \end{cases}
  \]
  Borel-meßbar.  Daher (?) ist auch für jede Funktion $f \in \hMeas(X,
  \mathfrak A)$ die "`abgeschnittene"' Funktion
  \[
    f_R\colon X \to \set R, p \mapsto \begin{cases}
      R & \text{für $f(p) \in \interval[open left] R \infty,$} \\
      f(p) & \text{für $f(p) \in \interval {-R} R,$} \\
      -R & \text{für $f(p) \in \interval[open right] {-\infty}{-R}$}
    \end{cases}
  \]
  eine $\mathfrak A$-meßbare Funktion.

\item \writtenex \textbf{(5 Punkte)}
  Es seien $a$, $b \in \set R$ reelle Zahlen mit $a < b$.  Zeige, dass jede
  Regelfunktion $f \in \Ruled(\interval a b, \set R)$
  Borel-meßbar ist, also
  \[
    \Ruled(\interval a b, \set R) \subseteq \Meas(\interval a b,
    \borel{\interval a b}).
  \]
  Zeige auch, daß $\Ruled(\set R, \set R) \subseteq \Meas(\set R,
  \borel{\set R})$.

\item \oralex \textbf{Elementare Konstruktion von Maßen.}  Es sei
  Sei $(X, \mathfrak A)$ ein meßbarer Raum.  Zeige:
  \begin{enumerate}
  \item Seien $\mu$ ein Maß auf $(X, \mathfrak A)$ und
    $A_0 \in \mathfrak A$.  Dann ist
    \[
      \mathfrak A \to \widehat{\set R}, A \mapsto \mu(A \cap A_0)
    \]
    ebenfalls ein Maß auf $(X, \mathfrak A)$.
  \item
    Sind $\mu_1$, \dots, $\mu_n$ Maße auf $(X, \mathfrak A)$ und
    sind $a_1$, \dots, $a_n$ nicht negative reelle Zahlen, so ist
    \[
      \sum_{k = 1}^n a_k \cdot \mu_k
    \]
    ebenfalls ein Maß auf $(X, \mathfrak A)$.
  \item
    Ist $(\mu_n)_{n \ge 1}$ eine Folge von
    Wahrscheinlichkeitsmaßen auf $(X, \mathfrak A)$, so ist
    \[
      \sum_{n = 1}^\infty 2^{-n} \cdot \mu_n
    \]
    ebenfalls ein Wahrscheinlichkeitsmaß auf $(X, \mathfrak A)$.
  \end{enumerate}

\item \writtenex \textbf{(10 Punkte)}
  Seien $n \in \set N_2$ und $\alpha\colon
  \interval a b \to \set R^n$ ein
  rektifizierbarer Weg (vgl.\ Abschnitt~9.6).  Zeige, daß seine Spur
  $\alpha(\interval a b)$ eine Lebesgue-Nullmenge ist.  Folglich (?)
  sind Peano-Wege niemals rektifizierbar.

  (Tip: Wähle einen $\alpha$ hinreichend gut approximierenden
  Streckenzug $\sigma$ und skizziere die $\epsilon$-Umgebung
  $U_\epsilon(\sigma(\interval a b))$; weiterhin benutze die
  Charakterisierung Lebesguescher Nullmengen aus Abschnitt~15.3.)

\item \oralex
  Es sei $(A_n)_{n \ge 0}$ eine Folge von Teilmengen einer Menge $X$.  Zeige:
  \begin{enumerate}
  \item
    \label{it:monotone_sets}
    Setzen wir $E_n \coloneqq \bigcup\limits_{k = 0}^{n - 1} A_k$
    und $F_n \coloneqq A_n \setminus E_n$ für $n \ge 0$, so ist
    $(E_n)$ eine monoton wachsende Folge von Teilmengen (d.\,h.~$E_n
    \subseteq E_{n + 1}$ für alle $n \ge 0$) und $(F_n)$ eine Folge
    paarweise disjunkter Teilmengen (d.\,h.~$F_n \cap F_m = \emptyset$
    für $n \neq m$) mit
    \[
      \bigcup_{n = 0}^\infty E_n = \bigcup_{n = 0}^\infty F_n =
      \bigcup_{n = 0}^\infty A_n.
    \]
  \item
    Es gelten
    \begin{align*}
      \limsup_{n \to \infty} A_n
      & \coloneqq \bigcap_{m = 0}^\infty
        \bigcup_{n = m}^\infty A_n = \{p \in X \mid \abs{\{n \in \set
        N_0 \mid p \in A_n\}} = \infty\} \\
      \intertext{und}
      \liminf_{n \to \infty} A_n
      & \coloneqq \bigcup_{m = 0}^\infty
        \bigcap_{n = m}^\infty A_n = \{p \in X \mid \abs{\{n \in \set
        N_0 \mid p \notin A_n\}} < \infty\}.
    \end{align*}
    Es gilt weiter
    \[
      \emptyset \subseteq \liminf_{n \to \infty} A_n
      \subseteq \limsup_{n \to \infty} A_n \subseteq X.
    \]
    Zeige außerdem:  Sind die $A_n$ Elemente einer
    $\sigma$-Algebra $\mathfrak A$, so gilt auch
    \[
      \liminf_{n \to \infty} A_n, \limsup_{n \to \infty} A_n \in
      \mathfrak A.
    \]
    Wie die Bezeichnungen schon andeuten, werden diese Mengen der
    \emph{Limes inferior} bzw.~der \emph{Limes superior} der Folge
    $(A_n)$ genannt.
  \item
    Ist $(A_n)$ eine monoton wachsende Folge
    (vgl.~\ref{it:monotone_sets}), so gilt
    \[
      \limsup_{n \to \infty} A_n = \bigcup_{n = 0}^\infty A_n =
      \liminf_{n \to \infty} A_n;
    \]
    ist $(A_n)$ eine monoton fallende Folge (d.\,h.~$A_n \supseteq
    A_{n + 1}$ für alle $n \ge 0$), so gilt
    \[
      \liminf_{n \to \infty} A_n = \bigcap_{n = 0}^\infty A_n
      = \limsup_{n \to \infty} A_n.
    \]
    Im Falle $\limsup\limits_{n \to \infty} A_n = \liminf\limits_{n
      \to \infty} A_n$ nennen wir diese Menge den \emph{Limes}
    der Folge $(A_n)$, $\lim\limits_{n \to \infty} A_n$.

    Zeige durch Angabe eines Beispieles einer Folge $(A_n)$, daß
    $\lim\limits_{n \to \infty} A_n$ existieren kann, obwohl die Folge
    $(A_n)$ weder monoton wächst noch fällt.
  \item
    Sei $(X, \mathfrak A, \mu)$ ein Maßraum und $(A_n)_{n \ge 0}$ eine
    Folge in $\mathfrak A$.  Zeige:
    \[
      \mu\bigl(\liminf_{n \to \infty} A_n\bigr) \leq \liminf_{n \to
        \infty}
      \mu\bigl(A_n\bigr).
    \]
  \end{enumerate}

\item \writtenex \textbf{(5 Punkte)}
  Für jede Folge $(A_n)_{n \ge 0}$ von Teilmengen einer Menge $X$ gilt
  \[
    1_{\limsup\limits_{n \to \infty} A_n} = \limsup_{n \to \infty} 1_{A_n}.
  \]

\item \writtenex \textbf{(10 Punkte)} \textbf{Das Cantorsche Diskontinuum.}
  Es sei
  \[
    K \coloneqq \Bigl\{\sum_{k = 1}^\infty a_k \cdot 10^{-k} \in \set
    R \bigm\vert a_k \in \{0, 9\}\Bigr\}.
  \]
  Ziel der Aufgabe ist es zu zeigen, daß $K$ eine überabzählbare,
  kompakte Lebesgue-Nullmenge ist.  Dazu beweise folgende Schritte:
  \begin{enumerate}
  \item
    Die Funktion
    \[
      \phi\colon K \to \interval 0 1, a = \sum_{k = 1}^\infty a_k
      \cdot 10^{-k} \mapsto \frac 1 9 \sum_{k = 1}^\infty a_k \cdot
      2^{-k}
    \]
    ist wohldefiniert und surjektiv.  Weiterhin gilt $\abs K =
    \abs{\interval 0 1}$.

    (Tip: Abschnitte 5.14 und 7.10.)
  \item
    Für jedes $n \in \set N_0$ definieren wir kompakte Intervalle
    $I_{n, k} \coloneqq \interval {a_{n, k}}{b_{n, k}} \subseteq \set
    R$ für $k = 1$, \dots, $2^n$ rekursiv durch
    \begin{align*}
      I_{0, 1} & \coloneqq \interval 0 1 \\
      \intertext{und}
      I_{n + 1, 2k - 1} & \coloneqq \interval{a_{n, k}}{a_{n, k} + 10^{-(n +
                          1)}} \\
      \intertext{und}
      I_{n + 1, 2k} & \coloneqq \interval{b_{n, k} - 10^{-(n + 1)}}
                      {b_{n, k}}.
    \end{align*}
    Schließlich sei
    \[
      K_n \coloneqq \bigcup_{k = 1}^{2^n} I_{n, k}.
    \]
    \begin{enumerate}
    \item
      Berechne für alle $n \in \set N_0$ das "`Maß"' $L_n \coloneqq
      \sum\limits_{k = 1}^{2^n} \lambda(I_{n, k})$ von $K_n$.
    \item
      Zeige $K_n = \{\sum\limits_{k = 1}^\infty a_k \cdot 10^{-k} \mid
      a_1, \dotsc, a_n \in \{0, 9\} \land a_{n + 1}, \dotsc
      \in \{0, \dotsc, 9\}\}$.
    \item
      Zeige $K = \bigcap\limits_{n = 0}^\infty K_n$.
    \end{enumerate}
    Also (?) ist $K$ eine Lebesguesche Nullmenge (und eine Borel-Menge).
  \end{enumerate}

\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
