\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2022/23}

\title{\bfseries 12.~Übung zur Analysis III}
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Lukas\ Stoll, M.\ Sc.}
\date{19.~Januar -- 26.~Januar 2023
  \thanks{Von den 25 zu erreichenden Punkten werden maximal 20 Punkte angerechnet.}
  }

\begin{document}

\maketitle

% Vormals start=301
\begin{enumerate}[start=299]

\item \oralex
  Berechne mit Hilfe des Cavalierischen Prinzips:
  \begin{enumerate}
  \item
    Die Fläche einer Kreisscheibe um Null mit Radius $r \in \set R_+$.
  \item
    Das Volumen desjenigen Teils des einschaligen Hyperboloids
    \[
      \{p \in \set R^3 \mid x(p)^2 + y(p)^2 - z(p)^2 \leq 1\},
    \]
    der zwischen den Ebenen $z = -1$ und $z = 1$ liegt.
  \end{enumerate}

% Neu
\item \writtenex \textbf{(5 Punkte)} \textbf{Faltung.} 
  Es seien $E_1$, $E_2$, $E$ Banachräume und $B:E_1 \times E_2 \to E$ eine
  stetige bilineare Abbildung. Für Abbildungen $f \in \Leb(\set
  R,\lambda;E_1)$, $g\in\Leb(\set R,\lambda;E_2)$,definieren wir
  \[
    F\colon \set R^2 \to E, (t, s) \mapsto B(f(t - s), g(s)).
  \]
  Zeige:
  \begin{enumerate}
    \item
      Es ist  $F\in\Leb(\set R^2, \lambda^2;E)$.
      Genauer gilt
      $
      \int \anorm F \, d\lambda^2 \leq \anorm B \norm 1 f \norm 1 g.
      $
      
      (Tip: Nimm zunächst an, dass $f=1_A\cdot v$, $g=1_B\cdot w$ für gewisse $A,B\in\borel{\set R}$, $v\in E_1$, $w \in E_2$ und verallgemeinere auf integrierbare einfache Funktionen.
      Verallgemeinere dann auf integrierbare einfache und anschließend auf stark meßbare Abbildungen.
      Für den stark integrierbaren Fall verwende den Satz von \name{Tonelli} und beachte Korollar 2 in Abschnitt 15.9.)
    \item
      Es existiert eine Nullmenge $N$, sodass $F_t\in\Leb(\set R,\lambda;E)$ für alle $t \in \set R\setminus N$.
      Definieren wir die \emph{Faltung} $f\ast g$ von $f$ und $g$ bezüglich $B$ als
      \[
        f \ast g\colon \set R \to E,
        t \mapsto \begin{cases}
          \int_{\set R} F(t, s) \, d\lambda(s) & \text{für $t \in \set R
          \setminus N$} \\
          0 & \text{sonst}
        \end{cases},
      \]
      so ist $f\ast g \in \Leb(\set R,\lambda;E)$ und es gilt
      $
      \norm 1 {f \ast g} \leq \anorm B \norm 1 f \norm 1 g.
      $

      (Tip: Im Theorem von \name{Fubini} läßt sich wählen, bezüglich welcher
      Variablen zuerst integriert wird.)

  \end{enumerate}
  Hinweis: Die Faltung $f\ast g$ ist nur bis auf eine Nullmenge definiert,
  sollte also sinnvollerweise als Element des Raums $\LB^1(\set R,\lambda;E)$
  aufgefasst werden.

\item \oralex
  Es sei $\phi\colon \interval a b \to \set R$ mit
  $-\infty < a < b < \infty$ eine streng monoton wachsende
  $\Diff 1$-Funktion.  Zeige, daß sich die Substitutionsmethode auf
  jede $\lambda$-integrierbare Funktion
  $f\colon \phi(\interval a b) \to \set R$ anwenden läßt.  (Vorsicht!
  $\interval a b$ ist nicht offen.)

\item \writtenex \textbf{(5 Punkte)}
  Zeige ausführlich, daß
  \[
    \int_{-\infty}^\infty \exp(-x^2) \, dx = \sqrt{\pi}.
  \]

\item Berechne mit Hilfe des Transformationssatzes:
  \begin{enumerate}
  \item \oralex
    $\displaystyle \int_M \exp\Bigl(\frac{y - x}{y + x}\Bigr) \,
    d\lambda^2$, wobei $M \coloneqq \{p \in \set R^2 \mid x(p), y(p) >
    0, x(p) + y(p) \leq 2\}.$
  \item \writtenex \textbf{(2 Punkte)}
    $\displaystyle \int_M (a^2 + x^2 + y^2)^t \, d\lambda^2$, wobei
    $a$, $t \in \set R$, $t \ge 0$ und $M \coloneqq \{p \in \set R^2
    \mid x(p)^2 + y(p)^2 \leq 1\}.$
  \item \writtenex \textbf{(3 Punkte)}
    Sei $f \colon \set R \to \set R$ stetig.  Zeige
    \[
      \int_M f(x \cdot y) \, d\lambda^2 = \ln(2) \cdot \int_1^2 f \,
      d\lambda,
    \]
    wobei $M$ die Menge derjenigen Punkte ist, die im ersten
    Quadranten $Q_1 \coloneqq \{p \in \set R^2 \mid x(p), y(p) \ge
    0\}$ liegen und von den Kurven $xy = 1$, $xy = 2$, $x = y$ und $x
    = 4y$ eingeschlossen werden.
  \end{enumerate}

\item \writtenex \textbf{(10 Punkte)} \textbf{Schwerpunkt- und Trägheitsmoment.}
  \label{ex:mass}
  Sei $\mu$ das Maß einer Massenverteilung im $\set R^3$.  Ferner
  sei $K \subseteq \set R^3$ eine kompakte Teilmenge.  Dann ist
  $\mu(K)$ die \emph{Gesamtmasse} von $K$,
  \[
    S(K) \coloneqq \frac 1 {\mu(K)} \cdot \int_K (x, y, z) \, d\mu
  \]
  der \emph{Schwerpunkt} von $K$ und
  \[
    T(K)\colon \set R^3 \times \set R^3 \to \set R,
    (u, v) \mapsto \int_K \scp{r \times u}{r \times v} \, d\mu
  \]
  mit $r \coloneqq (x, y, z)$ der \emph{Trägheitstensor} von $K$.
  \begin{enumerate}
  \item Berechne den Schwerpunkt der Halbkugel
    \[
      B \coloneqq \{p \in \set R^3 \mid x(p)^2 + y(p)^2 + z(p)^2 \leq r^2
      \land z(p) \ge 0\}
    \]
    vom Radius $r \in \set R^+$.  (D.h.\ $\mu=\lambda^3$. Die Formel
     $\lambda^3(B) = \frac 2
    3 \pi r^3$ muß nicht hergeleitet werden.)
  \item Sei
    $K \coloneqq \{p \in \set R^3 \mid \frac 1 4 x(p)^2 \leq y(p) \leq
    x(p) \land z(p) = 0\}$.  Skizziere $K$.  Wir fassen $K$ als
    "`unendlich dünne"' Scheibe mit homogener Masse auf; daher ist
    $\mu \coloneqq \lambda^2 \otimes \delta_0^1$ das "`richtige"' Maß für
    $K$, wobei $\delta_0^1$ das Dirac-Maß auf $\set R^1$ am Punkt $0$ bezeichne
    (vgl.~Beispiel (b) in Abschnitt 15.3).
    Berechne $\mu(K), S(K)$ und
    $T_i(K) \coloneqq T(K)(e_i, e_i)$, wobei $(e_1$, $e_2$, $e_3)$ die
    Standard-Orthonormalbasis von $\set R^3$ ist.
  \end{enumerate}

% Nächstes Blatt
%\item \oralex \textbf{\name{Guldin}sche Regel für das Volumen eines
%  Rotationskörpers.}
%  Es seien $K \subseteq \interval[open right] 0 \infty \times \set R
%  \subseteq \set R^2$ eine kompakte Menge (in der $(x, z)$-Ebene)
%  und $R$ die $x$-Koordinate des Schwerpunktes von $K$ (vgl.\
%  Aufgabe~\ref{ex:mass}).
%
%  Dann hat der \emph{Rotationskörper}
%  \[
%    K^* \coloneqq \{(r \cos (\phi), r \sin(\phi), t) \in \set R^3 \mid
%    (r, t) \in K \land \phi \in \interval 0 {2 \pi}\}
%  \]
%  das Volumen
%  \[
%    \lambda^3(K^*) = 2 \pi \cdot R \cdot \lambda^2(K).
%  \]
%
%\item
%  \begin{enumerate}
%  \item \oralex
%    Sei $M$ das Segment des Einheitskreises im $\set R^2$, welches von
%    der Sehne von $(1, 0)$ nach $(0, 1)$ begrenzt wird.  Berechne
%    $\int\limits_M xy \, d\lambda^2$.
%  \item \oralex
%    Auf dem Effelsberg regnet es und der Abfluß in der Parabolantenne
%    des Radioteleskops ist verstopft.  Wie hoch steht das Wasser im
%    Spiegel, wenn $n\,\mathrm{mm}$ Niederschlag fallen?  Der maximale
%    Durchmesser der Antenne beträgt $100 \, \mathrm{m}$; die Antenne
%    wird durch die Parabel $f = a \cdot x^2$ mit $a = 8{,}336 \cdot
%    10^{-3}/\mathrm{m}$ parametrisiert.
%  \item \writtenex \textbf{(5 Punkte)} Holzwurm Woody frißt zentral mitten durch eine
%    kleine Holzkugel von zwei Zentimetern Durchmesser ein
%    zylindrisches Loch von zwei Millimetern Durchmesser.  Wieviel
%    Gramm Holz verspeist er dabei, wenn die Dichte $\rho$ des Holzes
%    genau $0{,}7\,\mathrm{g}/\mathrm{cm^3}$ beträgt?
%  \end{enumerate}
%
\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
