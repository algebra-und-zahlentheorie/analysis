\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2022/23}

\title{\bfseries 15.~Übung zur Analysis III}
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Lukas\ Stoll, M.\ Sc.}
\date{09.~Februar -- 20.~Februar 2023
  \thanks{Es sind maximal 35 Bonuspunkte zu erreichen.}
  }

\begin{document}

\maketitle

\begin{enumerate}[start=322]

\item \writtenex \textbf{(10 Punkte)}
  Seien $G$ eine offene Teilmenge eines Banachraums $E$.
  Zeige:
  \begin{enumerate}
  \item
    \label{it:cartan}
    Ist $\omega$ eine $\Diff 2$-Form vom Grade $n$, so gilt
    \[
      d^2 \omega \coloneqq d(d\omega) = 0.
    \]
  \item
    Ist $E = \set R^n$, $X\colon G \to \set R^n$ ein $\Diff {r +
      1}$-Vektorfeld und $\omega$ die $\Diff {r + 1}$-Form
    \[
      \omega = \det(X, \dotsc)
    \]
    vom Grade $n - 1$, so hat $d\omega$ als $\Diff r$-Form vom Grad
    $n$ die Gestalt $f \cdot \det$ mit einer Funktion
    $f \in \Diff r(G, \set R)$.  Diese Funktion heißt die \emph{Divergenz}
    oder \emph{Quellstärke} von $X$ und wird mit $\Div X$ bezeichnet.
    Sie erfüllt also die charakteristische Gleichung
    \[
      d \det(X, \dotsc) = (\Div X) \cdot \det.
    \]
    Ist $X = \sum\limits_{i = 1}^n X_i \cdot e_i$ mit Funktionen $X_i \in
    \Diff {r + 1} (G, \set R)$, so ist
    \[
      \Div_p X = \sum_{i = 1}^n \frac{\partial X_i}{\partial x_i}(p)
      = \tr(J_p X) = \tr (D_p X).
    \]
  \item
    Ist $E = \set R^3$, $X\colon G \to \set R^3$ ein $\Diff {r +
      1}$-Vektorfeld und $\omega$ die $\Diff {r + 1}$-Form
    \[
      \omega = \scp X \cdot,
    \]
    so hat $d\omega$ als $\Diff r$-Form vom Grad $2$ die Gestalt
    $\det(Y, \cdot, \cdot)$ mit einem geeigneten $\Diff r$-Vektorfeld
    $Y\colon G \to \set R^3$.  Dieses Vektorfeld $Y$ heißt die
    \emph{Rotation} oder \emph{Wirbelstärke} von $X$ und wird mit
    $\rot X$ bezeichnet.  Es erfüllt also die charakteristische
    Gleichung
    \[
      d \scp X \cdot = \det(\rot X, \cdot, \cdot);
    \]
    vgl.~auch Beispiel~(a) aus Abschnitt~16.11.
    Ist $X = \sum\limits_{i = 1}^3 X_i \cdot e_i$ mit Funktionen $X_i \in
    \Diff{r + 1}(G, \set R)$, so ist
    \[
      \rot X = \Bigl(
      \frac{\partial X_3}{\partial x_2} - \frac{\partial X_2}{\partial x_3},
      \frac{\partial X_1}{\partial x_3} - \frac{\partial X_3}{\partial x_1},
      \frac{\partial X_2}{\partial x_1} - \frac{\partial X_1}{\partial x_2}
      \Bigr).
    \]
  \item
    Sei $E = \set R^3$.  Zeige:
    \begin{enumerate}
    \item
      Für jede $\Diff 2$-Funktion $f\colon G \to \set R$ ist
      \[
        \rot(\grad f) = 0.
      \]
    \item
      Für jedes $\Diff 2$-Vektorfeld $X\colon G \to \set R^3$ ist
      \[
        \Div(\rot X) = 0.
      \]
    \end{enumerate}
    (Tip:~\ref{it:cartan}.)
  \end{enumerate}

\item \writtenex \textbf{(5 Punkte)}
  Sei $X \coloneqq (x, y^2, z^3)\colon \set R^3 \to \set R^3$.
  Berechne erneut den Fluß des Vektorfeldes $X$ durch die Oberfläche
  des Würfels; dieses Mal mit Hilfe des Satzes von \name{Gauß}.  Vergleiche
  das Ergebnis mit dem Ergebnis von Aufgabe 314.

\item \writtenex \textbf{(10 Punkte)}
  Berechne
  \begin{enumerate}
  \item $\displaystyle \int_S ((x - y)^3 \, dx + x^3 \, dy)$ mittels
    des Satzes von \name{Green}, wobei $S$ die (mathematisch positiv
    orientierte) Kreislinie um $0$ mit dem Radius $1$ in $\set R^2$
    ist;
  \item
    $\displaystyle \int_S (xy + yz + zx) \, d\lambda^3$ mittels des
    Satzes von \name{Gauß}, wobei $S \coloneqq \{p \in \set R^3 \mid x(p),
    y(p), z(p) \ge 0, x(p)^2 + y(p)^2 + z(p)^2 \leq 1\}$;
  \item
    $\displaystyle \int_S (yz^2 \, dx + (x z^2 - 2x) \, dy + 2 xyz \,
    dz)$ mittels des Satzes von \name{Stokes}, wobei $S \coloneqq \{p \in
    \set R^3 \mid x(p) = \cos(t), y(p) = \sin(t), z(p) = \cos(t), t
    \in \interval 0 {2\pi}\}$.
  \end{enumerate}

\item \writtenex \textbf{(5 Punkte)} \textbf{Eine Formel für den Flächeninhalt einer
    ebenen Figur.}
  Sei ein positiv orientiertes $\Diff 2$-Pflaster
  $F\colon \interval 0 1 \times \interval 0 1 \to \set R^2$ im $\set R^2$
  gegeben; sei weiter
  $\alpha\colon \interval 0 4 \to \set R^2$ seine "`Randkurve"',
  welche durch
  \[
    \alpha(t) \coloneqq
    \begin{cases}
      F(t, 0)     & \text{für $0 \leq t \leq 1,$} \\
      F(1, t - 1) & \text{für $1 \leq t \leq 2,$} \\
      F(3 - t, 1) & \text{für $2 \leq t \leq 3,$} \\
      F(0, 4 - t) & \text{für $3 \leq t \leq 4$} \\
    \end{cases}
  \]
  definiert ist.  Zeige:
  \[
    \lambda^2(F) = \int_\alpha x \, dy.
  \]

\item \writtenex \textbf{(5 Punkte)} \textbf{Abschmelzen eines Schneeballs.}
  Das Volumen $V$ eines kugelförmigen Schneeballs vermindere sich
  durch Abschmelzen mit einer zeitlichen Rate, die proportional zur
  jeweils vorhandenen Oberfläche $F$ ist, also
  \[
    \frac{dV}{dt} = - \alpha \cdot F,
  \]
  wobei $\alpha$ die bekannte "`Schmelzkonstante"' ist.
  Sei $r_0$ der Radius des Schneeballs zu Beginn des
  Abschmelzvorgangs; $r(t)$ sein Radius nach Ablauf der Zeit $t$.
  Nach einer Zeiteinheit möge sich das ursprüngliche Volumen $V_0
  \coloneqq V(0)$ um $p$~Prozent vermindert haben.  Zeige, daß $r(t) =
  r_0 - \alpha t$ ist und daß die gesamte Schmelzzeit $T$ durch
  \[
    T = \frac{1}{1 - (1 - \frac p{100})^{\frac 1 3}}
  \]
  gegeben ist.  Wähle als Zeiteinheit $1$ Stunde, gib Dir einige
  $p$-Werte vor, und berechne die zugehörigen Schmelzzeiten.

  Das obige mathematische Modell kann auch für das Lutschen eines
  runden Bonbons verwendet werden.  Allerletzte Übungsaufgabe:
  Verifiziere das obige Ergebnis experimentell.

\end{enumerate}

\begin{verse}
  \begin{center}
    Wir wünschen Euch allen ein erfolgreiches weiteres Studium!
    \\
    \name{Marc Nieper-Wißkirchen} und \name{Lukas Stoll}
  \end{center}
\end{verse}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
