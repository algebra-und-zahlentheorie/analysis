\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2021/22}

\title{\bfseries 11.~Übung zur Analysis I} 
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Dr.\ Ingo Blechschmidt}  
\date{11.~Januar 2022\thanks{Die Übungsblätter sind bis zur Übung
    am 18.~Januar 2022 zu bearbeiten.}}

\begin{document}

\maketitle

\begin{enumerate}[start=65]

\item \oralex Seien $E$ und $E'$ metrische Räume, und seien $f$,
  $g\colon E \to E'$ stetige Abbildungen.  Zeige, daß die Menge
  $\{p \in E \mid f(p) = g(p)\}$ in $E$ abgeschlossen ist.

\item Sei $E$ ein metrischer Raum.
  \begin{enumerate}   
  \item \oralex 
    \label{it:closed}
    Zeige, daß eine Teilmenge $A \in \powerset E$ genau
    dann in $E$ abgeschlossen ist, wenn gilt:
    \[
    \forall p \in E \setminus A\, \exists \epsilon \in \set R_+\colon
    U_\epsilon(p) \cap A = \emptyset.
    \]
    (Tip: Proposition aus 4.8.)

  \item \writtenex Sei $E'$ ein weiterer metrischer Raum, seien $A$ und $B$
    abgeschlossene Teilmengen von $E$, sowie $f\colon A \to E'$ und
    $g\colon B \to E'$ stetige Abbildungen mit
    $f|(A \cap B) = g|(A \cap B)$.  Zeige, daß genau eine Abbildung
    $h\colon A \cup B \to E'$ mit $h|A = f$ und $h|B = g$ existiert
    und daß diese Abbildung stetig ist.
    (Tip: Für $p \in (A \cup B) \setminus (A \cap B)$ benutze Aufgabenteil \ref{it:closed}.)
  \end{enumerate}

\item \writtenex
  \begin{enumerate}
  \item Sei $B\colon \set R^n \to \set R^n$ eine lineare Abbildung;
    bekanntlich wird diese bezüglich der kanonischen Basis eindeutig
    durch eine $(n \times n)$-Matrix $(b_{ik})_{i, k = 1, \ldots, n}$
    beschrieben.  Sei weiter
    $M \coloneqq \max\{\sum_{k = 1}^n \abs{b_{ik}} \mid i = 1, \ldots,
    n\}$
    die \emph{Zeilensummennorm} dieser Matrix $(b_{ik})_{i,k}$.
    Ferner bezeichne $\anorm \cdot$ die Maximumsnorm des $\set R^n$
    (siehe Beispiel 2 aus Abschnitt 5.2).  Zeige:
    \[
    \forall v \in \set R^n\colon \anorm {Bv} \leq M \cdot \anorm v.
    \]
    \paragraph{Bemerkung.} Die Zahl $M$ werden wir später als die
    \emph{Operatornorm} der linearen Abbildung $B$ bezüglich der
    Maximumsnorm des $\set R^n$ erkennen.
  \item Wir betrachten ein lineares Gleichungssystem (LGS) mit $n$ Gleichungen
    \begin{align}
      \label{eq:lgs}
      \sum_{k = 1}^n a_{ik} v_k & = b_i & \text{für $i = 1$, \dots, $n$}
    \end{align}
    für $n$ Unbekannte $v_1$, \dots, $v_n \in \set R$ mit $a_{ik}$,
    $b_i \in \set R$ für $i$, $k = 1, \dotsc, n$.  Ein solches LGS
    läßt sich bekanntlich immer in der Form
    \[
    A v = b
    \]
    schreiben, wobei $A\colon \set R^n \to \set R^n$ die durch
    $(a_{ik})_{i, k = 1, \ldots, n}$ beschriebene lineare Abbildung
    ist, und $v = (v_1, \dotsc, v_n)$ und $b = (b_1, \dotsc, b_n)$.

    Zeige:  Gilt $M < 1$ für die Operatornorm der linearen Abbildung
    \[
    B \coloneqq A - \id_{\set R^n}\colon \set R^n \to \set R^n,
    \]
    so besitzt das LGS \eqref{eq:lgs} genau eine Lösung.
    
    (Tip: Finde eine geeignete Abbildung
    $f\colon \set R^n \to \set R^n$, auf die Du den Banachschen
    Fixpunktsatz anwenden kannst.)

    \paragraph{Interpretation.}
    Für $A = \id_{\set R^n}$ besitzt das LGS \eqref{eq:lgs} natürlich
    genau eine Lösung, und obiges Resultat besagt nun, daß
    \eqref{eq:lgs} auch dann noch genau eine Lösung besitzt, wenn sich
    $A$ "`nicht zu weit entfernt von der Identität befindet"'.
  \end{enumerate}

\item \writtenex Zeige: Für vier paarweise verschiedene komplexe
  Zahlen $z_1$, $z_2$, $z_3$, $z_4$ mit
  $\abs{z_1}=\abs{z_2}=\abs{z_3}=\abs{z_4}$ sind die folgenden Aussagen äquivalent:
  \begin{enumerate}
  \item
    $z_1$, $z_2$, $z_3$ und $z_4$ bilden die Ecken eines Rechtecks.
  \item
    $z_1 + z_2 + z_3 + z_4 = 0$.
  \item $z_1$, $z_2$, $z_3$ und $z_4$ sind die Nullstellen eines
    Polynoms $(z^2 - a^2) (z^2 - b^2)$ mit $a$, $b \in \set C$,
    $a^2 \neq b^2$, $\abs a = \abs b$.
  \end{enumerate}
  (Tip: Im Beweis dürfen auch Winkelüberlegungen angestellt werden.
  Bekanntlich besitzt jede komplexe Zahl $z$ die Darstellung
  $z = \abs z \cdot (\cos(\phi) + i \cdot \sin(\phi))$ mit
  $\phi \in \left[0, 2\pi\right[$.)

\item \oralex
  Zeige: In jedem normierten $\set K$-Vektorraum sind die $\epsilon$-Umgebungen
  konvex. Dazu beachte:
  \paragraph{Definition.}
  Eine Teilmenge $M$ eines $\set K$-Vektorraumes $E$ heißt
  \emph{konvex}, wenn für je zwei Vektoren $v$, $w \in M$ auch deren
  \emph{Verbindungsstrecke}
  \[
  [v, w] \coloneqq \{v + t \cdot (w - v) = (1 - t) \cdot v + t \cdot w\mid t \in [0, 1]\}
  \]
  ganz in $M$ liegt.

\item \oralex Zeige:
  \begin{enumerate}
  \item Ist $V$ ein normierter $\set K$-Vektorraum und ist $U$ ein
    Untervektorraum von $V$, so ist die abgeschlossene Hülle
    $\overline U$ ebenfalls ein Untervektorraum von $V$, d.\,h.~mit
    $u$, $v \in \overline U$ und $a \in \set K$ sind auch
    $u + v \in \overline U$ und $a \cdot u \in \overline U$.

    \paragraph{Bemerkung.}
    In endlich-dimensionalen Vektorräumen gilt sogar, daß jeder
    Untervektorraum abgeschlossen ist.
  \item Ist $V$ ein normierter $\set K$-Vektorraum und $A \subseteq V$ konvex,
    dann ist auch $\overline A$ konvex.
  \end{enumerate}

\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
