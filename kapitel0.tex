
\chapter{Die grundlegenden Elemente der mathematischen Sprache}

Dieses Kapitel ist völlig untypisch für die Vorlesung, welches auch
durch die ungewöhnliche Numerierung angedeutet wird.  Neben
Literaturhinweisen werden die Bezeichnungen der für die Analysis
wichtigsten Mengen aufgeführt und dann die fundamentalen logischen und
mengentheoretischen Sprachelemente der Mathematik beschrieben.

\section{Die grundlegenden Mengen der Analysis}

\begin{definition*}
  Für die grundlegenden Mengen der Analysis verabreden wir folgende Bezeichnungen:
  \begin{itemize}
  \item $\set N_0$ die Menge der natürlichen Zahlen $0$, $1$, $2$, \dots,
  \item $\set Z$ die Menge der ganzen Zahlen \dots, $-2$, $-1$, $0$, $1$, $2$, \dots,
  \item $\set N_m \coloneqq \{n \in \set Z \mid n \ge m\}$ für jedes $m \in \mathbf Z$,
  \item $\set Q$ die Menge der rationalen Zahlen,
  \item $\set R$ die Menge der reellen Zahlen,
  \item $\set R_+ \coloneqq \{a \in \set R \mid a > 0\}$,
  \item $\set R^* \coloneqq \set R \setminus \{0\}$.
  \end{itemize}
\end{definition*}

Hierbei bedeutet $a \coloneqq \ldots$, daß das Objekt $a$ durch die
rechte Seite $\ldots$ der "`Gleichung"' definiert wird; entsprechend
ist $\ldots \eqqcolon a$ zu verstehen.

\section{Literatur}

Die folgende Liste empfehlenswerter Literatur ist sicherlich nicht
vollständig.  Es schadet nicht, nicht nur mit einem Buch zu arbeiten.

\begin{itemize}
\item
  Barner, M., Flohr, F.: \emph{Analysis I}, 4.~Aufl.~1991, de Gruyter.
\item
  Bartle, R.: \emph{Elements of Integration}, 1966, John Wiley \& Sons Inc.
\item
  Bauer, H.: \emph{Maß- und Integrationstheorie}, 2.~Aufl.~1992, de Gruyter.
\item
  Cartan, H.: \emph{Differentialrechnung}. Mannheim 1985, B.~I.
\item
  Deimling, K.: \emph{Nonlinear Functional Analysis}. Berlin 1985, Springer.
\item
  Dieudonné, J.: \emph{Grundzüge der modernen Analysis}, 3.~Auf\/l. Braunschweig 1985, Vieweg.
\item
  Greenberg, M.: \emph{Lectures on Algebraic Topology}.  1967, W.~A.~Benjamin.
\item
  Heuser, H.: \emph{Lehrbuch der Analysis, Teil 1}, 15.~Auf\/l. 2003, Vieweg+Teubner.
\item Hirzebruch, F., Scharlau, W.: \emph{Einführung in die
    Funktionalanalysis}, 1991, Spektrum Akademischer Verlag.
\item
  Köhler, G.: \emph{Analysis}, 2006, Heldermann.
\item
  Kowalski, H.-J., Michler G.: \emph{Lineare Algebra}, 12.~Auf\/l. 2003, Walter de Gruyter.
\item
  Lang, S.: \emph{Real and Functional Analysis}, 3.~Auf\/l. 1993, Springer.
\item
  Lang, S.: \emph{Undergraduate Analysis}, 4.~Auf\/l. 2005, Springer.
\item
  Jänich, K.: \emph{Analysis für Physiker und Ingenieure}, 4.~Auf\/l. Berlin 2001, Springer.
\item
  Schubert, H.: \emph{Topologie. Eine Einführung}, 4.~Auf\/l. 1975, Teubner.
\item
  Storch, U., Wiebe H.: \emph{Lehrbuch der Mathematik, Band 1: Analysis einer Veränderlichen},
  3.~Auf\/l. 2003, Springer.
\item
  Walter, R.: \emph{Einführung in die lineare Algebra}, 4.~Auf\/l. 1996, Vieweg+Teubner.
\item Warner, F.\ W.: \emph{Foundations of differentiable manifolds
    and Lie groups}, 1971, Springer.
\end{itemize}

\section{Symbole der Logik}

\begin{namedthm}{Tertium non datur\footnote{Lat.\ für "`ein Drittes ist nicht gegeben"'}}
  Eine \emph{Aussage} ist (entweder) wahr oder
  falsch.\footnote{Genaugenommen handelt es sich hierbei nicht um ein
    einzelnes Axiom, sondern um ein ganze Familie von Axiomen: Für
    jede einzelne Aussage $A$ ist "`$A$ ist wahr oder falsch"' ein
    Axiom. Eine solche Familie heißt richtiger \emph{Axiomenschema}.}
\end{namedthm}

\begin{example}
  Beispiele für Aussagen sind etwa:
  \begin{enumerate}
  \item "`$0$ ist eine natürliche Zahl."'
  \item "`Jede natürliche Zahl ist positiv."'
  \end{enumerate}
\end{example}

Aussagen können durch \emph{Junktoren} zu neuen Aussagen verknüpft werden:
\begin{definition}
  Seien $A$ und $B$ zwei Aussagen. Dann steht
  \begin{itemize}
  \item
    $\lnot A$ für die \emph{Negation} "`nicht $A$"',
  \item $A \land B$ für die \emph{Konjunktion} "`$A$ und $B$"',
  \item $A \lor B$ für die \emph{Disjunktion} "`$A$ oder $B$ (oder beide)"',
  \item $A \implies B$ für die \emph{Implikation} "`aus $A$ folgt $B$"',
  \item $A \iff B$ für die \emph{Äquivalenz} "`$A$ genau dann, wenn $B$"'.
  \end{itemize}
\end{definition}

Aufgrund des "`Tertium non datur"' kann jede mathematische Aussage nur
zwei Wahrheitswerte annehmen.  Damit können wir die logischen
Junktoren durch endliche Tabellen beschreiben, die sogenannten
\emph{Wahrheitstafeln}.  Die folgende Tabelle beschreibt zum Beispiel
die Operation der Negation:
\[
\begin{array}{c|c}
  A & \lnot A \\
  \hline
  w & f \\
  f & w
\end{array}
\]
Hierbei steht $w$ für "`wahr"' und $f$ für "`falsch"'.

\begin{theorem*}
  Sind $A$ und $B$ zwei Aussagen, so ist
  \[
    \lnot (A \implies B) \quad\text{äquivalent zu}\quad A \land (\lnot B).
  \]
\end{theorem*}

\begin{proof}
  Wir stellen eine Wahrheitstafel für alle möglichen Kombinationen der
  Wahrheitswerte von $A$ und $B$ auf und stellen fest, daß in allen
  vier Fällen die Wahrheitswerte der Aussagen $\lnot(A \implies B)$ und
  $A \land (\lnot B)$ gleich sind:
  \[
  \begin{array}{c|c|c|c|c|c}
    A & B & A \land B & A \implies B & \lnot(A \implies B) & A \land \lnot B \\
    \hline
    w & w & w         & w            & f            & f                      \\
    w & f & f         & f            & w            & w                      \\
    f & w & f         & w            & f            & f                      \\
    f & f & f         & w            & f            & f
  \end{array}
  \]
\end{proof}

In der Spalte "`$A \land B$"' der Wahrheitstafel des Beweises haben
wir noch einmal den Wahrheitsgehalt der Konjuktion festgelegt.
Insbesondere wollen wir besonders auf den Wahrheitswert der
Implikation in der Spalte "`$A \implies B$"' hinweisen.  Es gilt nämlich:

\begin{proposition}
  Ist die Aussage $A$ falsch, so ist die Aussage $A \implies B$ in jedem Falle wahr.
\end{proposition}

Diese Festsetzung mag zunächst nicht einleuchtend erscheinen,
entspricht aber auch der Alltagslogik.  Sagt etwa Adam zu Eva: "`Wenn
es morgen regnet, dann komme ich zu Dir."', behauptet Adam also die
Implikation
\[
\textrm{"`Morgen regnet es."'} \implies \textrm{"`Adam geht zu Eva."'},
\]
und regnet es morgen nicht, so hat Adam in keinem Fall gelogen, ob er
nun zu Eva geht oder nicht.

\begin{exercise*}
  Sind $A$ und $B$ zwei Aussagen, so ist
  \[
    A \implies B \quad\text{äquivalent zu}\quad \lnot B \implies \lnot A.
  \]

  (Tip: Man stelle für beide Aussagen die Wahrheitstafeln auf; man
  vergleiche dazu den Beweis des vorangegangenen Theorems.)
\end{exercise*}

\begin{definition}
  Sei $A$ eine Aussage, und sei $M$ eine Menge. Dann steht
  \begin{itemize}
  \item
    $\forall a \in M\colon A$ für die Aussage "`für alle $a \in M$ gilt die Aussage $A$"',
  \item
    $\exists a \in M\colon A$ für die Aussage "`es existiert ein $a \in M$, für das die Aussage $A$ gilt"',
  \item
    $\exists! a \in M\colon A$ für die Aussage "`es existiert genau ein $a \in M$, für das die Aussage $A$ gilt"'.
  \end{itemize}
\end{definition}

\begin{example}
  Für die Funktion $f = x^2 - 4 x + 5$ gilt
  \[
  \forall a \in \set R\, \forall \epsilon \in \set R_+ \, \exists \delta \in \set R_+\,
  \forall t \in \set R\colon \bigl(\abs{t - a} < \delta \implies \abs{f(t) - f(a)} < \epsilon\bigr).
  \]
  In Worten: Zu jedem $a \in \set R$ und zu jedem
  $\epsilon \in \set R_+$ existiert ein $\delta \in \set R_+$, so daß
  für alle $t \in \set R$ gilt: Es ist $\abs{f(t) - f(a)} < \epsilon$,
  wenn $\abs{t - a} < \delta$. Kurz gesagt: Die Funktion $f$ ist in
  allen Punkten $a \in \set R$ stetig.
\end{example}

\begin{proposition}
  Sei $A$ eine Aussage, und sei $M$ eine Menge.
  \begin{enumerate}
  \item
    $\lnot \forall a \in M\colon A \iff \exists a \in M\colon \lnot A$,
  \item
    $\lnot \exists a \in M\colon A \iff \forall a \in M\colon \lnot A$.
  \end{enumerate}
\end{proposition}

\begin{warning*}
  Die Quantoren $\forall$, $\exists$ und $\exists!$ sollten sprachlich
  richtig benutzt werden.  Insbesondere stehen Quantoren vor der
  Aussage, über die quantifiziert wird.  Nur bei Beachtung dieser
  Vorschrift können die Regeln obiger Proposition systematisch zum
  Negieren von Aussagen verwendet werden.

  Die Reihenfolge der Quantoren ist wesentlich.  Verschieben wir etwa
  im obigen Beispiel den Quantor "`$\forall a \in M$"', so daß er
  hinter den Quantor "`$\exists \delta \in M$"' zu stehen kommt, so
  erhalten wir die falsche Aussage, daß $f$ eine gleichmäßig stetige
  Funktion ist.
\end{warning*}

Schließlich sei bemerkt, daß durch $A \coloniff B$ ausgedrückt wird,
daß die Aussage $A$ durch die Aussage $B$ definiert wird. Z.\,B.\
könnten wir für eine Funktion der Form $f\colon \set R \to \set R$ definieren:
\begin{multline*}
\textrm{"`$f$ ist in $a$ stetig"'} \\
\coloniff
\forall \epsilon \in \set R_+ \, \exists \delta \in \set R_+\,
\forall t \in \set R\colon \bigl(\abs{t - a} < \delta \implies \abs{f(t) - f(a)} < \epsilon\bigr).
\end{multline*}

\section{Mengentheoretische Bezeichnungen}

Die Mengenlehre benutzen wir weitestgehend naiv, achten aber darauf,
keine unerlaubten Mengen zu bilden.

Mengen sind Zusammenfassungen von gewissen mathematischen Objekten zu
einem neuen mathematischen Objekt.  Ist $a$ ein mathematisches Objekt
und $M$ eine Menge, so steht $a \in M$ (oder $M \ni a$) für die
Aussage, daß $a$ ein Element von $M$ ist (wir sagen auch, daß $a$ in
$M$ \emph{liegt}).  Wir definieren
\[
a \notin M \coloniff \lnot (a \in M).
\]
Die Tatsache, daß sich eine Menge durch die durch sie zusammengefaßten
Elemente definiert, können wir mathematisch auch durch folgendes Axiom
beschreiben:
\begin{namedthm}{Bestimmtheitsaxiom}[R.\ Dedekind 1888]
  Sind $M$ und $N$ zwei Mengen, so gilt
  \[
  M = N \iff \bigl(\forall a \in M\colon a \in N\big)
  \land \bigl(\forall a \in N\colon a \in M\bigr).
  \]
\end{namedthm}

Neue Mengen erhalten wir aus bekannten Mengen mit Hilfe folgender Axiome:
\begin{namedthm}{Elementarmengenaxiom}
  Sind $a_1$, $a_2$, \dots, $a_n$ mathematische Objekte, so existiert
  eine Menge $M$, die genau diese Objekte als Elemente enthält.  Wir
  schreiben dann $M = \{a_1, a_2, \dotsc, a_n\}$.  (Es sei beachtet,
  daß diese Menge aufgrund des vorangehenden Axioms eindeutig bestimmt
  ist.)
\end{namedthm}

\begin{example}
  Die \emph{leere Menge} $\emptyset \coloneqq \{\}$ ist eine Menge,
  die wir durch Angabe ihrer Elemente (nämlich keines) angeben können.
\end{example}

\begin{namedthm}{Aussonderungsaxiom}
  Ist $M$ eine Menge und $A$ eine Aussage, so existiert eine Menge
  $N$, deren Elemente genau diejenigen Elemente von $M$ sind, auf die
  die Aussage $A$ zutrifft.  Wir schreiben dann
  $N = \{a \in M \mid A\}$.
\end{namedthm}

\begin{example}
  Das Aussonderungsaxiom ist uns schon einmal, bei der Definition von
  $\set R_+ = \{a \in \set R \mid a > 0\}$ begegnet.
\end{example}

Mit Hilfe des Aussonderungsaxioms können Teilmengen von Mengen konstruiert werden:
\begin{definition}
  Eine Menge $N$ heißt \emph{Teilmenge} einer Menge $M$, wenn jedes
  Element von $N$ auch zu $M$ gehört.  Wir schreiben dann
  $N \subseteq M$.
\end{definition}

Das Bestimmtheitsaxiom liefert uns folgende Beweisstrategie für die Gleichheit zweier Mengen:
\begin{proposition*}
  Sind $M$ und $N$ zwei Mengen, so gilt
  \[
  M = N \iff M \subseteq N \land N \subseteq M.
  \]
\end{proposition*}

\begin{namedthm}{Potenzmengenaxiom}
  Ist $M$ eine Menge, so existiert eine Menge $\powerset M$, deren
  Elemente gerade die Teilmengen von $M$ sind.  Wir nennen die Menge
  $\powerset M$ die \emph{Potenzmenge} von $M$.
\end{namedthm}
Es sei beachtet, daß die Potenzmenge immer mindestens ein Element
besitzt, nämlich $\emptyset \in \powerset M$.

\begin{namedthm}{Vereinigungsmengenaxiom}
  Ist $N$ eine Menge von Mengen, so
  existiert eine Menge $\bigcup N = \bigcup_{A \in N} A$ mit
  \[
  p \in \bigcup N \iff \exists A \in N\colon p \in A.
  \]
  Wir nennen $\bigcup N$ die \emph{Vereinigung} des Mengensystems $N$.

  Im Falle, daß $A$ und $B$ zwei Mengen sind, schreiben wir
  \[
  A \cup B \coloneqq \bigcup \{A, B\}
  \]
  und nennen $A \cup B$ die \emph{Vereinigung} von $A$ und $B$. Es gilt dann
  \[
  p \in A \cup B \iff p \in A \lor p \in B.
  \]
\end{namedthm}

\begin{namedthm}{Produktmengenaxiom}
  Sind $M$ und $N$ zwei Mengen, so existiert die Menge $M \times N$
  der \emph{geordneten Paare} $(p, q)$ mit $p \in M$ und $q \in N$.
  Wir nennen $M \times N$ das \emph{kartesische Produkt} von $M$ und $N$.
\end{namedthm}

Schließlich können wir mit Hilfe der schon angegeben Axiome weitere Mengen formen:
\begin{definition}
  Es seien $M$ und $N$ zwei Mengen.
  \begin{enumerate}
  \item
    Es heißt
    \[
    M \cap N \coloneqq \{a \in M \mid a \in N\} = \{a \in N\mid a \in M\}
    \]
    der \emph{Durchschnitt} von $M$ und $N$.
  \item
    Schließlich ist
    \[
    M \setminus N \coloneqq \{a \in M \mid a \notin N\}
    \]
    das \emph{Komplement} von $N$ in $M$.
  \end{enumerate}
\end{definition}

\begin{exercise*}
  Sind $M$ und $N$ Mengen, so gilt
  \[
  M \setminus (M \setminus N) = M \cap N.
  \]
\end{exercise*}

\section{Abbildungen und Funktionen}
\label{sec:maps}

\begin{definition}
  \leavevmode
  \begin{enumerate}
  \item Eine \emph{Abbildung} einer Menge $M$ in eine Menge $N$ ist
    eine Teilmenge des kartesischen Produkts $f \subseteq M \times N$
    zusammen mit der Angabe des \emph{Bildbereiches} $N$, so daß
    \begin{equation}
      \label{it:map}
      \forall p \in M\, \exists! q \in N\colon (p, q) \in f.
    \end{equation}
    Wir schreiben dann
    \[
    f\colon M \to N.
    \]
    Anstelle von $(p, q) \in f$ schreiben wir $q = f(p)$ oder
    $f\colon p \mapsto q$. Den \emph{Definitionsbereich} $M$ von $f$
    bezeichnen wir auch mit $D_f$.
  \item Ist $f\colon M \to N$ eine Abbildung, $A \in \powerset M$ und
    $B$ eine beliebige Menge, so definieren wir das \emph{Bild} von $A$ unter $f$ durch
    \[
    f(A) \coloneqq \{f(p) \mid p \in A\} \coloneqq \{q \in N \mid \exists p \in A\colon q = f(p)\}
    \]
    und das \emph{Urbild} von $B$ bezüglich $f$ durch
    \[
    f^{-1}(B) \coloneqq \{p \in M \mid f(p) \in B\}.
    \]
  \end{enumerate}
\end{definition}

\begin{comment*}
  Wir haben in obiger Definition beschrieben, was auch als
  \emph{Graph} einer Funktion bekannt ist.  Aufgrund von
  \eqref{it:map} wird durch $f$ eine \emph{Zuordnung} definiert.  Die
  Begriffe "`Abbildung"' und "`Funktion"' werden häufig synonym
  verwendet.  Wir bevorzugen die Bezeichnung "`Funktion"', wenn $N$
  ein Rechenbereich wie etwa $\mathbf R$ ist.
\end{comment*}

Aus der folgenden Proposition leitet sich sofort eine Beweisstrategie für Abbildungen ab:
\begin{proposition}
  Sind $f$ und $g$ Abbildungen in $N$, so gilt
  \[
  f = g \iff \bigl(D_f = D_g \land \forall p \in D_f\colon f(p) = g(p)\bigr).
  \]
\end{proposition}

\begin{examples*}
  Seien $M$, $M_1$, $M_2$, $N$, $N_1$, $N_2$ Mengen.
  \begin{enumerate}
  \item Die \emph{Identität} von $M$ ist die Abbildung
    $\id_M \coloneqq \{(p, p) \in M \times M \mid p \in M\}$, also
    $\id_M\colon M \to M, p \mapsto p$.
  \item
    Ist $M$ eine Teilmenge von $N$, so bezeichne $M \injto N$ die \emph{Inklusion} von $M$ in $N$;
    das ist die Abbildung $M \to N, p \mapsto p$.
  \item Ist $M = \emptyset$, so ist $M \times N = \emptyset$, also ist
    die leere Menge die einzige Teilmenge von $M \times N$.  Diese ist
    trivialerweise eine Abbildung; wir wollen sie die \emph{leere
      Abbildung} nennen.
  \item Die \emph{Komposition} zweier Abbildungen
    $f_1\colon M_1 \to N_1$ und $f_2\colon M_2 \to N_2$ ist die Abbildung
    $f_1 \circ f_2\colon f_2^{-1}(M_1) \to N_1, p \mapsto f_1(f_2(p))$.
  \item
    Ist $f\colon M \to N$ eine Abbildung und $A \subseteq M$, so heißt
    \[
    f|A \coloneqq f \circ (A \injto M)\colon A \to N, p \mapsto f(p)
    \]
    die \emph{Restriktion} von $f$ auf $A$.
  \end{enumerate}
\end{examples*}

\begin{exercise}
  Es seien $f_1\colon M_1 \to N_1$ und $f_2\colon M_2 \to N_2$ zwei Abbildungen. Man beweise:
  \[
  \text{$f_1 \circ f_2$ ist die leere Abbildung}\iff f_2(M_2) \cap M_1 = \emptyset.
  \]
\end{exercise}

\begin{proposition}
  Für jede Abbildung $f\colon M \to N$ und für jede Wahl von
  Teilmengen $A_1$, $A_2 \in \powerset M$ und $B$, $B_1$,
  $B_2 \in \powerset N$ gilt:
  \begin{enumerate}
  \item $A_1 \subseteq A_2 \implies f(A_1) \subseteq f(A_2)$,
  \item $B_1 \subseteq B_2 \implies f^{-1}(B_1) \subseteq f^{-1}(B_2)$,
  \item $f^{-1}(N \setminus B) = M \setminus f^{-1}(B)$ und allgemeiner $f^{-1}(B_1 \setminus B_2)
    = f^{-1}(B_1) \setminus f^{-1}(B_2)$.
  \end{enumerate}
\end{proposition}

\begin{definition}
  Ist $f\colon M \to N$ eine Abbildung, so definieren wir:
  \begin{enumerate}
  \item
    Die Abbildung $f$ heißt \emph{injektiv}, wenn
    \[
    \forall p_1, p_2 \in M\colon \bigl(f(p_1) = f(p_2) \implies p_1 = p_2\bigr).
    \]
    Im Falle der Injektivitität ist
    \[
    \check f \coloneqq \{(f(p), p) \mid p \in M\}
    \]
    eine Abbildung von $f(M)$ auf $M$
    (vgl.~\ref{it:surjective}), die \emph{Umkehrabbildung} von $f$.
  \item
    \label{it:surjective}
    Die Abbildung $f$ heißt \emph{surjektiv} oder eine Abbildung \emph{auf} $N$,
    wenn $f(M) = N$.
  \item
    Die Abbildung $f$ heißt \emph{bijektiv}, wenn sie sowohl injektiv als auch
    surjektiv ist.
  \end{enumerate}
\end{definition}

\begin{proposition}
  Sind $f\colon M \to N$ und $g\colon N \to K$ Abbildungen, so gilt:
  Sind $f$ und $g$ injektiv (bzw.\ surjektiv bzw.\ bijektiv), so ist
  auch $g \circ f$ injektiv (bzw.\ surjektiv bzw.\ bijektiv).
\end{proposition}

Die folgende Aussage liefert ein wertvolles Kriterium für die
Injektivität und Surjektivität von Abbildungen:
\begin{proposition}
  \label{prop:inj_sur_crit}
  Sind $f\colon M \to N$ und $g\colon N \to M$ Abbildungen und gilt für die
  Komposition $g \circ f = \id_M$, so ist $f$ injektiv, $g$ surjektiv und
  $\check f = g|f(M)$.  Gilt außerdem $f \circ g = \id_N$, so sind daher
  $f$ und $g$ trivialerweise Bijektionen und $\check f = g$ und
  $\check g = f$.
\end{proposition}

\begin{proposition}
  Ist $f\colon M \to N$ injektiv, so ist $\check f\colon f(M) \to M$
  eine \emph{Bijektion}, d.h.\ eine bijektive Abbildung, und es gelten
  \begin{align*}
    \check f \circ f & = \id_M & \text{und} &
    & f \circ \check f & = (f(M) \injto N).
  \end{align*}
  Ist insbesondere $f$ eine Bijektion, so ist auch
  $\check f\colon N \to M$ eine Bijektion.
\end{proposition}

\begin{exercise}
  Es seien $f\colon M \to N$ eine Abbildung, $A \subseteq M$ und $B \subseteq N$. Beweisen Sie:
  \begin{enumerate}
  \item
    \label{it:preimage_of_image}
    $A \subseteq f^{-1}(f(A))$ und Gleichheit gilt in jedem der folgenden Fälle:
    \begin{enumerate}
    \item
      \label{it:injective_preimage_of_image}
      $f$ ist injektiv,
    \item
      \label{it:full_preimage_of_image}
      $A = f^{-1}(B)$ für ein beliebiges $B \in \powerset N$.
    \end{enumerate}
  \item
    \label{it:image_of_preimage}
    $f(f^{-1}(B)) \subseteq B$, und Gleichheit gilt in jedem der folgenden Fälle:
    \begin{enumerate}
    \item
      \label{it:surjective_image_of_preimage}
      $f$ ist surjektiv.
    \item
      \label{it:full_image_of_preimage}
      $B = f(A)$ für ein beliebiges $A \in \powerset M$.
    \end{enumerate}
  \end{enumerate}
  (Anleitung: Man beweise zunächst die Inklusionen und dann die Teile
  \ref{it:preimage_of_image}\ref{it:injective_preimage_of_image} und \ref{it:image_of_preimage}\ref{it:surjective_image_of_preimage},
  und folgere anschließend unter alleiniger
  Ausnutzung der bewiesenen Inklusionen die Teile \ref{it:preimage_of_image}\ref{it:full_preimage_of_image}
  und \ref{it:image_of_preimage}\ref{it:full_image_of_preimage}.)
\end{exercise}

\section{Mächtigkeit von Mengen}

\begin{definition*}
  Zwei Mengen $M$ und $N$ heißen \emph{gleichmächtig}, wenn es eine
  Bijektion $M \to N$ gibt.  In diesem Falle schreiben wir
  \[
  \abs M = \abs N.
  \]
\end{definition*}

\begin{proposition*}
  "`Gleichmächtigkeit"' ist eine Äquivalenzrelation, d.h.\ sind $M$, $N$ und $K$ Mengen, so gilt
  \begin{enumerate}
  \item $\abs M = \abs M$,
  \item $\abs M = \abs N \implies \abs N = \abs M$,
  \item $\bigl(\abs M = \abs N \land \abs N = \abs K\bigr) \implies \abs M = \abs K$.
  \end{enumerate}
\end{proposition*}

\begin{exercise*}[Die Potenzmenge einer Menge ist mächtiger als die Menge]
  Sei $M$ eine Menge. Man zeige: Für jede Abbildung $f\colon M \to \powerset M$ ist
  \label{it:barbier}
  \[
  N_f \coloneqq \{p \in M \mid p \notin f(p)\} \in \powerset M \setminus f(M).
  \]
  Insbesondere existiert keine surjektive Abbildung $M \to \powerset M$.
\end{exercise*}

Diese Tatsache erklärt auch das sogenannte Paradoxon vom Barbier: Der
Barbier eines Dorfes behauptet, daß er genau die Männer des Dorfes
rasiert, die sich nicht selbst rasieren.  Das kann wohl nicht wahr
sein.  Wieso? Wer rasiert dann den Barbier? Um dieses Paradoxon
aufzuklären bezeichnen wir mit $M$ die Menge aller Männer des Dorfes und definieren
\[
f\colon M \to \powerset M, p \mapsto \{m \in M \mid \text{$m$ wird von $p$ rasiert}\}.
\]
Die obige Aufgabe zeigt, daß kein Mann des Dorfes genau die Männer
rasiert, die es nicht selbst tun.

\section{Familien und Folgen}
\label{sec:families}

\begin{definition*}
  Sind wir bei einer Abbildung $f\colon I \to A$ weniger an den
  Eigenschaften der Zuordnungsvorschrift als vielmehr an den Werten
  $f(i)$ interessiert, so benutzen wir häufig die Indexschreibweise
  \[
  f = (a_i)_{i \in I},\quad a_i \coloneqq f(i)
  \]
  und nennen $(a_i)_{i \in I}$ eine \emph{Familie} und $I$ ihre
  \emph{Indexmenge}. Anstatt $(a_i)_{i \in \set N_0}$ schreiben wir
  auch $(a_i)_{i = 0, 1, 2, \dotsc}$ oder $(a_i)_{i = 0}^\infty$.
  Entsprechend ist
  $(a_i)_{i = k, k + 1, k + 2, \dotsc} = (a_i)_{i = k}^{\infty}$,
  wobei $k \in \set Z$, zu verstehen. Bei den letzen zwei Beispielen
  reden wir auch von $\emph{Folgen}$.
\end{definition*}

\begin{warning*}
  Die Folge $(a_i)_{i = 0, 1, 2, \dotsc}$ ist wesentlich verschieden von der
  Menge ihrer Glieder $\{a_0, a_1, a_2, \dotsc\}$.
\end{warning*}

\begin{example*}
  Ist $(M_i)_{i \in I}$ eine Familie von Mengen, so kann für diese die \emph{Vereinigung}
  \[
  \bigcup_{i \in I} M_i \coloneqq \{p \mid \exists i \in I\colon p \in M_i\}
  = \bigcup \{M_i \mid i \in I\}
  \]
  und, wenn $I \neq \emptyset$, der \emph{Durchschnitt}
  \[
  \bigcap_{i \in I} M_i \coloneqq \{p \mid \forall i \in I\colon p \in M_i\}
  \]
  gebildet werden.
\end{example*}

\begin{proposition*}[de-Morgansche Regeln]
  Ist $(M_i)_{i \in I}$ eine Familie von Mengen und $M$ eine weitere Menge, so gilt
  \begin{enumerate}
  \item
    $M \cap (\bigcup_{i \in I} M_i) = \bigcup_{i \in I} (M \cap M_i)$ und
    $M \cup (\bigcap_{i \in I} M_i) = \bigcap_{i \in I} (M \cup M_i)$, wenn
    $I \neq \emptyset$,
  \item
    $M \setminus (\bigcup_{i \in I} M_i) = \bigcap_{i \in I} (M \setminus M_i)$
    und
    $M \setminus (\bigcap_{i \in I} M_i) = \bigcup_{i \in I} (M \setminus M_i)$,
    wenn jeweils $I \neq \emptyset$.
  \end{enumerate}
\end{proposition*}

\begin{exercise*}
  Es seien $f\colon M \to N$ eine Abbildung, $(M_i)_{i \in I}$ eine
  nicht leere Familie von Mengen $M_i \subseteq M$, $(N_i)_{i \in I}$
  eine nicht leere Familie von Mengen $N_i \subseteq N$ und $A$,
  $B \subseteq N$. Man zeige:
  \begin{enumerate}
  \item
    $f^{-1}(\bigcup_{i \in I} N_i) = \bigcup_{i \in I} f^{-1}(N_i)$.
  \item
    $f^{-1}(\bigcap_{i \in I} N_i) = \bigcap_{i \in I} f^{-1}(N_i)$.
  \item
    $f(\bigcup_{i \in I} M_i) = \bigcup_{i \in I} f(M_i)$
  \item
    \label{it:image_of_intersection}
    $f(\bigcap_{i \in I} M_i) \subseteq \bigcap_{i \in I} f(M_i)$.
  \end{enumerate}
  Man zeige durch Angabe eines Beispiels, daß in
  \ref{it:image_of_intersection} Gleichheit im allgemeinen nicht gilt.
\end{exercise*}

\section{Die charakteristischen Eigenschaften von $\set N_0$}
\label{sec:peano}

Die Idee der Erzeugung der Menge der natürlichen Zahlen durch
fortlaufendes Weiterzählen beginnend bei der Null wird durch folgende Eigenschaften beschrieben,
wobei $S\colon \set N_0 \to \set N_0, n \mapsto n + 1$ die \emph{Nachfolgerfunktion} ist:
\begin{namedthm}{Peanosche Axiome}
  \leavevmode
  \begin{axiomlist}[label=(N\arabic*)]
  \item
    $0 \in \set N_0$,
  \item
    $S$ ist injektiv,
  \item
    $0 \notin S(\set N_0)$,
  \item
    $\forall M \in \powerset{\set N_0}\colon \bigl(
    0 \in M \land S(M) \subseteq M \implies M = \set N_0\bigr).$
  \end{axiomlist}
\end{namedthm}

Aus den Peanoschen Axiomen folgt:
\begin{namedthm}{Beweisprinzip der vollständigen Induktion}
  Sei $A_0$, $A_1$, $A_2$, \dots\ eine Folge von Aussagen. Es sei
  $A_0$ wahr (\emph{Induktionsbeginn}).  Weiterhin gelte für alle
  $n \in \set N_0$: Ist $A_n$ wahr, so auch $A_{n + 1}$.  Dann ist
  $A_n$ für alle $n \in \set N_0$ wahr.
\end{namedthm}

\begin{corollary*}
  \leavevmode
  \begin{enumerate}
  \item Sei $m \in \set Z$. Sei $A_m$, $A_{m + 1}$, $A_{m + 2}$,
    \dots\ eine Folge von Aussagen. Es sei $A_m$ wahr.  Weiterhin
    gelte für alle ganzen Zahlen $n \ge m$: Ist $A_n$ wahr, so auch
    $A_{n + 1}$.  Dann ist $A_n$ für alle ganzen Zahlen $n \ge m$
    wahr.
  \item Sei $A_0$, $A_1$, $A_2$, \dots\ eine Folge von Aussagen.  Es
    gelte für alle $n \in \set N_0$:
    \begin{equation}
      \label{eq:induction}
      \text{Sind alle $A_k$ für $k = 0$,
        \dots, $n - 1$ wahr, so ist auch $A_n$ wahr.}
    \end{equation}
    Dann ist $A_n$ für alle $n \in \set N_0$ wahr.
  \end{enumerate}
\end{corollary*}

\begin{comment}
  Man beachte, daß in~\eqref{eq:induction} der Fall $n = 0$ explizit
  erlaubt ist. In diesem Fall ist die Voraussetzung (nämlich, daß alle
  $A_k$ für $k = 0$, \dots, $n - 1$ wahr sind) leer, so
  daß~\eqref{eq:induction} für $n = 0$ gerade fordert, daß $A_0$ wahr
  ist.
\end{comment}

\begin{namedthm}{Der Dedekindsche Satz über die rekursive Definition}
  Es seien $M$ eine Menge, $g\colon \set N_0 \times M \to M$ eine
  Abbildung und $p^* \in M$.  Dann existiert genau eine Folge
  $(p_n)_{n \in \set N_0}$ von Elementen $p_n \in M$ mit dem
  \emph{Startwert} $p_0 = p^*$ und dem \emph{rekursiven
    Bildungsgesetz} $p_{n + 1} = g(n, p_n)$ für alle $n \in \set N_0$.
\end{namedthm}

\begin{small}
  \begin{proof}
    Sei $[n] \coloneqq \{0, \dotsc, n\}$ für $n \in \set N_0$ die
    Menge der ersten $n + 1$ natürlichen Zahlen.  Weiter sei
    $[\infty] \coloneqq \set N_0$ die Menge
    aller natürlicher Zahlen.  Für jedes $n \in \set N_0$ sei $A_n$
    die Aussage, daß genau eine Familie $(p_k)_{k \in [n]}$ von
    Elementen $p_k \in M$ mit
    \begin{align}
      \tag{$S_n$}
      \label{eq:dedekind1}
      p_0 & = p^* &
      \\
      \intertext{und}
      \tag{$R_n$}
      \label{eq:dedekind2}
      \forall k \in \set N_0\colon k + 1 \in [n]\implies
      p_{k + 1} & = g(k, p_k)
    \end{align}
    existiert.
    Wir zeigen per Induktion, daß
    $A_n$ für alle $n \in \set N_0$ wahr ist.
    \begin{description}
    \item[Induktionsanfang.]  Für $n = 0$ ist $[n] = \{0\}$. Damit
      besteht die Familie $(p_k)_{k \in [0]}$ nur aus dem Element
      $p_0$.  Dieses ist eindeutig durch die Bedingung ($S_0$)
      bestimmt.  Die Bedingung ($R_0$) an die Familie ist offensichtlich leer.
      Damit ist $A_0$ wahr.
    \item[Induktionsschritt.]  Sei die Aussage $A_n$ wahr.  Seien
      $(p_k)_{k \in [n + 1]}$ und $(\tilde p_k)_{k \in [n + 1]}$ zwei
      Familien, welche beide die Bedingungen ($S_{n + 1}$) und ($R_{n + 1}$)
      erfüllen.  Damit erfüllen $(p_k)_{k \in [n]}$ und
      $(\tilde p_k)_{k \in [n]}$ die Bedingungen \eqref{eq:dedekind1} und \eqref{eq:dedekind2}.  Da $A_n$ wahr ist, gilt
      damit $p_k = \tilde p_k$ für $k \leq n$.  Weiter gilt
      $p_{n + 1} = g(n, p_n) = g(n, \tilde p_n) = \tilde p_{n + 1}$ wegen ($R_{n + 1}$).  Damit ist
      sogar $(p_k)_{k \in [n + 1]} = (\tilde p_k)_{k \in [n + 1]}$  und somit
      die Eindeutigkeitsaussage von $A_{n + 1}$ wahr.

      Es bleibt, die Existenzaussage von $A_{n + 1}$ zu zeigen:
      Zunächst folgt aus der Existenzaussage von $A_n$, daß eine
      Familie $(p_k)_{k \in [n]}$ existiert, welche ($S_n$) und ($R_n$)
      erfüllt.  Setzen wir $p_{n + 1} \coloneqq g(n, p_n)$, so
      erhalten wir eine Familie $(p_k)_{k \in [n + 1]}$, welche
      $S_{n + 1}$ und $R_{n + 1}$ erfüllt.

      Insgesamt haben wir also
      gezeigt, daß $A_{n + 1}$ ebenfalls wahr ist.
    \end{description}

    Es bleibt, die Existenz und Eindeutigkeit einer Familie
    $(p_k)_{k \in \set N_0}$, welche ($S_\infty$) und ($R_\infty$)
    erfüllt, zu zeigen.  Die Eindeutigkeit folgt daraus, daß für jedes
    $n \in \set N_0$ die abgeschnittene Familie $(p_k)_{k \in [n]}$
    nach dem eben bewiesenen eindeutig bestimmt ist und damit
    insbesondere der Wert $p_n$ eindeutig bestimmt ist.  Die Existenz
    zeigen wir durch Konstruktion:  Sei dazu für jedes $n \in \set N_0$ die Familie
    $(p^{(n)}_k)_{k \in [n]}$ diejenige, deren Existenz durch $A_n$ postuliert wird.
    Dann setze $p_n \coloneqq p_n^{(n)}$.  Aufgrund der Eindeutigkeitsaussage von $A_n$
    ist $p_{n}^{(n + 1)} = p_{n}^{(n)}$ und damit
    erfüllt $(p_n)_{n \in \set N_0}$ die Bedingungen ($S_\infty$) und ($R_\infty$).
  \end{proof}
\end{small}

\begin{comment}
  In obigen Satz können wir $\set N_0$ auch durch $\set N_m$ mit
  $m \in \set Z$ ersetzen, wenn wir mit $p_m = p^*$ starten.
\end{comment}

\begin{examples*}
  \leavevmode
  \begin{enumerate}
  \item Die \emph{Fakultät} $n!$ ist charakterisiert durch $0! = 1$
    und $(n + 1)! = (n + 1) \cdot n!$ für alle $n \in \set N_0$ (hierbei ist offensichtlich
    $g\colon \set N_0 \times \set N_0 \to \set N_0, (n, m) \mapsto (n + 1) \cdot m$).
  \item Sei $a \in \set R$ eine reelle Zahl.  Die ganzzahligen, nicht
    negativen \emph{Potenzen} von $a$ sind durch $a^0 = 1$ und
    $a^{n + 1} = a \cdot a^n$ für alle $n \in \set N_0$
    charakterisiert.  Im Falle von $a \neq 0$ und $n > 0$ setzen wir
    zusätzlich $a^{-n}\coloneqq \frac 1 {a^n}$.  Damit sind Potenzen mit
    allen ganzzahligen Exponenten definiert.
  \end{enumerate}
\end{examples*}

Eine exakte Formulierung der folgenden Definitionen benutzt ebenfalls
den Satz von Dedekind über die rekursive Definition:
\begin{definition*}
  Seien $m$, $k \in \set Z$.  Seien weiter $a_m$, $a_{m + 1}$, \dots,
  $a_k \in \set R$ reelle Zahlen.  Dann ist
  \begin{align*}
    \sum_{n = m}^k a_n & \coloneqq \begin{cases}
      a_m + a_{m + 1} + \dotsb + a_k, & \text{wenn $m < k$ oder $m = k,$} \\
      0, & \text{wenn $m > k,$}
    \end{cases} \\
    \intertext{und}
    \prod_{n = m}^k a_n & \coloneqq \begin{cases}
      a_m \cdot a_{m + 1} \cdot \dotsm \cdot a_k, & \text{wenn $m < k$ oder $m = k,$} \\
      1, & \text{wenn $m > k.$}
    \end{cases}
  \end{align*}
\end{definition*}

\section{Über die ganzen, rationalen und reellen Zahlen}
\label{sec:numbers}

Die negativen Zahlen $-1$, $-2$, $-3$, \dots\ wurden als fiktive
Zahlen eingeführt, um die Gleichung
\begin{equation}
  \label{eq:integers}
  n + x = m,\quad n, m \in \set N_0
\end{equation}
stets lösen zu können. (Ähnlich war der Grund für die Schöpfung der
komplexen Zahlen --- man beachte auch die Bezeichnung "`imaginäre
Zahlen"' für $i$, $2 i$, usw.: Hier geht es um die uneingeschränkte
Lösbarkeit quadratischer Gleichungen und Gleichungen höheren Grades
wie zum Beispiel $x^2 + 1 = 0$.)  Dadurch entsteht die Menge $\set Z$
der ganzen Zahlen, auf die in bekannter Weise die Addition und
Multiplikation von $\set N_0$ fortgesetzt werden kann.  Die Addition
in $\set Z$ hat die Eigenschaft, daß die Gleichung~\eqref{eq:integers} in $\set Z$
immer lösbar ist.

Hingegen ist die Gleichung
\[
q x = p,\quad p, q \in \set Z, q \neq 0
\]
in $\set Z$ nicht uneingeschränkt lösbar.  Erforderlich wird
bekanntlich die Erweiterung zum Körper $\set Q$ der rationalen Zahlen.
Wie schon seit der Antike bekannt, gibt es jedoch z.\,B.\ in der
Geometrie auftauchende Zahlen, wie etwa die Länge $\sqrt 2$ der
Diagonale eines Einheitsquadrates, welche nicht durch rationale Zahlen
ausgedrückt werden können:
\begin{proposition*}
  In $\set Q$ ist die Gleichung $x^2 = 2$ nicht lösbar.
\end{proposition*}

In diesem Sinne hat der Körper $\set Q$ "`Lücken"'.  Aus diesem Grunde
gilt in ihm beispielsweise auch nicht der Zwischenwertsatz.  Um eine
befriedigende Analysis betreiben zu können, muß $\set Q$ durch
Hinzunahme weiterer Zahlen zur Menge $\set R$ der reellen Zahlen
vervollständigt werden.  Ausgehend von $\set Q$ können die reellen
Zahlen als Dedekindsche Schnitte oder als Äquivalenzklassen von
Cauchyfolgen konstruiert werden.  Sinn dieser Konstruktion ist, daß
so die axiomatische Einführung der reellen Zahlen, wie wir sie in den
beiden folgenden Kapiteln vornehmen werden, gerechtfertigt werden
kann.

%%% Local Variables:
%%% TeX-master: "skript"
%%% End:
