\chapter{Das Vollständigkeitsaxiom}

In der Analysis machen wir die Vorstellung von "`unendlich nahe bei"'
präzise.  Dazu werden wir den Begriff der $\epsilon$-Umgebung von
Punkten $a \in \widehat{\set R}$ definieren.  Außerdem wird in diesem
Kapitel schließlich die Axiomatik der reellen Zahlen durch Angabe des
noch fehlenden Vollständigkeitsaxioms (R13) abgeschlossen.  Dazu
benötigen wir noch den Begriff des Supremums und Infimums einer
Teilmenge.

\section{$\epsilon$-Umgebungen}
\label{sec:epsilon}

\begin{definition*}
  Für alle $\epsilon \in \set R_+$ und $a \in \widehat{\set R}$
  definieren wir die \emph{$\epsilon$-Umgebung}
  \[
  U_\epsilon(a) \coloneqq \begin{cases}
    \left]a - \epsilon, a + \epsilon\right[ & \text{für $a \in \set R,$} \\
    \left]1/\epsilon, \infty\right] & \text{für $a = \infty,$} \\
    \left[-\infty, -1/\epsilon\right[ & \text{für $a = -\infty.$}  
  \end{cases}
  \]
\end{definition*}

\begin{proposition*}
  Seien $a$, $b \in \widehat{\set R}$ und $\epsilon$, $\delta \in \set R_+$.  Dann gilt:
  \begin{enumerate}
  \item
    $a \in U_\epsilon(a)$,
  \item
    $\delta < \epsilon \implies U_\delta(a) \subseteq U_\epsilon(a)$,
  \item
    $a \neq b \implies \exists \epsilon \in \set R_+\colon
    U_\epsilon(a) \cap U_\epsilon(b) = \emptyset$.

    Diese letzte Eigenschaft ist die sogenannte "`Hausdorffsche Trennungseigenschaft"'.
  \end{enumerate}
\end{proposition*}

\section{Maximum, Minimum, Supremum und Infimum}
\label{sec:supremum}

\begin{definition}
  Es sei $M \in \powerset{\widehat{\set R}}$.
  \begin{enumerate}[ref={\thedefinition (\alph*)}]
  \item Eine Zahl $s \in \widehat{\set R}$ heißt \emph{obere}
    (bzw.~\emph{untere}) \emph{Schranke} von $M$, wenn
    \[
    \forall a \in M: a \leq s\quad\text{bzw.~$a \ge s$}.
    \]
    Insbesondere ist $\infty$ für jede Menge eine obere und $-\infty$
    eine untere Schranke.
  \item Besitzt $M$ eine obere (bzw.~untere) Schranke $s \in \set R$,
    so heißt $M$ nach \emph{oben} (bzw.~\emph{unten})
    \emph{beschränkt}.  Wir nennen $M$ \emph{beschränkt}, wenn $M$
    nach oben und unten beschränkt ist.
  \item
    Eine obere (bzw.~untere) Schranke $s$ von $M$ mit $s \in M$ heißt ein
    \emph{Maximum} (bzw.~\emph{Minimum}) von $M$.
  \item
    \label{it:supremum}
    Ist $M \neq \emptyset$, so heißt eine obere (bzw.~untere)
    Schranke $s$ von $M$ ein \emph{Supremum} (bzw.~\emph{Infimum}) von $M$, wenn
    \[
    \forall \epsilon \in \set R_+\colon M \cap U_\epsilon(s) \neq \emptyset.
    \]
    Das Supremum und Infimum der leeren Menge wird im folgenden Zusatz definiert.
  \end{enumerate}
\end{definition}

Ist $s$ ein Maximum (bzw.~Minimum) von $M$, so ist $s$ offenbar auch ein Supremum
(bzw.~Infimum) von $M$.

\begin{example*}
  Es ist $1$
  \begin{itemize}
  \item
    ein Maximum von $[0, 1]$, 
  \item
    kein Maximum von $\left[0, 1\right[$, aber
  \item
    ein Supremum von $\left[0, 1\right[$.
  \end{itemize}
\end{example*}

\begin{proposition*}
  Jede nicht leere Menge $M$ besitzt höchstens ein Supremum (also erst
  recht höchstens ein Maximum) und höchstens ein Infimum (also erst
  recht höchstens ein Minimum).  Daher können wir in Zukunft von
  \emph{dem} Maximum, Minimum, Supremum und Infimum einer Menge (im
  Falle der Existenz, vgl.~dazu auch das folgende Axiom (R13))
  sprechen.  Bezeichnungen: $\max(M)$, $\min(M)$, $\sup(M)$,
  $\inf(M)$.
\end{proposition*}

\begin{exercise*}
  Für $-M \coloneqq \{-a \mid a \in M\}$ gilt: $-M$ hat genau dann ein
  Maximum (bzw.~Supremum), wenn $M$ ein Minimum (bzw.~Infimum) hat.
  Im Falle der Existenz ist
  \[
  \max(-M) = -\min(M)\quad\text{bzw.~$\sup(-M) = -\inf(M)$}.
  \]
\end{exercise*}

\begin{namedthm}{Zusatz}
  Wir definieren $\sup(\emptyset) \coloneqq -\infty$ und
  $\inf(\emptyset) \coloneqq \infty$.
\end{namedthm}

\begin{definition}
  Ist $M$ eine Menge und $n \in \set N_0$, so definieren wir:
  \begin{enumerate}
  \item
    $\{0, \dotsc, n - 1\} \coloneqq [0, n - 1] \cap \set N_0$.
  \item
    $\abs M = n \coloniff \abs M = \abs{\{0, \dotsc, n - 1\}}$.
  \item
    $\abs M < \infty \coloniff \exists n \in \set N_0\colon \abs M = n$.
    In diesem Falle sagen wir, $M$ sei eine \emph{endliche} Menge.
  \item
    $\abs M = \infty \coloniff \lnot (\abs M < \infty)$.
    In diesem Falle sagen wir, $M$ sei eine \emph{unendliche} Menge.
  \end{enumerate}
\end{definition}

\begin{comment*}
  In der Mengenlehre wird gezeigt: Ist $M$ eine Menge und gilt sowohl
  $\abs M = m$ als auch $\abs M = n$ für $m$, $n \in \set N_0$, so ist
  $m = n$ (\emph{Dirichletscher Schubfächersatz}); in diesem Falle
  heißt $n$ die \emph{Anzahl} der Elemente von $M$.
\end{comment*}

\begin{theorem*}[Existenz von Maxima und Minima]
  \leavevmode
  \begin{enumerate}
  \item Jede endliche, nicht leere Teilmenge von $\set R$ hat ein
    Maximum und ein Minimum.
  \item
    Jede nicht leere Teilmenge von $\set N_0$ hat ein Minimum.
  \end{enumerate}
\end{theorem*}

\section{Das Vollständigkeitsaxiom}
\label{sec:r_completeness}

Wir kommen schließlich zum letzten Axiom, welches die Menge der
reellen Zahlen erfüllen soll.
\begin{namedthm}{Vollständigkeitsaxiom}
  \leavevmode
  \begin{axiomlist}[label=(R\arabic*), start=13]
  \item 
    \label{it:sup_axiom}
    Jede Teilmenge $M \in \powerset{\set R}$ von $\set R$ besitzt
    ein Supremum (u.\,U.~$\pm \infty$).
  \end{axiomlist}
\end{namedthm}
Ein angeordneter Körper, der wie $\set R$ das Vollständigkeitsaxiom
erfüllt, heißt ein \emph{vollständiger angeordneter Körper}.  Wir
kommen später in Abschnitt~\ref{sec:r_unicity} auf die Einzigartigkeit
von $\set R$ zu sprechen. 

\begin{corollary}
  Jede Teilmenge $M \in \powerset{\set R}$ von $\set R$ besitzt ein Infimum.
\end{corollary}

\begin{corollary}
  \label{cor:sqrt}
  Zu jeder Zahl $a \in \left[0, \infty\right[$ existiert genau eine
  Zahl $b \in \left[0, \infty\right[$, so daß $b^2 = a$ gilt; diese
  Zahl $b$ heißt die \emph{Quadratwurzel} von $a$; sie wird mit
  $\sqrt{a}$ bezeichnet.
\end{corollary}

\begin{proof}[Beweisidee]
  Für $a \in \set R_+$ ist $\sqrt{a} \in \set R_+$ das Supremum der
  nicht leeren Menge
  $\{t \in \left[0, \infty\right[ \mid t^2 \leq a\}$.
\end{proof}

\begin{comment*}
  Es ist nicht möglich, die Existenz der Quadratwurzeln ohne das Axiom
  (R13) zu beweisen.  Wenn dies doch ginge, könnten wir beweisen, daß
  der Körper $\set Q$, in welchem ja die anderen Axiome (R1)--(R12)
  erfüllt sind, die Quadratwurzel $\sqrt 2$ enthielte, was ja nach der
  Proposition aus~\ref{sec:numbers} nicht der Fall ist.

  Solche Unabhängigkeitsbeweise für ein Axiomensystem sind manchmal
  sehr schwer.  So wurde 2000 Jahre lang versucht, das
  "`Parallelaxiom"' aus den anderen Axiomen der euklidischen Geometrie
  herzuleiten, bis Anfang des 19.~Jahrhunderts mit der Entdeckung der
  hyperbolischen Geometrie eingesehen wurde, daß das Parallelenaxiom
  von den anderen Axiomen der euklidischen Geometrie unabhängig ist.
\end{comment*}

\begin{exercise*}[Alternative Beschreibung des Supremums]
  \leavevmode
  \begin{enumerate}
  \item Sei $\Sigma$ die Menge aller oberen Schranken einer
    beliebigen Teilmenge $M \in \powerset{\set R}$ von $\set R$
    ($M = \emptyset$ ist ausdrücklich zugelassen).  Dann besitzt
    $\Sigma$ ein Minimum, und zwar ist $\min(\Sigma) = \sup(M)$.  Eine
    entsprechende Aussage gilt für das Infimum.  Also:
    \begin{itemize}
    \item
      $\sup(M)$ ist die kleinste obere Schranke und
    \item
      $\inf(M)$ ist die größte untere Schranke von $M$.
    \end{itemize}
  \item Eine nicht leere Menge $M \subseteq \set R$ ist genau dann
    nach oben beschränkt, wenn $\sup(M) < \infty$ ist.
  \end{enumerate}
\end{exercise*}

\section{Der Satz des Archimedes}
\label{sec:archimedes}

\begin{definition*}
  Wir sagen, eine Teilmenge $M \in \powerset{\set R}$ liegt
  \emph{dicht} in $\set R$, wenn
  \[
  \forall a \in \set R\, \forall \epsilon \in \set R_+\colon U_\epsilon(a) \cap M
  \neq \emptyset.
  \]
\end{definition*}

\begin{theorem*}
  Die Menge $\set N_0$ der natürlichen Zahlen ist nicht nach oben
  beschränkt\footnote{Dieses Theorem war schon Eudoxos von Knidos,
    etwa 408--355~v.\,Chr., bekannt.  Da Archimedes von
    287--212~v.\,Chr. gelebt hat, ist die Bezeichnung des Satzes nach
    Archimedes eigentlich nicht korrekt.}, d.\,h.
  \[
  \forall a \in \set R\, \exists n \in \set N_0\colon a < n.
  \]
  Daher gilt auch
  \[
  \forall \epsilon \in \set R_+\, \exists n \in \set N_1\colon \frac 1 n < \epsilon.
  \]
\end{theorem*}

\begin{comment*}
  Es existieren angeordnete Körper (die also die Axiome (R1)--(R12)
  erfüllen), z.~B.\ die Körper der hyperreellen Zahlen der
  Nichtstandardanalysis, in welchen beide Teile des Satzes von
  Archimedes falsch sind.
\end{comment*}

\begin{corollary*}
  In $\set R$ liegen $\set Q$ und $\set R \setminus \set Q$, die Menge
  der \emph{irrationalen} Zahlen, dicht.
\end{corollary*}

\section{Die Mächtigkeit spezieller Mengen}

\begin{definition*}
  Für jede Menge $M$ definieren wir:
  \begin{enumerate}
  \item
    $M$ ist \emph{abzählbar}, wenn $\abs M = \abs{\set N_0}$.
  \item $M$ ist \emph{höchstens abzählbar}, wenn $\abs M < \infty$
    oder $\abs M = \abs{\set N_0}$.
  \item $M$ ist \emph{überabzählbar}, wenn $M$ nicht höchstens
    abzählbar ist.
  \end{enumerate}
\end{definition*}

\begin{theorem}
  Die Mengen $\set Z$, $\set N_0 \times \set N_0$ und $\set Q$ sind abzählbar.
\end{theorem}

\begin{theorem}
  Die Menge $\set R$ ist überabzählbar.
\end{theorem}

Der folgende Beweis der Überabzählbarkeit von $\set R$, der erste Beweis
dafür überhaupt, wurde 1873 von Georg Cantor gegeben.

\begin{small}
  \begin{proof}
    Zunächst stellen wir fest, daß es zu je endlich vielen reellen
    Zahlen eine weitere von diesen verschiedene gibt.  Damit kann
    jedenfalls $\abs{\set R} < \infty$ nicht gelten.  Nehmen wir als
    nächstes an, daß $\set R$ abzählbar ist.  Dies wollen wir zum
    Widerspruch führen.  Sei also $x_0$, $x_1$, $x_2$, \dots{} eine
    Abzählung von $\set R$ (das heißt, die Folge
    $(x_k)_{k \in \set N_0}$ reeller $x_k \in \set R$ vermittelt eine
    Bijektion $\set N_0 \to \set R$).  Wir können ohne Beschränkung
    der Allgemeinheit davon ausgehen, daß $x_0 < x_1$ (ansonsten
    vertauschen wir die ersten beiden Folgenglieder).  

    Wir
    konstruieren daraus zwei Folgen $(i_n)_{n \in \set N_0}$ und
    $(j_n)_{n \in \set N_0}$ natürlicher Zahlen $i_n$,
    $j_n \in \set N_0$ vermöge rekursiver Definition wie folgt: Wir
    setzen $i_0 \coloneqq 0$ und $j_0 \coloneqq 1$, so daß
    insbesondere $x_{i_0} < x_{j_0}$ nach Annahme an die Folge
    $(x_k)_{k \in \set N_0}$.

    Seien $i_n$ und $j_n$ schon konstruiert.  Da zwischen zwei reellen
    Zahlen unendlich viele weitere reelle Zahlen liegen und die Folge
    $(x_k)_{k \in \set N_0}$ nach Voraussetzung alle reellen Zahlen
    durchläuft, existieren $i_{n + 1} \in \set N_0$ minimal mit
    $x_{i_n} < x_{i_{n + 1}} < x_{j_n}$ und dann
    $j_{n + 1} \in \set N_0$ minimal mit
    $x_{i_{n + 1}} < x_{j_{n + 1}} < x_{j_n}$.  Nach Konstruktion sind
    $(i_n)_{n \in \set N_0}$ und $(j_n)_{n \in \set N_0}$ jeweils
    streng monoton wachsende Folgen natürlicher Zahlen für die
    \begin{equation}
      \label{eq:cantor}
      \forall k \in \set N_0\colon k < i_n \implies
      x_k < x_{i_n} \lor x_k > x_{j_n}
    \end{equation}
    für alle $n \in \set N_0$ gilt.

    Sei $a \coloneqq \sup(\{x_{i_n} \mid n \in \set N_0\})$.  Nach
    Konstruktion ist
    \[
    \forall n, m \in \set N_0\colon x_{i_n} < a < x_{j_m}
    \]
    und insbesondere $a \in \set R$.  Da das Bild der Folge
    $(x_k)_{k \in \set N_0}$ die Menge aller reeller Zahlen ist,
    existiert somit ein $\ell \in \set N_0$ mit $x_\ell = a$.
    Sei dann $n \in \set N_0$ mit $\ell < i_n$.
    Wegen~\eqref{eq:cantor} gilt $a < x_{i_n}$ oder $a > x_{j_n}$.
    Dies ist in jedem Falle ein Widerspruch.  Also kann unsere Annahme
    nicht stimmen und die Menge der reellen Zahlen ist folglich
    überabzählbar.
  \end{proof} 
\end{small}

\begin{corollary*}
  Die Menge $\set R \setminus \set Q$ ist überabzählbar.
\end{corollary*}

\begin{comment*}
  Die sogenannte \emph{Kontinuumshypothese} besagt, daß für jede
  überabzählbare Teilmenge $M$ von $\set R$ schon
  $\abs M = \abs {\set R}$ gilt, daß es also keine Mächtigkeiten
  zwischen der Mächtigkeit von $\set N_0$ und der Mächtigkeit des
  Kontinuums (das ist die Mächtigkeit von $\set R$) gibt.  Durch
  Arbeiten von Kurt Gödel und Paul Cohen ist inzwischen bekannt, daß die
  Gültigkeit dieser Hypothese nicht im Rahmen der
  Zermelo--Fraenkelschen Mengenlehre (und damit insbesondere auch
  nicht im Rahmen der bisher vorgestellten mengentheoretischen Axiome)
  entschieden werden kann.
\end{comment*}

\section{Einzigartigkeit von $\set R$} 
\label{sec:r_unicity}

Wir setzen jetzt die Untersuchung von Abschnitt~\ref{sec:q_unicity}
fort.  Die Bezeichnungen seien wie dort.  Insbesondere sei $f$ der
dort definierte Körperisomorphismus $\set Q \to \set Q'$.  Zusätzlich
setzen wir aber voraus, daß die Körper $\set R$ und $\set R'$ auch
noch das Axiom (R13) erfüllen.  Dann gilt:
\begin{theorem}
  Es gibt genau einen \emph{Isomorphismus angeordneter Körper}
  $g\colon \set R \to \set R'$; also eine Bijektion mit folgenden
  Eigenschaften für alle $a$, $b \in \set R$:
  \begin{enumerate}
  \item
    \label{it:r_sum}
    $g(a + b) = g(a) + g(b)$,
  \item
    \label{it:r_unit}
    $g(1) = 1'$,
  \item
    \label{it:r_product}
    $g(a \cdot b) = g(a) \cdot g(b)$,
  \end{enumerate}
  welche \emph{streng monoton wachsend} ist, also
  \begin{enumerate}[start=4]
  \item
    \label{it:r_order}
    $a < b \implies g(a) < g(b)$.
  \end{enumerate}
\end{theorem}

\begin{small}
  \begin{proof}[Beweisskizze]
    Natürlich werden im folgenden die Eigenschaften der Abbildung $f$
    aus dem Theorem aus Abschnitt~\ref{sec:q_unicity} vollumfänglich
    ausgenutzt.  Zur Konstruktion von $g$ definieren wir für jedes
    $a \in \set R$ die Mengen
    \begin{align*}
      M(a) & \coloneqq \{q \in \set Q \mid q \leq a\} & & \text{und}
      & N(a) & \coloneqq \{q \in \set Q \mid q \ge a\}. \\
      \intertext{Nun zeigt man, daß}
      g(a) & \coloneqq \sup f(M(a)) & & \text{und}
      & h(a) & \coloneqq \inf f(N(a))
    \end{align*}
    dieselbe Zahl aus $\set R'$ sind.  Warum dann die
    unterschiedlichen Definitionen für diese Zahl?  Weil man damit
    leicht zeigen kann:
    \begin{itemize}
    \item
      $g(a) = h(a) = f(a)$ für alle $a \in \set Q$,
    \item $g(a) + g(b) \leq g(a + b)$ und $h(a + b) \leq h(a) + h(b)$
      für alle $a$, $b \in \set R$,
    \item $g(a) \cdot g(b) \leq g(a \cdot b)$ und
      $h(a \cdot b) \leq h(a) \cdot h(b)$ für alle $a$,
      $b \in \set R_+$,
    \item
      $h(a) < g(b)$ für alle $a$, $b \in \set R$ mit $a < b$.
    \end{itemize}
    Aus diesen Eigenschaften folgen unmittelbar die
    Eigenschaften~\ref{it:r_sum}--\ref{it:r_order} des Theorems.  Da
    nach dem Theorem aus~\ref{sec:q_unicity} die Abbildung
    $g|\set Q = f$ durch die
    Bedingungen~\ref{it:r_sum}--\ref{it:r_product} eindeutig
    festgelegt ist, sieht man leicht, daß nur die von uns definierte
    Funktion $g$ auch noch die Bedingung~\ref{it:r_order} erfüllt.

    Es bleibt also nur noch die Bijektivität von $g$ zu beweisen.  Die
    Injektivität von $g$ folgt natürlich sofort aus~\ref{it:r_order}.
    Mehr Arbeit hat man mit der Surjektivität. Oder doch nicht? Hier
    ein einfaches, wenn auch abstraktes Argument. Entsprechend zur
    Konstruktion von $g$ konstruieren wir \emph{die} Abbildung
    $g'\colon \set R' \to \set R$, welche mutatis mutandis die
    Bedingungen~\ref{it:r_sum}--\ref{it:r_order} erfüllt.  Dann
    besitzt neben $\id_{\set R'}$ auch die Komposition
    $g \circ g'\colon \set R' \to \set R'$ die
    Eigenschaften~\ref{it:r_sum}--\ref{it:r_order}.  Da es aber nur
    eine Abbildung $\set R' \to \set R'$ gibt, welche die
    Bedingungen~\ref{it:r_sum}--\ref{it:r_order} erfüllt, ist
    $g \circ g' = \id_{\set R'}$.  Hieraus folgt die Surjektivität von
    $g$ nach der Proposition~\ref{prop:inj_sur_crit} aus~\ref{sec:maps}.
  \end{proof}
\end{small}

\begin{proposition*}
  Die Bedingung~\ref{it:r_order} im Theorem folgt bereits
  aus~\ref{it:r_sum} und \ref{it:r_product}.
\end{proposition*}

\begin{proof}
  Wir benutzen das Korollar~\ref{cor:sqrt}
  aus~\ref{sec:r_completeness}.  Sind $a$, $b \in \set R$ mit $a < b$,
  so existiert ein $c \in \set R_+$ mit $c^2 = b - a$.  Daher folgt
  mit~\ref{it:r_product}: $g(b - a) = g(c^2) = g(c)^2 > 0$, wobei der
  Fall $g(c)^2 = 0$ nicht eintreten kann, weil $g(0) = 0'$ und $g$
  injektiv ist, also $g(c) \neq 0'$.  Wegen~\ref{it:q_sum} ist daher
  $g(b) - g(a) > 0$, also $g(a) < g(b)$.
\end{proof}

Nachdem wir die Einzigartigkeit eines vollständigen angeordneten
Körpers $\set R$ festgestellt haben, stellen wir seine Existenz schließlich
durch ein weiteres mengentheoretisches Axiom sicher:

\begin{namedthm}{Unendlichkeitsaxiom}
  Es existiert ein vollständiger angeordneter Körper.
\end{namedthm}

Der Grund für den Namen dieses Axioms ist der, daß aus den bis dahin
vorgestellten mengentheoretischen Axiomen nicht die Existenz einer
unendlichen Menge gefolgert werden kann.  Dagegen ist $\set R$ eine
unendliche Menge.

%%% Local Variables:
%%% TeX-master: "skript"
%%% End:
