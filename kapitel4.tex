\chapter{Konvergenz von Folgen}

Wir erinnern an die Definition von
\[
\set N_m \coloneqq \{n \in \set Z \mid n \ge m\}
\]
für alle ganzen Zahlen $m \in \set Z$.  Anstatt von $n \in \set N_m$
werden wir auch $n \ge m$ schreiben, wenn aus dem Zusammenhang klar
ist, daß $n$ eine ganze Zahl ist.

\section{Konvergenz in metrischen Räumen}
\label{sec:convergence}

Sei im folgenden $E$ ein metrischer Raum, $m \in \set Z$ und
$(p_n)_{n \ge m}$ eine Folge von Punkten $p_n \in E$.

\begin{definition*}
  Die Folge $(p_n)_{n \ge m}$ \emph{konvergiert} gegen ein $q \in E$, wenn
  \begin{equation}
    \label{eq:convergence}
    \forall \epsilon \in \set R_+\,\exists n_0 \ge m\,\forall n \ge n_0\colon
    p_n \in U_\epsilon(q).
  \end{equation}
  In diesem Falle schreiben wir auch $\displaystyle \lim_{n \to \infty} p_n = q$.  Konvergiert die
  Folge gegen \emph{kein} $q$, so sagen wir auch, daß sie \emph{divergiert}.
\end{definition*}

\begin{example}
  Jede konstante Folge $(p_n)_{n \ge m}$ von $E$ konvergiert, und zwar
  gegen $q$, wenn $\forall n \ge m: p_n = q$ ist.
\end{example}

\begin{proposition}
  Die Folge $(p_n)_{n \ge m}$ kann gegen höchstens einen Punkt $q \in E$ konvergieren.

  Im Falle, daß $(p_n)_{n \ge m}$ gegen $q$ konvergiert, heißt $q$
  auch der \emph{Limes} oder, falls $E$ ein Rechenbereich wie $\set R$
  oder ein Vektorraum ist, der \emph{Grenzwert} der Folge
  $(p_n)_{n \ge m}$.
\end{proposition}

\begin{proposition}
  \label{prop:cofinal}
  Für Konvergenz sind nur die "`Endstücke"' der Folgen entscheidend;
  genauer: Sind $(p_n)_{n \ge m_1}$ und $(q_n)_{n \ge m_2}$ zwei
  Folgen in $E$ und existiert ein $m_3 \ge \max\{m_1, m_2\}$, so daß
  $\forall n \ge m_3: p_n = q_n$ gilt, so konvergiert die Folge
  $(p_n)_{n \ge m_1}$ genau dann gegen $q \in E$, wenn die Folge
  $(q_n)_{n \ge m_2}$ gegen $q$ konvergiert.

  (Dadurch wird im Nachhinein die Bezeichnung
  $\displaystyle \lim_{n \to \infty} p_n$ gerechtfertigt, in welche
  nicht eingeht, für welche $n$ genau die $p_n$ zu betrachten sind.)
\end{proposition}

\begin{namedthm}{Konvergenz in $\set R$}
  Da $\set R$ ein metrischer Raum ist, wissen wir nach Obigem, was
  unter Konvergenz einer Folge in $\set R$ zu verstehen ist.
  Konvergiert eine Folge $(a_n)_{n \ge m}$ reeller Zahlen $a_n$ gegen
  $0$, so nennen wir $(a_n)_{n \ge m}$ eine \emph{Nullfolge}.
\end{namedthm}

\begin{example}
  $(1/n)_{n \ge 1}$ ist eine Nullfolge.
\end{example}

\begin{namedthm}{Konvergenz in $\widehat{\set R}$}
  Obwohl $\widehat{\set R}$ kein metrischer Raum ist, können wir die
  Konvergenzdefinition \eqref{eq:convergence} ohne Änderung für diesen
  Raum übernehmen, da wir auch $U_\epsilon(\infty)$ und
  $U_\epsilon(-\infty)$ definiert haben.  Wir können also von der
  Konvergenz einer reellen Zahlenfolge $(a_n)_{n \ge m}$ gegen
  $\infty$ bzw.~gegen $-\infty$ sprechen. (Andere Autoren sprechen an
  dieser Stelle von Divergenz; wir nicht!)

  Da auch $\widehat{\set R}$ die Hausdorffsche Trennungseigenschaft
  hat, gilt auch in $\widehat{\set R}$ die Aussage über die
  Eindeutigkeit des Limes.  Offenbar gilt:
  \begin{enumerate}
  \item
    $\displaystyle \lim_{n \to \infty} a_n = \infty \iff \forall c \in \set R_+ \,
    \exists n_0 \ge m \, \forall n \ge n_0 \colon a_n > c,$
  \item
    $\displaystyle \lim_{n \to \infty} a_n = -\infty \iff \forall c \in \set R_+ \,
    \exists n_0 \ge m \, \forall n \ge n_0 \colon a_n < - c.$
  \end{enumerate}
\end{namedthm}

\begin{proposition}
  Ist eine reelle Zahlenfolge $(a_n)_{n \ge m}$ monoton wachsend
  (bzw.~fallend), d.\,h.~die Funktion
  $f\colon \set N_m \to \set R, n \mapsto a_n$ ist monoton wachsend
  (bzw.~fallend) (vgl.~\ref{sec:monotony}), so gilt
  \[
  \lim_{n \to \infty} a_n = \sup\{a_n \mid n \ge m\}
  \quad\text{(bzw.~$\displaystyle \lim_{n \to \infty} a_n = \inf\{a_n \mid n \ge m\}$)}.
  \]

  Insbesondere konvergiert jede beschränkte monotone Zahlenfolge in $\set R$.
\end{proposition}

\begin{example}
  Ist $a$ eine reelle Zahl, so erhalten wir eine monoton wachsende
  Folge $(a_n)_{n \ge 0}$ rationaler Zahlen, wenn wir mit $a_n \leq a$
  die rationale Zahl bezeichnen, die entsteht, wenn die
  Dezimalbruchentwicklung von $a$ an der $n$-ten Stelle abgebrochen
  wird.  In diesem Falle ist $\lim_{n \to \infty} a_n = a$.  Zum
  Beispiel gilt für die Kreiszahl $\pi$:
  \[
  \begin{split}
    \pi_0 & = 3,\quad
    \pi_1 = 3{,}1,\quad
    \pi_2 = 3{,}14,\quad
    \pi_3 = 3{,}141,\quad
    \pi_4 = 3{,}1415,\quad
    \pi_5 = 3{,}14159,\\
    \pi_{50} & = 3{,}14159265358979323846264338327950288419716939937510
  \end{split}
  \]
\end{example}

\begin{small}
  Mit Hilfe des folgenden Scheme-Programmes können wir prinzipiell
  beliebig viele Stellen von $\pi$ bestimmen:
  \begin{lstlisting}[gobble=4,basicstyle=\ttfamily,keywordstyle=\bfseries,
    alsodigit=',
    alsoletter=*+?-<>,
    otherkeywords={'},
    morekeywords={define,car,cdr,cons,force,delay,let,if,quotient,zero?,-,<,+,*}]
    (define (head stream)
      (car stream))

    (define (tail stream)
      (force (cdr stream)))

    (define (stream->list stream n)
      (let loop ((stream stream) (n n))
        (if (zero? n)
            '()
            (cons (head stream)
                  (loop (tail stream) (- n 1))))))

    (define (stream/pi)
      (let loop ((q 1) (r 0) (t 1) (k 1) (n 3) (l 3))
        (if (< (+ (* 4 q) r (- t)) (* n t))
            (cons n
                  (delay
                    (loop (* 10 q) (* 10 (- r (* n t))) t k
                          (- (quotient (* 10 (+ (* 3 q) r))
                                       t)
                             (* 10 n))
                          l)))
                    (loop (* q k)
                          (* (+ (* 2 q) r) l)
                          (* t l)
                          (+ k 1)
                          (quotient (+ (* q (+ (* 7 k) 2))
                                       (* r l))
                                    (* t l))
                          (+ l 2)))))

    (define (digits/pi n)
      (stream->list (stream/pi) (+ n 1)))
  \end{lstlisting}
  Rufen wir die Prozedur \verb|(digits/pi n)| mit einer
  nicht-negativen Zahl $n$ auf, so liefert sie eine Liste der Ziffern
  bis einschließlich der $n$-ten Nachkommastelle von $\pi$ zurück. Der im
  Programm verwendete Algorithmus ist in
  \href{http://web.comlab.ox.ac.uk/oucl/work/jeremy.gibbons/publications/spigot.pdf}
  {\textsc{Jeremy Gibbons}:
    \textit{Unbounded Spigot Algorithms for the Digits of Pi}, American
    Mathematical Monthly, Vol.~113 (4), 2006, S.~318--328} zu finden.
\end{small}

\section{Konvergenz von Teilfolgen}
\label{sec:convergence_subseq}

\begin{definition*}
  Ist $(p_n)_{n \ge m}$ eine Folge irgendwelcher Elemente und
  $i\colon \set N_0 \to \set N_m$ eine streng monoton wachsende
  Funktion, so heißt $(p_{i(n)})_{n \in \set N_0}$ eine
  \emph{Teilfolge} von $(p_n)_{n \ge m}$.  Häufig schreiben wir auch
  $p_{i_k}$ anstatt $p_{i(k)}$.
\end{definition*}
Da die Funktion $i$ streng monoton wachsend ist, gilt:
$\forall n \in \set N_0\colon i(n) \ge m + n$.

\begin{proposition*}
  Konvergiert eine Folge $(p_n)_{n \ge m}$ von Punkten $p_n \in E$
  eines metrischen Raumes $E$ gegen einen Punkt $q \in E$, so
  konvergiert auch jede Teilfolge von $(p_n)_{n \ge m}$ gegen $q$.
  Die Aussage bleibt richtig, wenn $E$ durch $\widehat{\set R}$
  ersetzt wird.
\end{proposition*}

\begin{corollary*}
  Ist $q \in \set R$, so gilt:
  \[
  \lim_{n \to \infty} q^n = \begin{cases}
    \infty & \text{für $q > 1$},
    \\
    1 & \text{für $q = 1$},
    \\
    0 & \text{für $\abs q < 1$}.
  \end{cases}
  \]
  Im Falle $q \leq -1$ existiert der Grenzwert nicht.
\end{corollary*}

\section{Das Heine--Kriterium für Stetigkeit}

\begin{theorem*}
  Es seien $E$ und $E'$ metrische Räume, $f\colon E \to E'$ eine
  Abbildung und $q \in E$ ein Punkt.  Dann sind folgende Aussagen
  äquivalent:
  \begin{enumerate}
  \item
    \label{it:heine_cont}
    $f$ ist in $q$ stetig.
  \item
    \label{it:heine_seq}
    Für jede gegen $q$ konvergente Folge $(p_n)_{n \ge m}$ von Punkten $p_n \in E$ gilt
    \begin{equation}
      \label{eq:heine}
      \lim_{n \to \infty} f(p_n) = f(q).
    \end{equation}
  \end{enumerate}
\end{theorem*}
Anstelle von~\eqref{eq:heine} können wir auch
\[
\lim_{n \to \infty} f(p_n) = f(\lim_{n \to \infty} p_n)
\]
schreiben; anders ausgedrückt: Limesbildung und Operieren stetiger
Abbildungen sind miteinander vertauschbar.

\begin{comment*}
  \leavevmode
  \begin{enumerate}
  \item
    Die Implikation
    $\lnot\text{\ref{it:heine_seq}} \implies
    \lnot\text{\ref{it:heine_cont}}$,
    welche ja zur Implikation
    $\text{\ref{it:heine_cont}} \implies \text{\ref{it:heine_seq}}$
    äquivalent ist, beschreibt eine Strategie, um die Nicht-Stetigkeit
    von Abbildungen nachzuweisen.
  \item Zum Beweis von
    $\text{\ref{it:heine_seq}} \implies \text{\ref{it:heine_cont}}$
    wird das sogenannte \emph{Auswahlaxiom} der Mengenlehre benötigt.
  \end{enumerate}
\end{comment*}

\begin{namedthm}{Auswahlaxiom}[\textsc{E.~Zermelo 1904}]
  Sei $I$ eine Menge und $(M_i)_{i \in I}$ eine Familie
  nicht leerer Mengen. Dann existiert eine Familie $(p_i)_{i \in I}$
  von Elementen $p_i$ mit der Eigenschaft:
  \[
  \forall i \in I: p_i \in M_i.
  \]
\end{namedthm}
Diese Aussage wurde bis zum Ende des 19.~Jahrhunderts mit der größten
Selbstverständlichkeit benutzt, und zwar solange bis auffiel, daß sich
ohne diese Aussage einige überhaupt nicht selbstverständliche Sätze
(wie der \emph{Wohlordnungssatz} oder das \emph{Zornsche Lemma}) nicht
beweisen lassen.  Im Jahre 1963 gelang \textsc{P.~H.~Cohen} der
Beweis, daß das Auswahlaxiom nicht aus den anderen Axiomen der
Mengenlehre abgeleitet werden kann; vorher war schon durch Arbeiten
von \textsc{K.~Gödel} bekannt, daß das Auswahlaxiom verträglich mit
den anderen Axiomen der Mengenlehre ist.

Das folgende Korollar ist eine Folgerung aus dem Heine--Kriterium für Stetigkeit:
\begin{corollary*}
  $\displaystyle \forall a \in \set R_+\colon \lim_{n \to \infty} \sqrt[n] a = 1$ und
  $\displaystyle \lim_{n \to \infty} \sqrt[n]n = 1$.
\end{corollary*}

\section{Konvergenz in Teilräumen}
\label{sec:convergence_subspace}

\begin{exercise*}
  Sei $M$ ein Teilraum eines metrischen Raumes $E$, $(p_n)_{n \ge m}$
  eine Punktfolge in $M$ und $q \in M$.  Dann gilt:

  Die Folge $(p_n)_{n \ge m}$ konvergiert genau dann in dem Teilraum
  $M$ gegen $q$, wenn sie in $E$ gegen $q$ konvergiert.
\end{exercise*}

\section{Konvergenz in Produkträumen}
\label{sec:convergence_product}

\begin{proposition*}
  Seien $E_1$, \dots, $E_N$ metrische Räume und $E$ der Produktraum
  $\displaystyle \prod_{i = 1}^N E_i$ (vgl.~\ref{sec:metric_product}).
  Weiterhin sei jeweils
  $(p_{i,n})_{n \ge m}$ für $i = 1$, \dots, $N$ eine Folge in $E_i$,
  und für alle $n \ge m$ setzen wir
  $p_n \coloneqq (p_{i, n})_{i = 1, \ldots, N}$.  Dann
  konvergiert die Folge $(p_n)_{n \ge m}$ in $E$ genau dann gegen
  einen Punkt $p^*$, wenn für jedes $i = 1$, \dots, $N$ die
  "`Komponentenfolge"' $(p_{i, n})_{n \ge m}$ in $E_i$ gegen den Punkt
  $\pr_i(p^*)$ konvergiert (vgl.~\ref{sec:metric_product}).
\end{proposition*}

\begin{corollary*}
  Es seien $(a_n)_{n \ge m}$ und $(b_n)_{n \ge m}$ reelle
  Zahlenfolgen, $\alpha$, $a$, $b \in \set R$ reelle Zahlen, und es
  gelte $\displaystyle \lim_{n \to \infty} a_n = a$ und
  $\displaystyle \lim_{n \to \infty} b_n = b$.  Dann gilt:
  \begin{align*}
    \lim_{n \to \infty} (a_n + b_n) & = a + b,
    & \lim_{n \to \infty} (\alpha \cdot a_n) & = \alpha \cdot a,
    & \lim_{n \to \infty} (a_n \cdot b_n) & = a \cdot b, \\
    \intertext{und im Falle $a \neq 0$ auch}
    \lim_{n \to \infty} 1/a_n & = 1 / a.
  \end{align*}
\end{corollary*}

\begin{exercise*}[Weitere Rechenregeln für Zahlenfolgen]
  Seien $(a_n)_{n \ge m}$, $(b_n)_{n \ge m}$ und $(c_n)_{n \ge m}$
  reelle Zahlenfolgen, $a$, $b \in \widehat{\set R}$, und es gelte
  $\displaystyle \lim_{n \to \infty} a_n = a$ und
  $\displaystyle \lim_{n \to \infty} b_n = b$.  Man zeige:
  \begin{enumerate}
  \item
    Ist $a = \infty$, so ist $\displaystyle\lim_{n \to \infty} 1/a_n = 0$.
  \item
    Ist $a = 0$ und $(c_n)_{n \ge m}$ eine beschränkte Zahlenfolge, so gilt
    $\displaystyle\lim_{n \to \infty} (a_n \cdot c_n) = 0$.
  \item
    Ist $a = \infty$ und existiert ein $\epsilon \in \set R_+$, so daß $\forall n \ge m\colon
    c_n \ge \epsilon$ gilt, so gilt auch $\displaystyle\lim_{n \to \infty} (a_n \cdot c_n) = \infty$.
  \item
    Gilt $a_n \leq b_n$ für alle $n \geq m$, so gilt auch $a \leq b$.
  \item
    Und schließlich:
    \begin{namedthm}{Das Sandwich-Theorem}
      \leavevmode
      \begin{enumerate}
      \item Gilt $a_n \leq c_n \leq b_n$ für alle $n \ge m$ und ist
        $a = b$, so gilt auch $\displaystyle \lim_{n \to \infty} c_n = a$.
      \item Gilt $a_n \leq c_n$ für alle $n \ge m$ und ist $a = \infty$, so
        gilt auch $\displaystyle \lim_{n \to \infty} c_n = \infty$.
      \item
        Gilt $c_n \leq b_n$ für alle $n \ge m$ und ist $b = -\infty$,
        so gilt auch $\displaystyle \lim_{n \to \infty} c_n = -\infty$.
      \end{enumerate}
    \end{namedthm}
  \end{enumerate}
\end{exercise*}

\section{Häufungspunkte von Punktfolgen}
\label{sec:limit_points4}

\begin{definition*}
  Seien $E$ ein metrischer Raum, $(p_n)_{n \ge m}$ eine Punktfolge in $E$
  und $p \in E$ ein Punkt.  Dann heißt der Punkt $p$
  \emph{Häufungspunkt} der Folge $(p_n)_{n \ge m}$, wenn $p$ der Limes
  einer Teilfolge von $(p_n)_{n \ge m}$ ist.
\end{definition*}

\begin{proposition*}
  Jede konvergente Punktfolge besitzt genau einen Häufungspunkt, nämlich ihren Limes.
\end{proposition*}

\begin{comment*}
  Im Raum $E = \widehat{\set R}$ können wir Häufungspunkte genauso
  definieren; hier gilt die letzte Aussage ebenfalls unverändert.  Wir
  wissen also, was es bedeutet, daß $\infty$ oder $-\infty$
  Häufungspunkt einer reellen Zahlenfolge ist.
\end{comment*}

\section{Limes superior und Limes inferior einer Zahlenfolge}

Es sei $(a_n)_{n \ge m}$ eine Folge reeller Zahlen.

\begin{definition*}
  Für jedes $n \in \set N_m$ setzen wir
  \[
  \underline a_n \coloneqq \inf \{a_k \mid k \ge n\}\quad\text{und}\quad
  \overline a_n \coloneqq \sup \{a_k \mid k \ge n\}.
  \]
  Dann gilt für alle $n \in \set N_m$:
  \[
  \underline a_n \leq \underline a_{n + 1} \leq \overline a_{n + 1} \leq \overline a_n,
  \quad
  \underline a_n \in \left[-\infty, \infty\right[\quad
  \text{und}\quad
  \overline a_n \in \left]-\infty, \infty\right].
  \]
  Damit ist $(\underline a_n)_{n \ge m}$ eine monoton wachsende Folge reeller Zahlen, und wir können
  \[
  \liminf_{n \to \infty} a_n \coloneqq \lim_{n \to \infty} \underline a_n \in [-\infty, \infty]
  \]
  setzen.
  Weiter ist $(\overline a_n)_{n \ge m}$ eine monoton fallende Folge reeller Zahlen, und wir können
  \[
  \limsup_{n \to \infty} a_n \coloneqq \lim_{n \to \infty} \overline a_n \in [-\infty, \infty]
  \]
  setzen.  Die "`Zahlen"' $\displaystyle \liminf_{n \to \infty} a_n$
  und $\displaystyle \limsup_{n \to \infty} a_n$ heißen der
  \emph{Limes inferior} bzw.~\emph{Limes superior} der Folge
  $(a_n)_{n \ge m}$.
\end{definition*}

Ist $(a_n)_{n \ge m}$ nach unten beschränkt, so ist
$(\underline a_n)_{n \ge m}$ eine monoton wachsende Folge reeller Zahlen und daher
$\liminf\limits_{n \to \infty} a_n > -\infty$.  Andernfalls ist
$\underline a_n = - \infty$ für alle $n \ge m$ und daher $\liminf\limits_{n \to \infty} a_n = -\infty$.

Ist $(a_n)_{n \ge m}$ nach oben beschränkt, so ist
$(\overline a_n)_{n \ge m}$ eine monoton fallende Folge reeller Zahlen und daher
$\limsup\limits_{n \to \infty} a_n < \infty$.  Andernfalls ist
$\overline a_n = \infty$ für alle $n \ge m$ und daher $\limsup\limits_{n \to \infty} a_n = \infty$.

\begin{proposition*}
  In jedem Falle ist
  \begin{align*}
    \liminf_{n \to \infty} a_n & \leq \limsup_{n \to \infty} a_n
    & \text{und} &
    & \limsup_{n \to \infty} (-a_n) = - \liminf_{n \to \infty} a_n.
  \end{align*}
\end{proposition*}

\begin{namedthm}{Satz von Bolzano--Weierstraß}
  Jede reelle Zahlenfolge $(a_n)_{n \ge m}$ besitzt in
  $\widehat {\set R}$ mindestens einen Häufungspunkt; genauer:
  $\liminf\limits_{n \to \infty} a_n$ und $\limsup\limits_{n \to \infty} a_n$ sind
  Häufungspunkte der Folge $(a_n)_{n \ge m}$, und für jeden weiteren
  Häufungspunkt $a$ der Folge gilt:
  \[
  \liminf_{n \to \infty} a_n \leq a \leq \limsup_{n \to \infty} a_n.
  \]
\end{namedthm}

Mit anderen Worten sind der Limes inferior und der Limes superior der
kleinste bzw.~größte Häufungspunkt einer Folge.

\begin{theorem*}
  Eine reelle Zahlenfolge $(a_n)_{n \ge m}$ konvergiert genau dann, wenn
  \[
  \liminf_{n \to \infty} a_n = \limsup_{n \to \infty} a_n
  \]
  ist.  Im Falle der Konvergenz stimmen diese beiden Häufungspunkte mit
  $\lim_{n \to \infty} a_n$ überein.
\end{theorem*}

\section{Abgeschlossene Teilmengen eines metrischen Raumes}

\label{sec:closed_subsets}

Es seien $E$ ein metrischer Raum und $A \in \powerset E$ eine Teilmenge von $E$.

\begin{definition}
  \label{def:closure_metric}
  \leavevmode
  \begin{enumerate}
  \item Unter der \emph{abgeschlossenen Hülle} von $A$ verstehen wir
    die Menge $\overline A$ aller Punkte $p \in E$, welche Limes einer
    (in $E$) konvergenten Folge $(p_n)_{n \ge m}$ von Punkten
    $p_n \in A$ sind.

    Sicherlich ist $A \subseteq \overline A$.
  \item
    Die Teilmenge $A$ heißt \emph{abgeschlossen} (in $E$), wenn $\overline A = A$.
  \end{enumerate}
\end{definition}

\begin{examples*}
  \leavevmode
  \begin{enumerate}
  \item
    \label{it:trivial_closed}
    Die leere Menge und der ganze Raum $E$ sind abgeschlossene Teilmengen von $E$.
  \item
    \label{it:point_closed}
    Jede einpunktige Menge $\{p\} \subseteq E$ ist in $E$ abgeschlossen.
  \item Sind $a$, $b \in \set R$ Zahlen mit $a < b$, so ist das
    Intervall $[a, b]$ eine abgeschlossene Teilmenge von $\set R$ im
    Sinne dieses Abschnittes.
  \item
    Sind $f_1$, $f_2$, \dots, $f_n\colon E \to \set R$ stetige Funktionen, so sind die
    Mengen
    \[
    \{p \in E \mid f_1(p) = f_2(p) = \dotsb = f_n(p) = 0\}
    \]
    und
    \[
    \{p \in E \mid f_1(p) \ge 0, \dotsb, f_n(p) \ge 0\}
    \]
    abgeschlossene Teilmengen von $E$.  Wir sprechen bei diesen
    Beispielen von Mengen, die von stetigen Gleichungen
    bzw.~Ungleichungen definiert werden.  Insbesondere ist für jeden
    Punkt $p^* \in E$ und jeden Radius $r \in \left[0, \infty\right[$
    der \emph{Ball}
    \[
    B_r(p^*) \coloneqq \{p \in E \mid d(p^*, p) \leq r\}
    \]
    abgeschlossen.
  \end{enumerate}
\end{examples*}

\begin{proposition*}
  \label{prop:closure}
  Für jeden Punkt $p \in E$ gilt:
  $p \in \overline A \iff \forall \epsilon \in \set R_+\colon A \cap
  U_\epsilon(p) \neq \emptyset$.
\end{proposition*}

Hieraus entnehmen wir, daß aufgrund der Definition~\ref{it:supremum}
aus~\ref{sec:supremum} für jede beschränkte Menge
$M \in \powerset{\set R}$ gilt $\inf(M) \in \overline M$ und
$\sup(M) \in \overline M$.

\begin{corollary}
  Es ist $\overline{\overline A} = \overline A$, d.\,h.~die
  abgeschlossene Hülle ist --- wie der Name vermuten läßt ---
  tatsächlich abgeschlossen.
\end{corollary}

\begin{corollary}
  In $\set R$ gilt $\overline {\set Q} = \set R$ und
  $\overline{\set R \setminus \set Q} = \set R$.
\end{corollary}

\begin{small}
  \begin{proof}
    Man kombiniere die Dichtheitsaussage aus dem Korollar von
    \ref{sec:archimedes} mit obiger Proposition.
  \end{proof}
\end{small}

Eingedenk dieses Beweises können wir in Übereinstimmung mit der
Dichtheitsdefinition aus \ref{sec:archimedes} für beliebige metrische
Räume definieren:

\begin{definition}
  \label{def:dense}
  In einem metrischen Raum $E$ heißt $A \in \powerset{E}$ eine
  \emph{dichte} Teilmenge (von $E$), wenn $\overline A = E$.
\end{definition}

Es gilt: $\text{$A$ dicht in $E$} \iff \forall p \in E\,\forall \epsilon \in \set R_+\colon
A \cap U_\epsilon(p) \neq \emptyset$.

\begin{exercise*}
  \leavevmode
  \begin{enumerate}
  \item Sind $A$, $B \in \powerset E$ Teilmengen von $E$ und gilt $A \subseteq B$, so gilt auch
    $\overline A \subseteq \overline B$.
  \item
    \label{it:closure_union}
    Sind $A_1$, $A_2$, \dots, $A_n \in \powerset E$, so gilt
    $\displaystyle \overline{\bigcup_{\nu = 1}^n A_\nu} = \bigcup_{\nu
      = 1}^n \overline{A_\nu}$;
    insbesondere ist daher jede endliche Vereinigung von
    abgeschlossenen Teilmengen von $E$ wieder eine abgeschlossene
    Teilmenge.
  \item
    \label{it:closure_intersection}
    Ist $(A_i)_{i \in I}$ eine Familie von Teilmengen
    $A_i \in \powerset{E}$ mit $I \neq \emptyset$, so gilt für den Durchschnitt
    $\displaystyle \overline{\bigcap_{i \in I} A_i} \subseteq
    \bigcap_{i \in I} \overline{A_i}$.
    Man zeige an einem Beispiel, daß im allgemeinen keine Gleichheit
    gilt.

    Trotzdem ist der Durchschnitt von (beliebig vielen)
    abgeschlossenen Teilmengen von $E$ wieder eine abgeschlossene
    Teilmenge von $E$.
  \item
    \label{ex:preimage_closed}
    Es seien $E'$ ein weiterer metrischer Raum und
    $f\colon E \to E'$ eine stetige Abbildung.  Dann ist das Urbild
    $f^{-1}(B)$ einer jeden abgeschlossenen Teilmenge $B$ von $E'$
    eine abgeschlossene Teilmenge von $E$.  Man beachte dagegen: Das
    Bild $f(A)$ einer abgeschlossenen Teilmenge $A$ von $E$ ist im
    allgemeinen nicht in $E'$ abgeschlossen.
  \item Man bestimme für alle Intervalle von $\set R$ die
    abgeschlossene Hülle und entscheide damit, welche Intervalle im
    Sinne dieses Abschnittes abgeschlossene Teilmengen von $\set R$
    sind.
  \end{enumerate}
\end{exercise*}

\section{Folgenkompakte Mengen}
\label{sec:seq_compact}

\begin{definition*}
  Sei $E$ ein metrischer Raum.
  \begin{enumerate}
  \item Der Raum $E$ heißt \emph{folgenkompakt}, wenn jede Folge
    $(p_n)_{n \ge m}$ von Punkten aus $E$ einen Häufungspunkt (in $E$)
    besitzt, d.\,h.~also, wenn jede Folge $(p_n)_{n \ge m}$ von
    Punkten aus $E$ eine konvergente Teilfolge besitzt.
  \item Eine Teilmenge $K \in \powerset E$ von $E$ heißt eine
    \emph{folgenkompakte Teilmenge} von $E$, wenn der metrische
    Teilraum $K$ folgenkompakt ist, wenn also jede Folge
    $(p_n)_{n \ge m}$ von Punkten aus $K$ einen Häufungspunkt in $K$
    besitzt.
  \end{enumerate}
\end{definition*}

\begin{comment*}
  Die Folgenkompaktheit einer Teilmenge $K$ ist per definitionem eine
  Eigenschaft des metrischen Teilraumes $K$; hingegen ist der Begriff
  der Abgeschlossenheit nur relativ zu einem fixierten Gesamtraum
  sinnvoll.
\end{comment*}

\begin{examples*}
  \leavevmode
  \begin{enumerate}
  \item
    Die leere Menge ist folgenkompakt.
  \item
    Jede einpunktige Teilmenge $\{p\}$ eines metrischen Raumes ist
    folgenkompakt.
  \item
    Jedes Intervall $[a, b]$ mit $-\infty < a < b < \infty$ ist folgenkompakt.
  \item
    Der metrische Raum $\set R$ ist \emph{nicht} folgenkompakt.
  \end{enumerate}
\end{examples*}

\begin{proposition*}
  Für jede folgenkompakte Teilmenge $K$ eines metrischen Raumes $E$ gilt:
  \begin{enumerate}
  \item
    $K$ ist abgeschlossen in $E$.
  \item
    Jede abgeschlossene Teilmenge $A$ von $E$, die in $K$ liegt, ist auch folgenkompakt.
  \end{enumerate}
\end{proposition*}

\begin{exercise*}
  \leavevmode
  Man zeige:
  \begin{enumerate}
  \item
    Jede Vereinigung endlich vieler folgenkompakter Mengen ist folgenkompakt.
  \item Jeder (nicht-triviale) Durchschnitt (beliebig vieler)
    folgenkompakter Mengen ist folgenkompakt.
  \item
    \label{it:compact_image}
    Es seien $E'$ ein weiterer metrischer Raum und
    $f\colon E \to E'$ eine stetige Abbildung.  Dann ist das Bild
    $f(K)$ einer jeden folgenkompakten Teilmenge $K \in \powerset E$
    wieder folgenkompakt.  Man gebe weiter ein Beispiel dafür an, daß
    das Urbild $f^{-1}(B)$ einer folgenkompakten Teilmenge $B$ von
    $E'$ im allgemeinen \emph{nicht} folgenkompakt ist.
  \item
    Schließlich bestimme man alle folgenkompakten Intervalle von $\set R$.
  \end{enumerate}
\end{exercise*}

\section{Über die Existenz von Extremwerten}
\label{sec:extreme_values}

\begin{theorem*}
  Sei $E$ ein nicht leerer folgenkompakter metrischer Raum.  Dann nimmt jede
  stetige Funktion $f\colon E \to \set R$ ihr Maximum und ihr Minimum
  an, d.\,h.: Es existieren Punkte $p_1$, $p_2 \in E$, so daß gilt:
  \[
  \forall p \in E\colon f(p_1) \leq f(p) \leq f(p_2).
  \]
\end{theorem*}

\begin{corollary*}
  Jede folgenkompakte Teilmenge $K$ eines nicht leeren metrischen Raumes $E$
  ist \emph{beschränkt}, d.\,h.:
  Es existiert ein $r \in \set R_+$ und ein $p_0 \in E$, so daß $K \subseteq U_r(p_0)$.
\end{corollary*}

\section{Der Satz von Heine--Borel}

\label{sec:heine-borel}

\begin{theorem*}[Produkte folgenkompakter Mengen]
  Seien $E_1$, \dots, $E_N$ metrische Räume und $E$ der Produktraum
  $\prod\limits_{i = 1}^N E_i$
  (vergleiche~\ref{sec:metric_product}).  Weiterhin sei für jedes
  $i = 1$, \dots, $N$ eine folgenkompakte Teilmenge
  $K_i \in \powerset{E_i}$ gegeben.  Dann ist deren Produkt
  $\prod\limits_{i = 1}^N K_i$ eine folgenkompakte Teilmenge
  von $E$.
\end{theorem*}

\begin{corollary*}
  Im $\set R^n$ ist jeder \emph{Quader}
  $Q = \prod\limits_{i = 1}^n [a_i, b_i]$ mit $-\infty < a_i < b_i < \infty$
  für $i = 1$, \dots, $n$ eine folgenkompakte Teilmenge.
\end{corollary*}

\begin{namedthm}{Satz von Heine--Borel}[Charakterisierung der
  folgenkompakten Teilmengen des $\set R^n$]
  Eine Teilmenge $K \in \powerset{\set R^n}$ des $\set R^n$ ist genau
  dann folgenkompakt, wenn sie abgeschlossen und beschränkt ist.
\end{namedthm}

\section{Gleichmäßige Stetigkeit}

\label{sec:uniformity}

Es seien $(E, d)$ und $(E', d')$ metrische Räume und $f\colon E \to E'$ eine Abbildung.

\begin{definition*}
  Die Abbildung $f$ heißt \emph{gleichmäßig stetig}, wenn
  \[
  \forall \epsilon \in \set R_+\, \exists \delta \in \set R_+\,
  \forall p_0 \in E\colon f(U_\delta(p_0)) \subseteq U_\epsilon(f(p_0)),
  \]
  wenn also auf ganz $E$ das $\delta$ zum $\epsilon$ unabhängig vom
  speziellen Punkt $p_0$ gewählt werden kann.  Mit Hilfe der Metriken
  $d$ und $d'$ drückt sich dieser Tatbestand folgendermaßen aus:
  \[
  \forall \epsilon \in \set R_+\,
  \exists \delta \in \set R_+\,
  \forall p, q \in E\colon
  \bigl(d(p, q) < \delta \implies d'(f(p), f(q)) < \epsilon\bigr).
  \]
\end{definition*}

\begin{proposition*}
  Ist $f\colon E \to E'$ gleichmäßig stetig, so ist $f$ (in allen Punkten von $E$) stetig.
\end{proposition*}

\begin{example*}
  Es seien $E_1$, \dots, $E_N$ metrische Räume und $E$ der Produktraum
  $\prod\limits_{i = 1}^N E_i$.  Dann sind die kanonischen
  Projektionen $\pr_i\colon E \to E_i$ (vergleiche
  \ref{sec:metric_product}) gleichmäßig stetig.
\end{example*}

\begin{exercise*}
  Die Funktion $\sqrt[2]{}\colon \left[0, \infty\right[ \to \set R$
  ist gleichmäßig stetig; hingegen ist die Funktion
  $x^2\colon \set R \to \set R$ nicht gleichmäßig stetig.
\end{exercise*}

\begin{theorem*}
  Ist $E$ folgenkompakt und $f\colon E \to E'$ stetig, so ist $f$ auch gleichmäßig stetig.
\end{theorem*}

\section{Cauchyfolgen}

\label{sec:cauchy}

Es sei $(E, d)$ ein metrischer Raum.

\begin{definition*}
  Eine Punktfolge $(p_n)_{n \ge m}$ von $E$ heißt eine \emph{Cauchyfolge}, wenn gilt:
  \[
  \forall \epsilon \in \set R_+\,
  \exists n_0 \ge m\,
  \forall k, n \ge n_0\colon
  d(p_k, p_n) < \epsilon.
  \]
\end{definition*}

\begin{proposition}
  \label{prop:conv_cauchy}
  Jede konvergente Punktfolge von $E$ ist eine Cauchyfolge.
\end{proposition}

Wie es mit der Umkehrung dieser Aussage steht, sehen wir im nächsten Abschnitt.

\begin{proposition}
  Besitzt eine Cauchyfolge einen Häufungspunkt, so ist sie konvergent.
\end{proposition}

\begin{proposition}
  Jede Cauchyfolge ist beschränkt.
\end{proposition}

Im Falle von Cauchyfolgen gibt es ein Analogon zum ersten Teil des Heine-Kriteriums:
\begin{proposition}
  \label{prop:image_cauchy}
  Sind $E$ und $E'$ metrische Räume, $f\colon E \to E'$ eine
  gleichmäßig stetige Abbildung und $(p_n)_{n \ge m}$ eine
  Cauchyfolge, so ist auch $(f(p_n))_{n \ge m}$ eine Cauchyfolge.
\end{proposition}

\section{Vollständige metrische Räume}
\label{sec:complete_metric_space}

\begin{definition*}
  Ein metrischer Raum heißt \emph{vollständig}, wenn in ihm jede Cauchyfolge konvergiert.
\end{definition*}

\begin{comment}
  In vollständigen metrischen Räumen läßt sich also die Konvergenz von
  Punktfolgen ohne Kenntnis des Limes feststellen.
\end{comment}

\begin{example*}
  Es ist $\set R$ ein vollständiger metrischer Raum. Hingegen ist der
  Teilraum $\set Q$ nicht vollständig.
\end{example*}

Die Aussage über die Vollständigkeit von $\set R$ ist (unter Annahme
der übrigen Axiome) zum Vollständigkeitsaxiom \ref{it:sup_axiom}
äquivalent.  Cauchyfolgen wurden von \textsc{A.-L.~Cauchy} 1821 in
seinem Werk \emph{"`Course d'Analyse"'} wesentlich benutzt.  Allerdings
setzte er die Vollständigkeit von $\set R$ stillschweigend voraus.
Erst in der zweiten Hälfte des 19.~Jahrhunderts wurde den
Mathematikern die Vollständigkeitsproblematik bewußt.
\textsc{G.~Cantor} benutzte 1883 Cauchyfolgen, die er
Fundamentalfolgen nannte, zur Konstruktion der reellen Zahlen.

\begin{proposition*}
  Seien $E_1$, \dots, $E_n$ vollständige metrische Räume, so ist auch
  deren Produktraum $\prod\limits_{i = 1}^n E_i$ vollständig.
\end{proposition*}

\begin{corollary*}
  Der $\set R^n$ ist ein vollständiger metrischer Raum.
\end{corollary*}

\begin{comment}
  Jeder metrische Raum $E$ kann durch Hinzunahme geeigneter weiterer
  Punkte in natürlicher Weise \emph{vervollständigt} werden.  In
  diesem Sinne ist $\set R$ die Vervollständigung von $\set Q$.
\end{comment}

\begin{theorem*}[Vollständigkeit von Teilräumen]
  Es seien $E$ ein metrischer Raum und $M \in \powerset E$ eine
  Teilmenge von $E$.  Dann gilt:
  \begin{enumerate}
  \item Ist der metrische Teilraum $M$ vollständig, so ist $M$ eine
    abgeschlossene Teilmenge von $E$.
  \item Ist $E$ vollständig und $M$ eine abgeschlossene Teilmenge von
    $E$, so ist der metrische Teilraum $M$ ebenfalls vollständig.
  \end{enumerate}
\end{theorem*}

\begin{exercise*}[Der Cantorsche Durchschnittssatz]
  Es seien $(E, d)$ ein vollständiger metrischer Raum und
  $(A_n)_{n \ge m}$ eine Folge nicht-leerer abgeschlossener Teilmengen
  von $E$, für welche gelte:
  \begin{enumerate}
  \item
    \label{it:cantor_nesting}
    $\forall n \ge m\colon A_{n + 1} \subseteq A_n$ und
  \item die Folge $(\diam (A_n))_{n \ge m}$ der \emph{Durchmesser}
    $\diam (A_n) \coloneqq \sup\{d(p, q) \mid p, q \in A_n\}$ ist eine
    Nullfolge.
  \end{enumerate}
  Dann enthält der Durchschnitt $\bigcap\limits_{n \ge m} A_n$ genau einen Punkt, und zwar gilt:
  Jede Folge $(p_n)_{n \ge m}$ von Punkten $p_n \in A_n$ konvergiert gegen
  diesen einzigen Punkt
  $p^* \in \bigcap\limits_{n \ge m} A_n$.
\end{exercise*}

In dieser Version hat der Satz viele Anwendungen.

\begin{comment}
  Ein berühmter Spezialfall des Cantorschen Durchschnittssatzes ist
  der \emph{Satz über die Intervallschachtelung}. In ihm ist $E$ der
  metrische Raum $\set R$ und die $A_n$ sind abgeschlossene Intervalle
  $[a_n, b_n] \subseteq \set R$, welche --- wie in
  \ref{it:cantor_nesting} beschrieben --- ineinander geschachtelt sind
  und deren Längen $b_n - a_n$ eine Nullfolge bilden.  Zum Beweis der
  Existenz eines Elementes in $\bigcap\limits_n [a_n, b_n]$ wird die
  Vollständigkeit von $\set R$ gebraucht.  Tatsächlich ist diese zur
  Gültigkeit über die Intervallschachtelung äquivalent.
  \textsc{K.~Weierstraß} benutzte Intervallschachtelungen 1880/81 in
  Vorlesungen zur Einführung der reellen Zahlen und zum Beispiel auch
  zum Beweis des Satzes von Bolzano--Weierstraß.  Tatsächlich wurde
  aber mit Intervallschachtelungen schon vor über 2000 Jahren
  gearbeitet.  So stellt Archimedes' Approximation von $2 \pi$ durch
  die Länge von dem Einheitskreis eingeschriebenen Sehnenzügen
  bzw.~umbeschriebenen Tangentenzügen eine Intervallschachtelung dar.
\end{comment}

\section{Der Banachsche Fixpunktsatz}
\label{sec:banach_simple}

Es sei $E$ eine beliebige nicht-leere Menge und $f\colon E \to E$ eine
"`Selbstabbildung"' von $E$.  Ist dann $p_0 \in E$ irgendein Punkt, so
können wir durch die rekursive Definition
\[
\forall n \in \set N_0\colon p_{n + 1} \coloneqq f(p_n)
\]
eine Folge $(p_n)_{n \ge 0}$ in $E$ definieren. Im folgenden nennen
wir diese Punktfolge die Banachfolge bezüglich $f$ (oder kurz
$f$-Banachfolge) mit Startpunkt $p_0$.

\begin{exercise*}
  \leavevmode
  \begin{enumerate}
  \item
    Seien $E$ ein metrischer Raum und $f\colon E \to E$ eine stetige
    Abbildung.  Man zeige: Wenn eine $f$-Banachfolge gegen einen Punkt
    $p \in E$ konvergiert, so ist $p$ ein \emph{Fixpunkt} von $f$,
    d.\,h.~$f(p) = p$.
  \item Im \emph{\textsc{Heron}schen Verfahren} zur Bestimmung von
    $\sqrt a$ für $a \in \set R_+$ werden Banachfolgen bezüglich der
    Funktion $f = (x + a / x)/2|\set R_+$ benutzt.  Man zeige, daß
    jede $f$-Banachfolge konvergiert, und zwar gegen $\sqrt a$.
  \item Man versuche mittels eines Rechners unter Benutzung des ersten
    Aufgabenteils Fixpunkte für die Funktionen
    \[
    \cos,\quad\exp(-x),\quad\text{und}\quad\sqrt{}
    \]
    zu ermitteln und jeweils das \emph{Einzugsgebiet} der Fixpunkte zu
    bestimmen, d.\,h.~die Menge derjenigen Zahlen $a_0$, für welche
    die Banachfolge $(a_n)_{n \ge 0}$ bezüglich der jeweiligen
    Funktion gerade gegen den Fixpunkt konvergiert.  (Die
    Kosinus-Funktion betrachte man dabei als Funktion des Bogenmaßes.)
    Man prüfe seine experimentellen Ergebnisse an den Graphen der
    verschiedenen Funktionen.
  \end{enumerate}
\end{exercise*}

\begin{namedthm}{Der Banachsche Fixpunktsatz}
  Seien $(E, d)$ ein nicht-leerer vollständiger metrischer Raum und
  $f\colon E \to E$ eine \emph{kontrahierende} Abbildung, d.\,h.~es
  existiert eine Konstante $L \in \left[0, 1\right[$ (also
  $0 \leq L < 1$), so daß gilt:
  \[
  \forall p, q \in E\colon d(f(p), f(q)) \leq L \cdot d(p, q).
  \]
  (Insbesondere ist $f$ also gleichmäßig stetig.) Dann besitzt $f$
  genau einen Fixpunkt $p^*$; und zwar ist $p^*$ Limes einer jeden
  $f$-Banachfolge $(p_n)_{n \ge 0}$, denn es gilt:
  \[
  \forall n \ge 1 \colon d(p_n, p^*) \leq \frac{d(p_1, p_0)}{1 - L} \cdot L^n.
  \]
\end{namedthm}

\begin{comment*}
  Banachfolgen wurden schon im 19.~Jahrhundert benutzt.  Im Jahre 1903
  bewies \textsc{É.~Goursat} den Banachschen Fixpunktsatz für den
  $\set R^n$.  Im Jahre 1920 wurde er von dem Amerikaner
  \textsc{K.~Lamson} allgemeiner bewiesen; seine Untersuchung war aber
  in Europa nicht bekannt.  Im Jahre 1922 erschien er in der
  Dissertation des polnischen Mathematikers \textsc{S.~Banach},
  formuliert für die später nach ihm benannten Banachräume.  Der
  richtige Rahmen für den Banachschen Fixpunktsatz ist jedoch --- wie
  bald erkannt worden ist --- die Theorie der vollständigen metrischen
  Räume.
\end{comment*}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "skript"
%%% End:
