\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2022/23}

\title{\bfseries 11.~Übung zur Analysis III}
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Lukas\ Stoll, M.\ Sc.}
\date{12.~Januar -- 19.~Januar 2023
  \thanks{Es sind maximal 20 Punkte zu erreichen.}}

\begin{document}

\maketitle

% Vormals start=296
\begin{enumerate}[start=294]

\item \oralex \textbf{Translationsinvarianz des Lebesgueschen Maßes.}
  Das Lebesguesche Maß des $\set R^n$ ist translationsinvariant,
  d.\,h.:  Ist $v \in \set R^n$ ein beliebiger Vektor, $T\colon \set
  R^n \to \set R^n, p \mapsto p + v$ die Translation um $v$ und $A
  \subseteq \set R^n$
  eine Borel-meßbare Menge $A$, so ist $T(A)$
  Borel-meßbar mit $\lambda^n(T(A)) = \lambda^n(A)$.

  (Tip: Translationsinvarianz von $\borel{\set R^n}$;
  Translationsinvarianz des äußeren Lebesgueschen Maßes;
  Translationsinvarianz von $\mathfrak A_0(\set R^n)^*$; usw.)

\item \writtenex \textbf{(5 Punkte)} \textbf{Eine nicht-borelsche Teilmenge von $\set R$.}
  \label{ex:non_borel}
  Auf $\set R$ definieren wir die Äquivalenzrelation
  \[
    a \sim b \coloniff b - a \in \set Q.
  \]
  (Betrachten wir $\set R$ als Vektorraum über dem Körper $\set Q$, so
  sind die Äquivalenzklassen bezüglich der soeben definierten
  Äquivalenzrelation gerade die Elemente des Quotientenvektorraumes
  $\set R/\set Q$, also die affinen Unterräume $a + \set Q$, $a \in
  \set R$.) Aufgrund des Auswahlaxioms existiert eine Teilmenge $E
  \subseteq \interval 0 1$ mit
  \[
    \forall a \in \set R\, \exists! b \in E\colon a \sim b.
  \]
  Die abzählbare Teilmenge $\interval {-1} 1 \cap \set Q$ werde
  vermittels einer (injektiven) Abzählung $n \mapsto r_n$
  durchnummeriert, also
  \[
    \interval{-1} 1 \cap \set Q = \{r_n \mid n \in \set N_0\}.
  \]
  Wir setzen $E_n \coloneqq r_n + E$ für alle $n \in \set N_0$ und $F
  \coloneqq \bigcup\limits_{n = 0}^\infty E_n$.

  Zeige:
  \begin{enumerate}
  \item
    $\displaystyle \forall a, b \in E\colon (a \sim b \implies a =
    b).$
  \item
    \label{it:non_borel2}
    $(E_n)_{n \ge 0}$ ist eine Folge paarweise disjunkter Mengen.
  \item
    $\displaystyle \interval 0 1 \subseteq F \subseteq \interval{-1}
    2.$
  \item
    \label{it:non_borel4}
    Für das äußere Lebesguesche Maß $\mu_0^*$ gilt $1 \leq \mu_0^*(F)
    \leq 3$.
  \end{enumerate}
  Leite aus~\ref{it:non_borel2} und \ref{it:non_borel4} her, daß
  $E$ keine Borel-Menge sein kann.  (Sie kann nicht einmal ein Element
  der \name{Carathéodory}-Fortsetzung $\mathfrak A_0(\set R)^*$ sein.)

\item \oralex \textbf{Regularität des Lebesgueschen Maßes.}
  \label{ex:leb_regularity}
  Es sei $A \in \borel{\set R^n}$.  Beweise:
  \begin{enumerate}
  \item
    \label{it:lebreg1}
    Es ist $A$ \emph{von außen regulär}, d.\,h.~zu jedem $\epsilon \in
    \set R_+$ existiert eine offene Teilmenge $G \subseteq \set R^n$,
    so daß
    \[
      A \subseteq G\quad\text{und}\quad\lambda^n(G) \leq \lambda^n(A)
      + \epsilon.
    \]
    (Tip: Arbeite mit dem äußeren Maß $\mu_0^*$ und konstruiere
    $G$ als Vereinigung vieler offener Quader.)
  \item
    \label{it:leb_regularity2}
    Ist die Menge $A$ beschränkt, so ist sie \emph{von innen regulär},
    d.\,h.~zu jedem $\epsilon \in \set R_+$ existiert eine kompakte
    Teilmenge $K \subseteq \set R^n$, so daß
    \[
      K \subseteq A\quad\text{und}\quad\lambda^n(A) \leq \lambda^n(K)
      + \epsilon.
    \]
    (Tip: Es sei $C \subseteq \set R^n$ eine kompakte Teilmenge von
    $\set R^n$ mit $A \subseteq C$.  Dann ist $B \coloneqq C \setminus A$
    eine beschränkte Menge, auf die sich~\ref{it:lebreg1} anwenden
    läßt; nun betrachte $K \coloneqq C \setminus G$ und überlege
    $A \setminus K \subseteq G \setminus B$.)
  \end{enumerate}

\item \writtenex \textbf{(5 Punkte)} Seien $K \subseteq \set R^n$ eine kompakte Teilmenge
  und $G \in \Open{K, \set R^n}$ eine offene Umgebung von $K$ in $\set
  R^n$.  Dann existiert eine "`Höckerfunktion"' $\phi \in \Diffc
  \infty(G, \set R)$ mit $0 \leq \phi \leq 1$ und $\phi|K \equiv 1$.
  (Zur Definition von $\Diffc \infty(G, \set R)$ siehe
  Aufgabe~\ref{ex:density}\ref{thm:density}.)

  (Tip: Wir verwenden die euklidische Norm auf dem $\set R^n$.  Es
  existiert eine beliebig oft differenzierbare Funktion $f\colon \set
  R \to \set R$ mit $f|\interval[open left] {-\infty} 0 \equiv 0$ und
  $f|\interval[open] 0 \infty > 0$; vgl.\ die Aufgabe aus
  Abschnitt~9.5.  Die Funktion
  \[
    g\colon \set R \to \set R, t \mapsto \frac{f(t)}{f(t) + f(1 - t)}
  \]
  ist beliebig oft differenzierbar und erfüllt $0 \leq g \leq 1$,
  $g|\interval[open left] {-\infty} 0\equiv 0$ und
  $g|\interval[open right] 1 \infty \equiv 1$.  Für
  $\epsilon \in \set R_+$ ist die Funktion
  \[
    h_\epsilon\colon \set R^n \to \set R, p \mapsto g\Bigl(2 -
    \frac{\norm 2 p}{\epsilon}\Bigr)
  \]
  beliebig oft differenzierbar und erfüllt $0 \leq h_\epsilon \leq 1$,
  $h_\epsilon|(\set R^n \setminus U_{2\epsilon}(0)) \equiv 0$ und
  $h_\epsilon|U_\epsilon(0) \equiv 1$.
  Für jedes $p \in K$ wähle ein $\epsilon \in \set R_+$
  mit $U_{3\epsilon}(p) \subseteq G$ und setze $U_p \coloneqq
  U_\epsilon(p)$ und $\psi_p \coloneqq h_\epsilon(x - p)$.
  Es existiert eine endliche Teilmenge $I
  \subseteq K$, so daß $K \subseteq \bigcup\limits_{p \in I} U_p$.
  Schließlich setze $\psi \coloneqq \sum\limits_{p \in I}
  \psi_p$ und betrachte $\phi \coloneqq g \circ \psi|G$.)

\item \writtenex \textbf{(10 Punkte)} \textbf{Dichtesatz.}
  \label{ex:density}
  Seien $G \subseteq \set R^n$ eine offene Teilmenge und $E$ ein $\set K$-Banachraum.
  Wir bezeichnen mit $\Diffc \infty(G, E)$ den $\set K$-Vektorraum der
  beliebig oft differenzierbaren Funktionen $f\colon G \to E$ mit
  \emph{kompaktem Träger}.  Dabei hat eine Funktion $f\colon G \to E$
  kompakten Träger, wenn eine kompakte Teilmenge $K \subseteq G$ mit
  $f|(G \setminus K) \equiv 0$ existiert.
  Zeige:

  \begin{enumerate}
    \item Für jedes $p \in [1,\infty[$ ist $\Diffc \infty(G,E)$ ein Untervektorraum von $\Leb^p(G, \lambda^n; E)$.

      (Insbesondere ist zu zeigen, dass jedes $f\in\Diffc\infty(G,E)$ \emph{stark} messbar ist!)

    \item Sei $\phi \in \Simple_0(G, \borel G, \lambda^n; E)$ eine einfache
      Funktion mit $\phi|(G \setminus A) \equiv 0$ für eine beschränkte
      meßbare Teilmenge $A$.  Dann existiert für jedes $\epsilon \in
      \set R_+$ ein $f \in \Diffc \infty(G; E)$ mit kompaktem Träger und
      $\norm p {f - \phi} < \epsilon$.

      (Tip: Aufgabe~\ref{ex:leb_regularity}\ref{it:leb_regularity2}.)
    \item
    \label{thm:density}
    Für jedes $p \in \interval[open right] 1 \infty$ liegt
    $\Diffc \infty(G, E)$ in $\Leb^p(G, \lambda^n; E)$ dicht, d.\,h. zu jeder beliebigen
    Funktion $f \in \Leb^p(G, \lambda^n; E)$ existiert eine Folge
    $(f_k)$ von Funktionen $f_k \in \Diffc \infty(G, E)$, so daß
    $\lim\limits \norm p {f_k - f} = 0$.
    
    Infolgedessen kann jeweils $\LB^p(G, \lambda^n; E)$ als die
    Vervollständigung des normierten Raumes $(\Diffc \infty(G, E),
    \norm p \cdot)$ betrachtet werden.
    
    (Tip: Sei $f \in \Leb^p(G, \borel G, \lambda^n; E)$. 
    Sei $(\phi_n)_{n
      \ge 0}$ eine Folge von Funktionen $\phi_n \in \Simple_0(G,
    \borel G, \lambda^n; E)$ mit Grenzwert
    $\lim\limits_{n \to \infty} \norm p {\phi_n -
      f} = 0$.  Dann gilt auch
    $\lim\limits_{n \to \infty} \norm p {\phi_n \cdot 1_{K_n} - f} = 0$,
    wenn $(K_n)_{n \ge 0}$
    eine monotone Folge kompakter Teilmengen $K_n \subseteq \set R^n$
    mit $\bigcup\limits_{n = 0}^\infty K_n = \set R^n$ ist.)
  \end{enumerate}
\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
